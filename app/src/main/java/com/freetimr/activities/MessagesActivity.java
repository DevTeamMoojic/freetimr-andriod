package com.freetimr.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.freetimr.R;
import com.freetimr.adapter.MessagesAdapter;

import java.util.ArrayList;

public class MessagesActivity extends AppCompatActivity {

    RecyclerView messagesRecyclerView;
    MessagesAdapter messagesAdapter;
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        mToolbar = (Toolbar) findViewById(R.id.messages_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        messagesRecyclerView = (RecyclerView) findViewById(R.id.message_list_rv);
        initMessageAdapter();
    }

    public void initMessageAdapter() {
        ArrayList<String> jobList = new ArrayList<>();

        jobList.add("Jam Creative INC.,");
        jobList.add("Quixxi Pvt. Ltd");
        jobList.add("LingosMio");
        jobList.add("Siemens");
        jobList.add("National Laboratory");
        jobList.add("Amulya");
        jobList.add("Capegemini");
        jobList.add("Adonta");
        jobList.add("Ziva");
        jobList.add("Moojic");

        messagesAdapter = new MessagesAdapter(MessagesActivity.this, jobList);
        messagesRecyclerView.setAdapter(messagesAdapter);
        messagesRecyclerView.setLayoutManager(new LinearLayoutManager(MessagesActivity.this));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
