package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class JobMastersModel implements Parcelable {

    public AvailabilityDay[] AvailabilityDay;
    public AvailabilityTimeModel[] AvailabilityTime;
    public WorkLocationModel[] WorkLocation;
    public SkillModel[] Skill;
    public GraduationModel[] Graduation;
    public CityModel[] City;
    public SocialMediaModel[] SocialMedia;
    public LanguageModel[] Language;
    public JobActivityModel[] JobActivity;
    public JobStatusModel[] JobStatus;
    public DisabilityModel[] Disability;
    public JobTypeModel[] JobType;
    public JobCategoryModel[] JobCategory;

    protected JobMastersModel(Parcel in) {
        AvailabilityTime = in.createTypedArray(AvailabilityTimeModel.CREATOR);
        WorkLocation = in.createTypedArray(WorkLocationModel.CREATOR);
        Skill = in.createTypedArray(SkillModel.CREATOR);
        Graduation = in.createTypedArray(GraduationModel.CREATOR);
        City = in.createTypedArray(CityModel.CREATOR);
        SocialMedia = in.createTypedArray(SocialMediaModel.CREATOR);
        Language = in.createTypedArray(LanguageModel.CREATOR);
        JobActivity = in.createTypedArray(JobActivityModel.CREATOR);
        JobStatus = in.createTypedArray(JobStatusModel.CREATOR);
        Disability = in.createTypedArray(DisabilityModel.CREATOR);
        JobType = in.createTypedArray(JobTypeModel.CREATOR);
        JobCategory = in.createTypedArray(JobCategoryModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(AvailabilityTime, flags);
        dest.writeTypedArray(WorkLocation, flags);
        dest.writeTypedArray(Skill, flags);
        dest.writeTypedArray(Graduation, flags);
        dest.writeTypedArray(City, flags);
        dest.writeTypedArray(SocialMedia, flags);
        dest.writeTypedArray(Language, flags);
        dest.writeTypedArray(JobActivity, flags);
        dest.writeTypedArray(JobStatus, flags);
        dest.writeTypedArray(Disability, flags);
        dest.writeTypedArray(JobType, flags);
        dest.writeTypedArray(JobCategory, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobMastersModel> CREATOR = new Creator<JobMastersModel>() {
        @Override
        public JobMastersModel createFromParcel(Parcel in) {
            return new JobMastersModel(in);
        }

        @Override
        public JobMastersModel[] newArray(int size) {
            return new JobMastersModel[size];
        }
    };
}
