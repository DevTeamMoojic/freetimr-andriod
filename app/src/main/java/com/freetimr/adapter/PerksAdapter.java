package com.freetimr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.R;

import java.util.ArrayList;

/**
 * Created by Admin on 24-Dec-16.
 */

public class PerksAdapter extends RecyclerView.Adapter<PerksAdapter.PerksViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<String> list;

    public PerksAdapter(Context context, ArrayList<String> list) {
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public PerksAdapter.PerksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.perks_list_item, parent, false);
        return new PerksAdapter.PerksViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PerksAdapter.PerksViewHolder holder, int position) {
        holder.name.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PerksViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        public PerksViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.perks_name);
        }
    }

}