package com.freetimr.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.adapter.CityAdapter;
import com.freetimr.adapter.RoleAdapter;
import com.freetimr.adapter.SkillAdapter;
import com.freetimr.model.CandidateModel;
import com.freetimr.model.CandidatePreferredCityModel;
import com.freetimr.model.CandidateProfileSkillRoleModel;
import com.freetimr.model.CandidateSkillRoleModel;
import com.freetimr.model.CandidateSkills;
import com.freetimr.model.CityModel;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.FreeTimrUtils;
import com.freetimr.utils.ObservableScrollable;
import com.freetimr.utils.OnScrollChangedCallback;
import com.freetimr.utils.SystemBarTintManager;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.List;

public class ProfileActivity extends AppCompatActivity {

    private Drawable mActionBarBackgroundDrawable;
    Toolbar mToolbar;
    private View mHeader;
    private int mLastDampedScroll;
    private int mInitialStatusBarColor;
    private int mFinalStatusBarColor;
    private SystemBarTintManager mStatusBarManager;

    RecyclerView mRoleRecyclerView, mSkillsRecyclerView, mCityRecyclerView;
    RoleAdapter roleAdapter;
    SkillAdapter skillAdapter;
    CityAdapter cityAdapter;
    ImageView profileImage, cityEdit;

    BarChart mChart;

    TextView candidateDesignation, candidateExperience, candidateUniversity, candidateCurrentCompany, candidateLanguages, candidateCurrentLocation, candidateName, candidateDescription, candidateMobileNumber, noSkillView, noRoleView, noCityView, age, jobsAppliedText;
    RelativeLayout nameDesignationEditContainer, languageContainer;
    LinearLayout educationContainer, experienceContainer, resumeContainer, preferredCitiesContainer, skillsContainer, currentJobLocationContainer,skillRoleEditContainer;

    CandidateModel candidateModel;

    Menu menu;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mToolbar = (Toolbar) findViewById(R.id.profile_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        candidateModel = FreeTimrUtils.getCandidateInfo(ProfileActivity.this);
//        candidateModel.setProfileModels(FreeTimrUtils.getCandidateProfile(ProfileActivity.this));

        mActionBarBackgroundDrawable = getResources().getDrawable(R.color.background_color_toolbar);

        mStatusBarManager = new SystemBarTintManager(this);
        mStatusBarManager.setStatusBarTintEnabled(true);
        mInitialStatusBarColor = Color.TRANSPARENT;
        mFinalStatusBarColor = getResources().getColor(R.color.background_color_toolbar);

        ObservableScrollable scrollView = (ObservableScrollable) findViewById(R.id.profile_scrollview);
        mHeader = findViewById(R.id.profile_header);

        profileImage = (ImageView) findViewById(R.id.profile_image);
        candidateName = (TextView) findViewById(R.id.profile_candidate_name);
        candidateDesignation = (TextView) findViewById(R.id.profile_candidate_designation_location);
        candidateDescription = (TextView) findViewById(R.id.profile_candidate_description);
        candidateExperience = (TextView) findViewById(R.id.profile_candidate_exp);
        candidateUniversity = (TextView) findViewById(R.id.profile_candidate_university);
        candidateMobileNumber = (TextView) findViewById(R.id.profile_candidate_number);
        candidateCurrentCompany = (TextView) findViewById(R.id.profile_candidate_current_company);
        candidateLanguages = (TextView) findViewById(R.id.profile_candidate_languages_known);
        candidateCurrentLocation = (TextView) findViewById(R.id.profile_candidate_location);
        nameDesignationEditContainer = (RelativeLayout) findViewById(R.id.name_designation_edit_container);
        educationContainer = (LinearLayout) findViewById(R.id.education_container);
        experienceContainer = (LinearLayout) findViewById(R.id.experience_container);
        resumeContainer = (LinearLayout) findViewById(R.id.resume_container);
        preferredCitiesContainer = (LinearLayout) findViewById(R.id.preferred_cities_container);
        skillsContainer = (LinearLayout) findViewById(R.id.skills_container);
        cityEdit = (ImageView) findViewById(R.id.city_edit);
        noSkillView = (TextView) findViewById(R.id.no_skills_view);
        noRoleView = (TextView) findViewById(R.id.no_role_view);
        noCityView = (TextView) findViewById(R.id.no_city_view);
        age = (TextView) findViewById(R.id.profile_candidate_age);
        jobsAppliedText = (TextView) findViewById(R.id.jobs_applied_text);
        currentJobLocationContainer = (LinearLayout) findViewById(R.id.current_job_location_container);
        languageContainer = (RelativeLayout) findViewById(R.id.language_container);
        skillRoleEditContainer = (LinearLayout) findViewById(R.id.skill_role_edit_container);

//        setProfileData(candidateModel);

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, ProfilePictureActivity.class);
                startActivity(intent);
            }
        });

        mRoleRecyclerView = (RecyclerView) findViewById(R.id.role_recycler_view);
        initRoleAdapter();

        mSkillsRecyclerView = (RecyclerView) findViewById(R.id.skills_recycler_view);
        initSkillsAdapter();

        mCityRecyclerView = (RecyclerView) findViewById(R.id.city_recycler_view);
        initCityAdapter();

        mChart = (BarChart) findViewById(R.id.exp_skills_chart);
        initExperienceSkillsChart();

        scrollView.setOnScrollChangedCallback(new OnScrollChangedCallback() {
            @Override
            public void onScroll(int l, int scrollPosition) {
                int headerHeight = mHeader.getHeight() - mToolbar.getHeight();
                float ratio = 0;
                if (scrollPosition > 0 && headerHeight > 0)
                    ratio = (float) Math.min(Math.max(scrollPosition, 0), headerHeight) / headerHeight;

                updateActionBarTransparency(ratio);
                updateStatusBarColor(ratio);
                updateParallaxEffect(scrollPosition);
            }
        });

        nameDesignationEditContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, CandidateNameDesignationActivity.class);
                startActivity(intent);
            }
        });

        educationContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, EducationActivity.class);
                startActivity(intent);
            }
        });

        experienceContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, WorkExperienceActivity.class);
                startActivity(intent);
            }
        });

        resumeContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        preferredCitiesContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(ProfileActivity.this, PreferredCitiesActivity.class);
//                startActivity(intent);
            }
        });

        skillsContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, SkillsActivity.class);
                startActivity(intent);
            }
        });

        cityEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, CitySelectorActivity.class);
                startActivity(intent);
            }
        });

        languageContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, CandidateLanguageActivity.class);
                startActivity(intent);
            }
        });

        skillRoleEditContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, SkillRoleSelectorActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (candidateName != null) {
            setProfileData(FreeTimrUtils.getCandidateInfo(this));
        }
    }

    public void setProfileData(CandidateModel candidateModel) {
        ProfileModel profileModel = FreeTimrUtils.getCandidateProfile(ProfileActivity.this).get(0);

        candidateName.setText(candidateModel.getName());
        jobsAppliedText.setText("" + profileModel.JobApplication.size());

        if (profileModel.CandidateExperience.size() != 0) {
            candidateDesignation.setText(profileModel.CandidateExperience.get(0).Designation + ", " + profileModel.CandidateExperience.get(0).Location);
            candidateDescription.setText(profileModel.CandidateExperience.get(0).JobProfile);
            candidateCurrentCompany.setText(profileModel.CandidateExperience.get(0).EmployerName);
        }

        if (profileModel.CandidateEducation.size() != 0) {
            candidateUniversity.setText(profileModel.CandidateEducation.get(0).InstituteName);
        }

        candidateExperience.setText(profileModel.ExperienceYears + " Years");
        candidateMobileNumber.setText(candidateModel.getMobileNumber());
        candidateCurrentLocation.setText(profileModel.Location);
        age.setText(FreeTimrUtils.getAge(profileModel.DateOfBirth.getYear(), profileModel.DateOfBirth.getMonth(), profileModel.DateOfBirth.getDay()) + " years");
    }

    public void initRoleAdapter() {
        ArrayList<CandidateProfileSkillRoleModel> candidateSkillRoleModels = FreeTimrUtils.getCandidateProfile(this).get(0).CandidateSkillRole;

        if (candidateSkillRoleModels.isEmpty()) {
            noSkillView.setVisibility(View.VISIBLE);
            mRoleRecyclerView.setVisibility(View.GONE);
        } else {
            noSkillView.setVisibility(View.GONE);
            mRoleRecyclerView.setVisibility(View.VISIBLE);

            roleAdapter = new RoleAdapter(ProfileActivity.this, candidateSkillRoleModels);
            mRoleRecyclerView.setAdapter(roleAdapter);

            StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.HORIZONTAL);

            mRoleRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        }
    }

    public void initSkillsAdapter() {

        ArrayList<CandidateSkills> list = FreeTimrUtils.getCandidateProfile(this).get(0).CandidateSkill;

        if (list.isEmpty() || list == null) {
            noSkillView.setVisibility(View.VISIBLE);
            mSkillsRecyclerView.setVisibility(View.GONE);
        } else {
            noSkillView.setVisibility(View.GONE);
            mSkillsRecyclerView.setVisibility(View.VISIBLE);

            skillAdapter = new SkillAdapter(this, list, false);
            mSkillsRecyclerView.setAdapter(skillAdapter);
            StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.HORIZONTAL);
            mSkillsRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        }
    }

    public void initCityAdapter() {

        ArrayList<CandidatePreferredCityModel> list = FreeTimrUtils.getCandidateProfile(this).get(0).CandidatePreferredCity;

        if (list.isEmpty() || list == null) {
            noCityView.setVisibility(View.VISIBLE);
            mCityRecyclerView.setVisibility(View.GONE);
        } else {
            noCityView.setVisibility(View.GONE);
            mCityRecyclerView.setVisibility(View.VISIBLE);

            List<CityModel> cityModels = new ArrayList<>();

            for (int i = 0; i < list.size(); i++) {
                list.get(i).City.isSelected = true;
                cityModels.add(list.get(i).City);
            }

            cityAdapter = new CityAdapter(ProfileActivity.this, cityModels);
            mCityRecyclerView.setAdapter(cityAdapter);

            StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.HORIZONTAL);

            mCityRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        }


    }

    public void initExperienceSkillsChart() {
        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);
        mChart.getDescription().setEnabled(false);
        mChart.setMaxVisibleValueCount(100);
        mChart.setPinchZoom(false);
        mChart.setDrawGridBackground(false);
        mChart.getAxisRight().setEnabled(false);

        // INIT X AXIS
        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);

        // INIT Y AXIS
        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(5f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        leftAxis.setDrawGridLines(false);

        // INIT LEGENDS
        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(14f);

        setData(6, 100);
    }

    private void setData(int count, float range) {

        float start = 1f;

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

        for (int i = (int) start; i < start + count + 1; i++) {
            float mult = (range + 1);
            float val = (float) (Math.random() * mult);
            yVals1.add(new BarEntry(i, val));
        }

        BarDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, "Skill Set");
            set1.setColors(Color.parseColor("#66d99f"));

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.9f);

            mChart.setData(data);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void updateActionBarTransparency(float scrollRatio) {
        int newAlpha = (int) (scrollRatio * 255);
        mActionBarBackgroundDrawable.setAlpha(newAlpha);
        mToolbar.setBackground(mActionBarBackgroundDrawable);
    }

    private void updateStatusBarColor(float scrollRatio) {
        int r = interpolate(Color.red(mInitialStatusBarColor), Color.red(mFinalStatusBarColor), 1 - scrollRatio);
        int g = interpolate(Color.green(mInitialStatusBarColor), Color.green(mFinalStatusBarColor), 1 - scrollRatio);
        int b = interpolate(Color.blue(mInitialStatusBarColor), Color.blue(mFinalStatusBarColor), 1 - scrollRatio);
        mStatusBarManager.setTintColor(Color.rgb(r, g, b));
    }

    private void updateParallaxEffect(int scrollPosition) {
        float damping = 0.5f;
        int dampedScroll = (int) (scrollPosition * damping);
        int offset = mLastDampedScroll - dampedScroll;
        mHeader.offsetTopAndBottom(offset);

        mLastDampedScroll = dampedScroll;
    }

    private int interpolate(int from, int to, float param) {
        return (int) (from * param + to * (1 - param));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        MenuItem save = menu.findItem(R.id.profile_save);
        MenuItem profile = menu.findItem(R.id.profile_edit);

        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.profile_edit) {
            save.setVisible(true);
            profile.setVisible(false);
        } else if (id == R.id.profile_save) {
            save.setVisible(false);
            profile.setVisible(true);
        }

        return super.onOptionsItemSelected(item);
    }


}
