package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by varunbarve on 23/09/17.
 */

public class JobApplicationModel implements Parcelable {

    public int JobApplicationId;
    public String JobId;
    public String CandidateId;
    public int JobApplicationStatusId;
    public String StatusDate;
    public JobApplicationStatusModel JobApplicationStatus;
    public CandidateModel Candidate;


    protected JobApplicationModel (Parcel in) {
        JobApplicationId = in.readInt();
        JobId = in.readString();
        CandidateId = in.readString();
        JobApplicationStatusId = in.readInt();
        StatusDate = in.readString();
        JobApplicationStatus = in.readParcelable(JobApplicationStatusModel.class.getClassLoader());
        Candidate = in.readParcelable(CandidateModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeInt(JobApplicationId);
        dest.writeString(JobId);
        dest.writeString(CandidateId);
        dest.writeInt(JobApplicationStatusId);
        dest.writeString(StatusDate);
        dest.writeParcelable(JobApplicationStatus, flags);
        dest.writeParcelable(Candidate, flags);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<JobApplicationModel> CREATOR = new Creator<JobApplicationModel>() {
        @Override
        public JobApplicationModel createFromParcel (Parcel in) {
            return new JobApplicationModel(in);
        }

        @Override
        public JobApplicationModel[] newArray (int size) {
            return new JobApplicationModel[size];
        }
    };
}
