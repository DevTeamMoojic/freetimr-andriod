package com.freetimr.adapter;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.freetimr.fragments.JobListFragment;
import com.freetimr.model.JobCategoryModel;
import com.freetimr.model.JobsRequestModel;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 24-Mar-17.
 */

@RequiresApi(api = Build.VERSION_CODES.N)
public class JobsPagerAdapter extends FragmentStatePagerAdapter {

    ArrayList<JobCategoryModel> categoryModelsList;
    JobCategoryModel[] categoryModels;
    HashMap<Integer, ArrayList<JobsRequestModel>> jobMap;

    public JobsPagerAdapter(FragmentManager fm, JobCategoryModel[] categoryModels, HashMap<Integer, ArrayList<JobsRequestModel>> jobMap) {
        super(fm);
        this.categoryModels = categoryModels;
        this.jobMap = jobMap;
        categoryModelsList = new ArrayList<>();
        initAllData(categoryModels);
    }

    private void initAllData(JobCategoryModel[] categoryModels) {
        for (JobCategoryModel jobCategoryModel : categoryModels)
            if (jobMap.containsKey(jobCategoryModel.JobCategoryId))
                categoryModelsList.add(jobCategoryModel);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return categoryModelsList.get(position).JobCategoryName;
    }

    @Override
    public Fragment getItem(int position) {
        return JobListFragment.getInstance(jobMap.get(categoryModelsList.get(position).JobCategoryId));
    }

    @Override
    public int getCount() {
        return categoryModelsList.size();
    }
}

