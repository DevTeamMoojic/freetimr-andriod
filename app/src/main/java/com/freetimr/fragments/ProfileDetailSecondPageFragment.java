package com.freetimr.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.freetimr.R;
import com.freetimr.activities.ProfileDetailSecondPageActivity;
import com.freetimr.adapter.ProfileAvailableDayAdapter;
import com.freetimr.adapter.ProfileAvailableTimeAdapter;
import com.freetimr.adapter.ProfileWorkLocationAdapter;
import com.freetimr.model.CandidateAvailabilities;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.FreeTimrUtils;

import java.util.ArrayList;

/**
 * Created by Admin on 28-Dec-16.
 */

public class ProfileDetailSecondPageFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

    Button mProceedBtn;
    ProfileModel profileModel;
    RadioButton radio_btn_employer_location, radio_btn_field_work, radio_btn_work_from_home, radio_btn_weekdays, radio_btn_weekends, radio_btn_morning, radio_btn_afternoon, radio_btn_evening, radio_btn_night;
    RecyclerView workLocationRecyclerView, availableDayRecyclerView, availableTimeRecyclerView;

    int availabilityDayIdTemp, availabilityTimeIdTemp;

    ProfileWorkLocationAdapter profileWorkLocationAdapter;
    ProfileAvailableDayAdapter profileAvailableDayAdapter;
    ProfileAvailableTimeAdapter profileAvailableTimeAdapter;

    public static ProfileDetailSecondPageFragment getInstance(ProfileModel profileModel) {
        ProfileDetailSecondPageFragment fragment = new ProfileDetailSecondPageFragment();
        Bundle args = new Bundle();
        args.putParcelable("ProfileModel", profileModel);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        profileModel = getArguments().getParcelable("ProfileModel");
//        profileModel.CandidateAvailability = new ArrayList<>();
//                profileModel.CandidateAvailability = new CandidateAvailabilities[1];
//        profileModel.CandidateAvailability[0] = new CandidateAvailabilities();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_profile_detail_second_page, container, false);

        mProceedBtn = (Button) rootView.findViewById(R.id.proceed_btn);
        radio_btn_employer_location = (RadioButton) rootView.findViewById(R.id.radio_btn_employer_location);
        radio_btn_field_work = (RadioButton) rootView.findViewById(R.id.radio_btn_field_work);
        radio_btn_work_from_home = (RadioButton) rootView.findViewById(R.id.radio_btn_work_from_home);
        radio_btn_weekdays = (RadioButton) rootView.findViewById(R.id.radio_btn_weekdays);
        radio_btn_weekends = (RadioButton) rootView.findViewById(R.id.radio_btn_weekends);
        radio_btn_morning = (RadioButton) rootView.findViewById(R.id.radio_btn_morning);
        radio_btn_afternoon = (RadioButton) rootView.findViewById(R.id.radio_btn_afternoon);
        radio_btn_evening = (RadioButton) rootView.findViewById(R.id.radio_btn_evening);
        radio_btn_night = (RadioButton) rootView.findViewById(R.id.radio_btn_night);
        workLocationRecyclerView = (RecyclerView) rootView.findViewById(R.id.work_location_recycler_view);
        availableDayRecyclerView = (RecyclerView) rootView.findViewById(R.id.available_day_recycler_view);
        availableTimeRecyclerView = (RecyclerView) rootView.findViewById(R.id.available_time_recycler_view);

        radio_btn_employer_location.setOnCheckedChangeListener(this);
        radio_btn_field_work.setOnCheckedChangeListener(this);
        radio_btn_work_from_home.setOnCheckedChangeListener(this);
        radio_btn_weekdays.setOnCheckedChangeListener(this);
        radio_btn_weekends.setOnCheckedChangeListener(this);
        radio_btn_morning.setOnCheckedChangeListener(this);
        radio_btn_afternoon.setOnCheckedChangeListener(this);
        radio_btn_evening.setOnCheckedChangeListener(this);
        radio_btn_night.setOnCheckedChangeListener(this);

        profileWorkLocationAdapter = new ProfileWorkLocationAdapter(ProfileDetailSecondPageFragment.this.getActivity(), FreeTimrUtils.getCandidateWorkLocationList(ProfileDetailSecondPageFragment.this.getActivity()), this);
        workLocationRecyclerView.setAdapter(profileWorkLocationAdapter);
        workLocationRecyclerView.setLayoutManager(new LinearLayoutManager(ProfileDetailSecondPageFragment.this.getActivity(), LinearLayoutManager.VERTICAL, false));

        profileAvailableDayAdapter = new ProfileAvailableDayAdapter(ProfileDetailSecondPageFragment.this.getActivity(), FreeTimrUtils.getCandidateAvailableDayList(ProfileDetailSecondPageFragment.this.getActivity()), this);
        availableDayRecyclerView.setAdapter(profileAvailableDayAdapter);
        availableDayRecyclerView.setLayoutManager(new LinearLayoutManager(ProfileDetailSecondPageFragment.this.getActivity(), LinearLayoutManager.HORIZONTAL, false));

        profileAvailableTimeAdapter = new ProfileAvailableTimeAdapter(ProfileDetailSecondPageFragment.this.getActivity(), FreeTimrUtils.getCandidateAvailableTimeList(ProfileDetailSecondPageFragment.this.getActivity()), this);
        availableTimeRecyclerView.setAdapter(profileAvailableTimeAdapter);
        availableTimeRecyclerView.setLayoutManager(new LinearLayoutManager(ProfileDetailSecondPageFragment.this.getActivity(), LinearLayoutManager.VERTICAL, false));

        mProceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if (profileModel.WorkLocationId == 0 || profileModel.CandidateAvailability.get(0).AvailabilityDayId == 0 || profileModel.CandidateAvailability.get(0).AvailabilityTimeId == 0) {
//                    FreeTimrUtils.showMessage(ProfileDetailSecondPageFragment.this.getActivity(), "Please select Time and Location");
//                } else {
//                    addFragment(ProfileQualificationAndSkillsFragment.getInstance(profileModel));
//                }

                if (profileModel.WorkLocationId == 0 || availabilityDayIdTemp == 0 || availabilityTimeIdTemp == 0) {
                    FreeTimrUtils.showMessage(ProfileDetailSecondPageFragment.this.getActivity(), "Please select Time and Location");
                } else {
                    CandidateAvailabilities candidateAvailabilities = new CandidateAvailabilities();
                    candidateAvailabilities.AvailabilityDayId = availabilityDayIdTemp;
                    candidateAvailabilities.AvailabilityTimeId = availabilityTimeIdTemp;

                    profileModel.CandidateAvailability.add(candidateAvailabilities);
                    addFragment(ProfileQualificationAndSkillsFragment.getInstance(profileModel));
                }
            }
        });

        return rootView;
    }

    public void addFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        fragmentTransaction.replace(R.id.register_frame, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.radio_btn_employer_location:
                profileModel.WorkLocationId = 1;
                break;
            case R.id.radio_btn_field_work:
                profileModel.WorkLocationId = 2;
                break;
            case R.id.radio_btn_work_from_home:
                profileModel.WorkLocationId = 3;
                break;
            case R.id.radio_btn_weekdays:
                availabilityDayIdTemp = 2;
//                profileModel.CandidateAvaila bility.get(0).AvailabilityDayId = 2;
                break;
            case R.id.radio_btn_weekends:
                availabilityDayIdTemp = 3;
//                profileModel.CandidateAvailability.get(0).AvailabilityDayId = 3;
                break;
            case R.id.radio_btn_morning:
                availabilityTimeIdTemp = 1;
//                profileModel.CandidateAvailability.get(0).AvailabilityTimeId = 1;
                break;
            case R.id.radio_btn_afternoon:
                availabilityTimeIdTemp = 2;
//                profileModel.CandidateAvailability.get(0).AvailabilityTimeId = 2;
                break;
            case R.id.radio_btn_evening:
                availabilityTimeIdTemp = 3;
//                profileModel.CandidateAvailability.get(0).AvailabilityTimeId = 3;
                break;
            case R.id.radio_btn_night:
                availabilityTimeIdTemp = 4;
//                profileModel.CandidateAvailability.get(0).AvailabilityTimeId = 4;
                break;
        }
    }

    public void setWorkLocationId(int id) {
        profileModel.WorkLocationId = id;
    }

    public void setAvailabilityDayIdTemp(int id) {
        availabilityDayIdTemp = id;
    }

    public void setAvailabilityTimeIdTemp(int id) {
        availabilityTimeIdTemp = id;
    }
}
