package com.freetimr.activities;

import android.app.Dialog;
import android.media.Image;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.adapter.SkillAdapter;
import com.freetimr.adapter.SkillAutoCompleteAdapter;
import com.freetimr.adapter.SkillsAdapter;
import com.freetimr.adapter.SkillsNewAdapter;
import com.freetimr.model.CandidateSkills;
import com.freetimr.model.ProfileModel;
import com.freetimr.model.Skill;
import com.freetimr.model.SkillsModel;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SkillsActivity extends AppCompatActivity {

    RecyclerView skillsRecyclerView;
    SkillAdapter adapter;
    FloatingActionButton addSkills;
    ImageView done;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skills);

        skillsRecyclerView = (RecyclerView) findViewById(R.id.skills_rv);
        addSkills = (FloatingActionButton) findViewById(R.id.add_skills);
        done = (ImageView) findViewById(R.id.update_btn);

        addSkills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSkill();
            }
        });

        initSkillsAdapter();

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void initSkillsAdapter() {
        ArrayList<CandidateSkills> list = FreeTimrUtils.getCandidateProfile(this).get(0).CandidateSkill;
        adapter = new SkillAdapter(this, list, true);
        skillsRecyclerView.setAdapter(adapter);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        skillsRecyclerView.setLayoutManager(staggeredGridLayoutManager);
    }

    String skillId, skillYear, skillMonth, skillRating;

    public void addSkill() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(SkillsActivity.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.add_skill_dialog);

        AutoCompleteTextView skillNameAutoComplete = (AutoCompleteTextView) dialog.findViewById(R.id.skill_name_autocomplete);
        final EditText year = (EditText) dialog.findViewById(R.id.skill_year);
        final EditText month = (EditText) dialog.findViewById(R.id.skill_month);
        final DiscreteSeekBar ratings = (DiscreteSeekBar) dialog.findViewById(R.id.skills_percent);
        TextView cancel = (TextView) dialog.findViewById(R.id.skill_cancel);
        TextView add = (TextView) dialog.findViewById(R.id.skill_add);

        ArrayList<Skill> skills = FreeTimrUtils.getCandidateMasters(this).skills;
        final SkillAutoCompleteAdapter skillAutoCompleteAdapter = new SkillAutoCompleteAdapter(this, skills);
        skillNameAutoComplete.setAdapter(skillAutoCompleteAdapter);

        skillNameAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                skillId = "" + skillAutoCompleteAdapter.getItem(position).id;
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skillYear = year.getText().toString();
                skillMonth = month.getText().toString();
                skillRating = "" + ratings.getProgress();

                CandidateSkills candidateSkills = new CandidateSkills();
                candidateSkills.SkillExperienceInMonths = Integer.parseInt(skillMonth);
                candidateSkills.SkillExperienceInYears = Integer.parseInt(skillYear);
                candidateSkills.SkillId = Integer.parseInt(skillId);
                candidateSkills.SkillRating = Integer.parseInt(skillRating);

                FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(candidateSkills));

                addCandidateSkills(candidateSkills);

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void addCandidateSkills(CandidateSkills model) {
        FreeTimrUtils.showProgressDialog(this);
        String profileId = FreeTimrUtils.getCandidateProfile(SkillsActivity.this).get(0).CandidateProfileId;

        Call<CandidateSkills> call = FreeTimrUtils.getRetrofit().addCandidateSkills(FreeTimrURLs.HOST + FreeTimrURLs.ADD_SKILLS + profileId, model);
        call.enqueue(new Callback<CandidateSkills>() {
            @Override
            public void onResponse(Call<CandidateSkills> call, Response<CandidateSkills> response) {
                if (response.code() == 200) {
                    ArrayList<ProfileModel> profileModels = FreeTimrUtils.getCandidateProfile(SkillsActivity.this);
                    ArrayList<CandidateSkills> candidateSkillses = profileModels.get(0).CandidateSkill;

                    candidateSkillses.add(response.body());

                    FreeTimrUtils.saveCandidateProfile(SkillsActivity.this, FreeTimrUtils.newGson().toJson(profileModels));

                    initSkillsAdapter();
                } else {
                    FreeTimrUtils.showMessage(SkillsActivity.this, "Something went wrong. Please try again.");
                }
                FreeTimrUtils.cancelCurrentDialog(SkillsActivity.this);
            }

            @Override
            public void onFailure(Call<CandidateSkills> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
                FreeTimrUtils.cancelCurrentDialog(SkillsActivity.this);
            }
        });
    }
}
