package com.freetimr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.R;

import java.util.ArrayList;

/**
 * Created by Admin on 24-Dec-16.
 */

public class CompanySpecsAdapter extends RecyclerView.Adapter<CompanySpecsAdapter.CompanySpecsViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<String> list;

    public CompanySpecsAdapter(Context context, ArrayList<String> list) {
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public CompanySpecsAdapter.CompanySpecsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.company_specs_list_item, parent, false);
        return new CompanySpecsAdapter.CompanySpecsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CompanySpecsAdapter.CompanySpecsViewHolder holder, int position) {
        holder.name.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CompanySpecsViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        public CompanySpecsViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.company_specs_name);
        }
    }

}