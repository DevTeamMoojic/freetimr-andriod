package com.freetimr.model;

/**
 * Created by Admin on 23-Dec-16.
 */

public class SkillsModel {

    private int percent;
    private String name;

    public SkillsModel(int percent, String name) {
        this.percent = percent;
        this.name = name;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
