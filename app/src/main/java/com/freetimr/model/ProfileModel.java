package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Admin on 17-Feb-17.
 */

public class ProfileModel implements Parcelable {

    public String CandidateProfileId;
    public String CandidateId;
    public Date DateOfBirth;
    public String ProfileHeadline;
    public String Location;
    public String Pincode;
    public int OccupationId;
    public int ExperienceYears;
    public int ExperienceMonths;
    public int WorkLocationId;
    public ArrayList<CandidateAvailabilities> CandidateAvailability = new ArrayList<>();
    public ArrayList<CandidateEducations> CandidateEducation = new ArrayList<>();
    public ArrayList<CandidateExperiences> CandidateExperience = new ArrayList<>();
    public ArrayList<CandidateSkills> CandidateSkill = new ArrayList<>();
    public String CreatedDate;
    public String UpdatedDate;
    public ArrayList<CandidateProfileSkillRoleModel> CandidateSkillRole = new ArrayList<>();
    public ArrayList<CandidatePreferredCityModel> CandidatePreferredCity = new ArrayList<>();
    public ArrayList<JobApplicationModel> JobApplication = new ArrayList<>();

    public ProfileModel() {
    }

    protected ProfileModel(Parcel in) {
        CandidateProfileId = in.readString();
        CandidateId = in.readString();
        ProfileHeadline = in.readString();
        Location = in.readString();
        Pincode = in.readString();
        OccupationId = in.readInt();
        ExperienceYears = in.readInt();
        ExperienceMonths = in.readInt();
        WorkLocationId = in.readInt();
        CandidateAvailability = in.createTypedArrayList(CandidateAvailabilities.CREATOR);
        CandidateEducation = in.createTypedArrayList(CandidateEducations.CREATOR);
        CandidateExperience = in.createTypedArrayList(CandidateExperiences.CREATOR);
        CandidateSkill = in.createTypedArrayList(CandidateSkills.CREATOR);
        CreatedDate = in.readString();
        UpdatedDate = in.readString();
        CandidateSkillRole = in.createTypedArrayList(CandidateProfileSkillRoleModel.CREATOR);
        CandidatePreferredCity = in.createTypedArrayList(CandidatePreferredCityModel.CREATOR);
        JobApplication = in.createTypedArrayList(JobApplicationModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(CandidateProfileId);
        dest.writeString(CandidateId);
        dest.writeString(ProfileHeadline);
        dest.writeString(Location);
        dest.writeString(Pincode);
        dest.writeInt(OccupationId);
        dest.writeInt(ExperienceYears);
        dest.writeInt(ExperienceMonths);
        dest.writeInt(WorkLocationId);
        dest.writeTypedList(CandidateAvailability);
        dest.writeTypedList(CandidateEducation);
        dest.writeTypedList(CandidateExperience);
        dest.writeTypedList(CandidateSkill);
        dest.writeString(CreatedDate);
        dest.writeString(UpdatedDate);
        dest.writeTypedList(CandidateSkillRole);
        dest.writeTypedList(CandidatePreferredCity);
        dest.writeTypedList(JobApplication);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProfileModel> CREATOR = new Creator<ProfileModel>() {
        @Override
        public ProfileModel createFromParcel(Parcel in) {
            return new ProfileModel(in);
        }

        @Override
        public ProfileModel[] newArray(int size) {
            return new ProfileModel[size];
        }
    };
}
