package com.freetimr.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.SkillsModel;

import java.util.ArrayList;

/**
 * Created by Admin on 23-Dec-16.
 */

public class SkillsAdapter extends RecyclerView.Adapter<SkillsAdapter.SkillsViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<SkillsModel> list;

    public SkillsAdapter(Context context, ArrayList<SkillsModel> list) {
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SkillsAdapter.SkillsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.skills_progress_list_item, parent, false);
        return new SkillsAdapter.SkillsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SkillsAdapter.SkillsViewHolder holder, int position) {

        SkillsModel skillsModel = list.get(position);

        holder.name.setText(skillsModel.getName());
        holder.percent.setText(skillsModel.getPercent() + "%");
//        holder.progressBar.setProgress(skillsModel.getPercent());

        ObjectAnimator anim = ObjectAnimator.ofInt(holder.progressBar, "progress", 0, skillsModel.getPercent());
        anim.setDuration(150);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SkillsViewHolder extends RecyclerView.ViewHolder {

        TextView name, percent;
        ProgressBar progressBar;

        public SkillsViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.skills_name);
            percent = (TextView) itemView.findViewById(R.id.skills_percentage);
            progressBar = (ProgressBar) itemView.findViewById(R.id.skills_progress_bar);
        }
    }

}