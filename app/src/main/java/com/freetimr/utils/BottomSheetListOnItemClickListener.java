package com.freetimr.utils;

import java.io.Serializable;

/**
 * Created by Admin on 17-Feb-17.
 */

public interface BottomSheetListOnItemClickListener extends Serializable {

    void onListItemSelected(int position);
}
