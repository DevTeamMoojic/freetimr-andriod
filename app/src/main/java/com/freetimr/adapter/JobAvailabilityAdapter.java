package com.freetimr.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.AvailabilityTimeModel;

import java.util.ArrayList;

/**
 * Created by varunbarve on 29/04/17.
 */

public class JobAvailabilityAdapter extends RecyclerView.Adapter<JobAvailabilityAdapter.JobAvailabilityHolder>{

    Context context;
    ArrayList<AvailabilityTimeModel> list;
    LayoutInflater inflater;
    int pos = -1;
    int availabilityId = -1;

    public JobAvailabilityAdapter(Context context, ArrayList<AvailabilityTimeModel> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public JobAvailabilityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.work_location_type_list_item,parent,false);
        return new JobAvailabilityHolder(view);
    }

    @Override
    public void onBindViewHolder(final JobAvailabilityHolder holder, final int position) {
        AvailabilityTimeModel model = list.get(position);
        holder.name.setText(model.AvailabilityTimeName);

        if(this.pos==position){
            int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                holder.name.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.blue_curved_background));
            } else {
                holder.name.setBackground(context.getResources().getDrawable(R.drawable.blue_curved_background));
            }
            holder.name.setTextColor(Color.parseColor("#ffffff"));
        }else{
            holder.name.setBackgroundColor(Color.parseColor("#00000000"));
            holder.name.setTextColor(Color.parseColor("#4e5f6f"));
        }

        holder.itemView.setTag(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = (int)holder.itemView.getTag();
                availabilityId = list.get(pos).AvailabilityTimeId;
                onBindViewHolder(holder,position);
                notifyDataSetChanged();
            }
        });
    }

    public int getAvailabilityId(){
        return availabilityId;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class JobAvailabilityHolder extends RecyclerView.ViewHolder{

        TextView name;

        public JobAvailabilityHolder(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.work_loc_label);
        }
    }
}
