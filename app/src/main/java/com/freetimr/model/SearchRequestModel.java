package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 24-Mar-17.
 */

public class SearchRequestModel implements Parcelable{

    public String CityId;
    public String AvailabilityTimeId;
    public String WorkLocationId;
    public String KeywordTosearch;
    public String CandidateId;
    public String JobCategoryId;
    public boolean isFilteredApplied;

    public SearchRequestModel(){}

    protected SearchRequestModel (Parcel in) {
        CityId = in.readString();
        AvailabilityTimeId = in.readString();
        WorkLocationId = in.readString();
        KeywordTosearch = in.readString();
        CandidateId = in.readString();
        JobCategoryId = in.readString();
        isFilteredApplied = in.readByte() != 0;
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeString(CityId);
        dest.writeString(AvailabilityTimeId);
        dest.writeString(WorkLocationId);
        dest.writeString(KeywordTosearch);
        dest.writeString(CandidateId);
        dest.writeString(JobCategoryId);
        dest.writeByte((byte) (isFilteredApplied ? 1 : 0));
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<SearchRequestModel> CREATOR = new Creator<SearchRequestModel>() {
        @Override
        public SearchRequestModel createFromParcel (Parcel in) {
            return new SearchRequestModel(in);
        }

        @Override
        public SearchRequestModel[] newArray (int size) {
            return new SearchRequestModel[size];
        }
    };
}
