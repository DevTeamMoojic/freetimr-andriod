package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 17-Feb-17.
 */

public class CandidateAvailabilities implements Parcelable {

    public int AvailabilityDayId = 0;
    public int AvailabilityTimeId = 0;

    public CandidateAvailabilities() {
    }

    public CandidateAvailabilities(Parcel in) {
        AvailabilityDayId = in.readInt();
        AvailabilityTimeId = in.readInt();
    }

    public static final Creator<CandidateAvailabilities> CREATOR = new Creator<CandidateAvailabilities>() {
        @Override
        public CandidateAvailabilities createFromParcel(Parcel in) {
            return new CandidateAvailabilities(in);
        }

        @Override
        public CandidateAvailabilities[] newArray(int size) {
            return new CandidateAvailabilities[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(AvailabilityDayId);
        parcel.writeInt(AvailabilityTimeId);
    }
}
