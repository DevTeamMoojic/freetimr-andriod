package com.freetimr.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.freetimr.R;
import com.freetimr.model.CandidateModel;
import com.freetimr.utils.Constants;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;
import com.freetimr.utils.SharedPreferenceStore;
import com.freetimr.views.AnimatedCustomTextView;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    RadioButton mRadioButtonMale, mRadioButtonFemale;
    AnimatedCustomTextView mHelloLabel;
    LinearLayout mRegisterLoginContainer;
    Button mRegisterButton;
    EditText mNameTextView, mMobileNumberTextView, mEmailAddressTextView, mPasswordTextView;

    String candidateNameStr, candidateGenderStr, candidateMobileNumberStr, candidateEmailAddressStr, candidatePasswordStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        mNameTextView = (EditText) findViewById(R.id.candidate_name);
        mMobileNumberTextView = (EditText) findViewById(R.id.candidate_mobile_number);
        mEmailAddressTextView = (EditText) findViewById(R.id.candidate_email_address);
        mPasswordTextView = (EditText) findViewById(R.id.candidate_password);
        mRadioButtonMale = (RadioButton) findViewById(R.id.radio_btn_male);
        mRadioButtonFemale = (RadioButton) findViewById(R.id.radio_btn_female);
        mHelloLabel = (AnimatedCustomTextView) findViewById(R.id.hello_label);
        mRegisterLoginContainer = (LinearLayout) findViewById(R.id.register_login_container);
        mRegisterButton = (Button) findViewById(R.id.register_btn);

        final Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        mRadioButtonMale.setTypeface(tf);
        mRadioButtonFemale.setTypeface(tf);

        startTextAnimation(mHelloLabel, "Hello!");

        mRadioButtonMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                candidateGenderStr = "M";
            }
        });

        mRadioButtonFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                candidateGenderStr = "F";
            }
        });

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FreeTimrUtils.showProgressDialog(RegisterActivity.this);

                validateInput();
            }
        });

//        FreeTimrUtils.showProgressDialog(RegisterActivity.this);
    }

    private void validateInput() {
        candidateNameStr = mNameTextView.getText().toString();
        candidateMobileNumberStr = mMobileNumberTextView.getText().toString();
        candidateEmailAddressStr = mEmailAddressTextView.getText().toString();
        candidatePasswordStr = mPasswordTextView.getText().toString();

        if (FreeTimrUtils.notNull(candidateNameStr) || FreeTimrUtils.notNull(candidateEmailAddressStr) || FreeTimrUtils.notNull(candidateMobileNumberStr) || FreeTimrUtils.notNull(candidateGenderStr) || FreeTimrUtils.notNull(candidatePasswordStr)) {
            if (FreeTimrUtils.isValidEmailId(candidateEmailAddressStr)) {

                CandidateModel candidateModel = new CandidateModel();
                candidateModel.Name = candidateNameStr;
                candidateModel.EmailId = candidateEmailAddressStr;
                candidateModel.MobileNumber = candidateMobileNumberStr;
                candidateModel.Gender = candidateGenderStr;
                candidateModel.Password = candidatePasswordStr;

                makeCandidateRegisterAPICall(candidateModel);
            } else {
                FreeTimrUtils.showMessage(RegisterActivity.this, "Please enter a valid Email Address");
            }
        } else {
            FreeTimrUtils.showMessage(RegisterActivity.this, "Please fill all the fields");
        }
    }

    private void makeCandidateRegisterAPICall(CandidateModel candidateModel) {

        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(candidateModel));

        Call<String> registerCandidate = FreeTimrUtils.getRetrofit().registerCandidate(candidateModel);
        registerCandidate.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                FreeTimrUtils.log("" + response.code());
                FreeTimrUtils.log("" + response.body());

                if (response.code() == Constants.RESPONSE_CREATED) {
                    getCandidateAPICall(response.body());
                    registerWithSendBird(response.body());
                } else if (response.code() == Constants.RESPONSE_BAD_REQUEST) {
                    try {
                        ResponseBody responseBody = response.errorBody();
                        FreeTimrUtils.showMessage(RegisterActivity.this, FreeTimrUtils.getErrorMessage(responseBody.string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                FreeTimrUtils.cancelCurrentDialog(RegisterActivity.this);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
            }
        });
    }

    private void getCandidateAPICall(String id) {
        Call<CandidateModel> candidateModelCall = FreeTimrUtils.getRetrofit().getCandidate(FreeTimrURLs.GET_CANDIDATE + "/" + id);
        candidateModelCall.enqueue(new Callback<CandidateModel>() {
            @Override
            public void onResponse(Call<CandidateModel> call, Response<CandidateModel> response) {
                FreeTimrUtils.log("" + response.code());
                FreeTimrUtils.log("" + response.body());

                if (response.code() == Constants.RESPONSE_SUCCESS)
                    saveCandidateInfo(FreeTimrUtils.newGson().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<CandidateModel> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
            }
        });
    }

    /**
     * Save candidate info in SharedPreferences
     *
     * @param candidateInfo JSON String for candidate info
     */
    private void saveCandidateInfo(String candidateInfo) {
        if (candidateInfo != null)
            SharedPreferenceStore.storeValue(RegisterActivity.this, Constants.KEY_CANDIDATE_INFO, candidateInfo);

        FreeTimrUtils.cancelCurrentDialog(RegisterActivity.this);

        Intent intent = new Intent(RegisterActivity.this, OTPActivity.class);
        startActivity(intent);
        finish();
    }

    private void startTextAnimation(AnimatedCustomTextView textView, String text) {
        textView.setCharacterDelay(200);
        textView.animateText(text);

        slideUpRegisterLoginContainer(textView.getNextAnimationOffset());
    }

    private void slideUpRegisterLoginContainer(int offset) {
        Animation slideUp = AnimationUtils.loadAnimation(RegisterActivity.this, R.anim.bottom_up);
        slideUp.setStartOffset(offset + 500);
        mRegisterLoginContainer.startAnimation(slideUp);
        mRegisterLoginContainer.setVisibility(View.VISIBLE);
    }

    private void registerWithSendBird(String userID) {
        SendBird.connect(userID, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                if (e != null) {
                    // Error.
                    return;
                } else {
                    FreeTimrUtils.log("" + user.getProfileUrl());
                }
            }
        });
    }
}
