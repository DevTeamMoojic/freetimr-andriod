package com.freetimr.model;

import com.google.gson.annotations.SerializedName;

public class GradingSystem {
    @SerializedName("GradingSystemId")
    public int id;
    @SerializedName("GradingSystemName")
    public String name;
}
