package com.freetimr.activities;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.freetimr.R;
import com.freetimr.adapter.CandidateLanguageAdapter;
import com.freetimr.model.CityModel;
import com.freetimr.model.LanguageModel;
import com.freetimr.utils.FreeTimrUtils;

import java.util.ArrayList;

public class CandidateLanguageActivity extends AppCompatActivity {

    ImageView updateCandidateCancel, updateCandidateBtn;
    RecyclerView candidateLanguageRecyclerView;

    ArrayList<CityModel> locationsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_language);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        locationsList = FreeTimrUtils.getCityList(this);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");

        updateCandidateBtn = (ImageView) findViewById(R.id.update_candidate_btn);
        updateCandidateCancel = (ImageView) findViewById(R.id.update_candidate_cancel);
        candidateLanguageRecyclerView = (RecyclerView) findViewById(R.id.candidate_language_recycler_view);

        ArrayList<LanguageModel> list = FreeTimrUtils.getLanguageList(this);
        CandidateLanguageAdapter adapter = new CandidateLanguageAdapter(this, list);
        candidateLanguageRecyclerView.setAdapter(adapter);
        candidateLanguageRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        updateCandidateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
