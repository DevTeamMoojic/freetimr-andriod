package com.freetimr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.LanguageModel;

import java.util.ArrayList;

/**
 * Created by varunbarve on 01/02/18.
 */

public class CandidateLanguageAdapter extends RecyclerView.Adapter<CandidateLanguageAdapter.CandidateLanguageViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<LanguageModel> list;

    public CandidateLanguageAdapter(Context context, ArrayList<LanguageModel> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public CandidateLanguageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.candidate_language_list_item, parent, false);
        return new CandidateLanguageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CandidateLanguageViewHolder holder, int position) {
        LanguageModel model = list.get(position);
        holder.langugage.setText("" + model.LanguageName);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CandidateLanguageViewHolder extends RecyclerView.ViewHolder {

        TextView langugage;
        CheckBox checkBox;

        public CandidateLanguageViewHolder(View itemView) {
            super(itemView);

            langugage = (TextView) itemView.findViewById(R.id.candidate_language_label);
            checkBox = (CheckBox) itemView.findViewById(R.id.candidate_language_checkbox);
        }
    }

    public class LanguageUpdateModel {
        public int CandidateLanguageId;
        public int LanguageId;
    }

    private void updateLanguage() {

    }
}
