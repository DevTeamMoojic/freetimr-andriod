package com.freetimr.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.fragments.ProfileQualificationAndSkillsFragment;
import com.freetimr.model.CandidateModel;
import com.freetimr.model.LoginRequestModel;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.Constants;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;
import com.freetimr.utils.SharedPreferenceStore;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText emailEdit, passwordEdit;
    Button login;
    TextView signUp;
    TextView skip;

    String emailStr, passwordStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration);

        emailEdit = (EditText) findViewById(R.id.candidate_login_email);
        passwordEdit = (EditText) findViewById(R.id.candidate_login_password);
        login = (Button) findViewById(R.id.login_btn);
        signUp = (TextView) findViewById(R.id.sign_up_btn);
        skip = (TextView) findViewById(R.id.skip_login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailStr = emailEdit.getText().toString();
                passwordStr = passwordEdit.getText().toString();
                if (emailStr.isEmpty() || passwordStr.isEmpty()) {
                    FreeTimrUtils.showMessage(LoginActivity.this, "Please enter all the fields");
                } else {
                    if (FreeTimrUtils.isValidEmailId(emailStr)) {
//                        FreeTimrUtils.log(emailStr+" : "+passwordStr);
                        loginUser(emailStr, passwordStr);
                    } else {
                        FreeTimrUtils.showMessage(LoginActivity.this, "Please enter a valid email address");
                    }
                }

            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void loginUser(String emailAddress, String password) {
        FreeTimrUtils.showProgressDialog(LoginActivity.this);
        LoginRequestModel login = new LoginRequestModel();
        login.EmailId = emailAddress;
        login.Passcode = password;

        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(login));

        Call<CandidateModel> loginCall = FreeTimrUtils.getRetrofit().login(login);
        loginCall.enqueue(new Callback<CandidateModel>() {
            @Override
            public void onResponse(Call<CandidateModel> call, Response<CandidateModel> response) {
                if (response.code() == 200) {
                    String candidateModel = FreeTimrUtils.newGson().toJson(response.body());
                    saveCandidateInfo(candidateModel);
                    getUserProfile(response.body().getId());
                } else if (response.code() == 404) {
                    FreeTimrUtils.showMessage(LoginActivity.this, "Incorrect email address or password.");
                    FreeTimrUtils.cancelCurrentDialog(LoginActivity.this);
                } else {
                    FreeTimrUtils.showMessage(LoginActivity.this, "Something went wrong. Please try again.");
                    FreeTimrUtils.cancelCurrentDialog(LoginActivity.this);
                }
//                FreeTimrUtils.cancelCurrentDialog(LoginActivity.this);
            }

            @Override
            public void onFailure(Call<CandidateModel> call, Throwable t) {
                FreeTimrUtils.log("" + t.getLocalizedMessage());
                FreeTimrUtils.cancelCurrentDialog(LoginActivity.this);
            }
        });
    }

    public void getUserProfile(String candidateId) {
        Call<ProfileModel[]> profileCall = FreeTimrUtils.getRetrofit().getCandidateProfile(FreeTimrURLs.HOST + FreeTimrURLs.GET_PROFILE + "/" + candidateId);
        profileCall.enqueue(new Callback<ProfileModel[]>() {
            @Override
            public void onResponse(Call<ProfileModel[]> call, Response<ProfileModel[]> response) {
                FreeTimrUtils.log("" + FreeTimrUtils.newGson().toJson(response.body()));
                if (response.body().length != 0) {
                    FreeTimrUtils.saveCandidateProfile(LoginActivity.this, FreeTimrUtils.newGson().toJson(response.body()));
                    SharedPreferenceStore.storeValue(LoginActivity.this, Constants.IS_RPOFILE_COMPLETE, true);
                    switchToNextScreen();
                } else {
                    Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                    startActivity(intent);
                    finish();
                    FreeTimrUtils.cancelCurrentDialog(LoginActivity.this);
                }
            }

            @Override
            public void onFailure(Call<ProfileModel[]> call, Throwable t) {
                FreeTimrUtils.log("" + t.getLocalizedMessage());
            }
        });
    }

    private void saveCandidateInfo(String candidateInfo) {
        if (candidateInfo != null)
            SharedPreferenceStore.storeValue(LoginActivity.this, Constants.KEY_CANDIDATE_INFO, candidateInfo);
    }

    private void switchToNextScreen() {
        FreeTimrUtils.cancelCurrentDialog(LoginActivity.this);

        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

}
