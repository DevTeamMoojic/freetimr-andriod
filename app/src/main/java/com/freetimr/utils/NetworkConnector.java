package com.freetimr.utils;

import com.freetimr.activities.CandidateNameDesignationActivity;
import com.freetimr.model.ApplyJobModel;
import com.freetimr.model.CandidateEducations;
import com.freetimr.model.CandidateExperiences;
import com.freetimr.model.CandidateMastersModel;
import com.freetimr.model.CandidateModel;
import com.freetimr.model.CandidatePreferredCityModel;
import com.freetimr.model.CandidateSkillRoleModel;
import com.freetimr.model.CandidateSkills;
import com.freetimr.model.CityModel;
import com.freetimr.model.CompanyModel;
import com.freetimr.model.CreateSkillRoleModel;
import com.freetimr.model.JobMastersModel;
import com.freetimr.model.JobsRequestModel;
import com.freetimr.model.LoginRequestModel;
import com.freetimr.model.ProfileModel;
import com.freetimr.model.SearchRequestModel;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;


/**
 * Created by Admin on 09-Feb-17.
 */

public interface NetworkConnector {

    @POST("Candidate/Register")
    @Headers({"Content-Type: application/json"})
    Call<String> registerCandidate(@Body CandidateModel requestParams);

    @GET()
    Call<CandidateModel> getCandidate(@Url String url);

    @GET()
    Call<CandidateMastersModel> getCandidateMasters(@Url String url);

    @GET
    Call<JobMastersModel> getJobMaster(@Url String url);

    @GET
    Call<CompanyModel> getCompanyModel(@Url String url);

    @POST("Candidate/Profile/Create")
    @Headers({"Content-Type: application/json"})
    Call<String> registerCandidateProfile(@Body ProfileModel profileModel);

    @PUT("Candidate/Profile/Update")
    Call<String> updateCandidateProfile(@Body ProfileModel profileModel);

    @POST("Candidate/SearchJob")
    @Headers({"Content-Type: application/json"})
    Call<JobsRequestModel[]> searchJob(@Body SearchRequestModel searchRequestModel);

    @GET
    Call<ApplyJobModel> applyJob(@Url String url);

    @GET
    Call<JobsRequestModel[]> getAppliedJobs(@Url String url);

    @POST("RegisterMobile")
    Call<String> sendOtp(@Body HashMap<String, String> otpModel);

    @POST("RegisterWithOTP")
    Call<String> verifyOtp(@Body HashMap<String, String> otpModel);

    @POST("Candidate/AuthCandidate")
    Call<CandidateModel> login(@Body LoginRequestModel loginRequestModel);

    @GET()
    Call<ProfileModel[]> getCandidateProfile(@Url String url);

    @POST
    Call<CandidatePreferredCityModel> addCandidateCity(@Url String url, @Body CityModel model);

    @DELETE
    Call<Integer> deleteCandidateCity(@Url String url);

    @PUT("Candidate/Candidate/Update")
    Call<CandidateModel> updateCandidate(@Body CandidateNameDesignationActivity.CandidateUpdateRequestModel body);

    @POST()
    Call<CandidateExperiences> addCandidateWorkExperience(@Url String url, @Body CandidateExperiences body);

    @PUT("Candidate/Profile/CandidateExperience/Update")
    Call<CandidateExperiences> updateCandidateWorkExperience(@Body CandidateExperiences body);

    @DELETE
    Call<Integer> deleteCandidateWorkExperience(@Url String url);

    @POST()
    Call<CandidateEducations> addCandidateEducation(@Url String url, @Body CandidateEducations body);

    @DELETE
    Call<Integer> deleteCandidateEducation(@Url String url);

    @PUT("Candidate/Profile/CandidateEducation/Update")
    Call<CandidateEducations> updateCandidateEducation(@Body CandidateEducations body);

    @POST()
    Call<CandidateSkills> addCandidateSkills(@Url String url, @Body CandidateSkills body);

    @PUT("Candidate/Profile/CandidateSkill/Update")
    Call<CandidateSkills> updateCandidateSkill(@Body CandidateSkills body);

    @DELETE
    Call<Integer> deleteCandidateSkill(@Url String url);

    @POST()
    Call<String> createSkillRole(@Url String url, @Body CreateSkillRoleModel model);
}
