package com.freetimr.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.freetimr.R;
import com.wang.avi.AVLoadingIndicatorView;

/**
 * Created by Admin on 09-Feb-17.
 */

public class LoadingDialogBox extends Dialog {

    public Context context;
    public Dialog dialog;
    String message;
    AVLoadingIndicatorView avi;

    public LoadingDialogBox(Context context) {
        super(context);
        this.context = context;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.loading_dialog_box);
        if (message != null) {
            ((TextView) findViewById(R.id.loader_text_view)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.loader_text_view)).setText(message);
        } else {
            ((TextView) findViewById(R.id.loader_text_view)).setVisibility(View.GONE);
        }

        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);
        avi.show();
    }
}
