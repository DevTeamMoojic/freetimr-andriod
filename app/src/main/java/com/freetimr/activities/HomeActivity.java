package com.freetimr.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.adapter.HomeRecyclerViewAdapter;
import com.freetimr.model.CandidateModel;
import com.freetimr.model.HomeModel;
import com.freetimr.model.JobCategoryModel;
import com.freetimr.utils.Constants;
import com.freetimr.utils.FreeTimrUtils;
import com.freetimr.utils.SharedPreferenceStore;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    HomeRecyclerViewAdapter adapter;
    RecyclerView mHomeRecyclerView;
    View mNavHeader;
    RelativeLayout mJobApplications, mMessages, mJobSearch, mProfile, mLogout, mHomeContainer;
    TextView candidateNameTextView, candidateHeadlineTextView;
    LinearLayout profileHeader;

    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;

    CandidateModel candidateModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        candidateModel = FreeTimrUtils.getCandidateInfo(HomeActivity.this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mNavHeader = navigationView.findViewById(R.id.nav_header);
        mHomeContainer = (RelativeLayout) navigationView.findViewById(R.id.home_container);
        mJobApplications = (RelativeLayout) navigationView.findViewById(R.id.menu_job_applications_container);
        mMessages = (RelativeLayout) navigationView.findViewById(R.id.menu_messages_container);
        mJobSearch = (RelativeLayout) navigationView.findViewById(R.id.menu_job_search_container);
        mProfile = (RelativeLayout) navigationView.findViewById(R.id.menu_profile_container);
        mLogout = (RelativeLayout) navigationView.findViewById(R.id.menu_logout_container);
        candidateNameTextView = (TextView) navigationView.findViewById(R.id.user_name);
        candidateHeadlineTextView = (TextView) navigationView.findViewById(R.id.user_headline);
        profileHeader = (LinearLayout) navigationView.findViewById(R.id.nav_profile_header_container);

        if (candidateModel != null)
            candidateNameTextView.setText(candidateModel.getName());

        if (FreeTimrUtils.getCandidateProfile(this) != null) {
            candidateHeadlineTextView.setText(FreeTimrUtils.getCandidateProfile(this).get(0).ProfileHeadline);
        } else {
            candidateHeadlineTextView.setVisibility(View.GONE);
        }

        mNavHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (candidateModel != null) {
                    Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (candidateModel != null) {
                    Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        mJobApplications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (candidateModel != null) {
                    Intent intent = new Intent(HomeActivity.this, JobApplicationsActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        mMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, MessagesActivity.class);
                startActivity(intent);
            }
        });

        mJobSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, BrowseJobActivity.class);
                startActivity(intent);
            }
        });

        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceStore.deleteValue(HomeActivity.this, Constants.IS_RPOFILE_COMPLETE);
                SharedPreferenceStore.deleteValue(HomeActivity.this, Constants.KEY_CANDIDATE_PROFILE);
                SharedPreferenceStore.deleteValue(HomeActivity.this, Constants.KEY_CANDIDATE_INFO);
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mHomeContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        mHomeRecyclerView = (RecyclerView) findViewById(R.id.home_recycler_view);

        JobCategoryModel[] jobCategoryModels = FreeTimrUtils.getJobMasters(HomeActivity.this).JobCategory;
        adapter = new HomeRecyclerViewAdapter(HomeActivity.this, jobCategoryModels);
        mHomeRecyclerView.setAdapter(adapter);
        mHomeRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.home_search) {
            Intent intent = new Intent(HomeActivity.this, BrowseJobActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
