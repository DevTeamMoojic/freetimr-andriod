package com.freetimr.utils;

/**
 * Created by Admin on 22-Dec-16.
 */

public interface ObservableScrollable {
    void setOnScrollChangedCallback(OnScrollChangedCallback callback);
}
