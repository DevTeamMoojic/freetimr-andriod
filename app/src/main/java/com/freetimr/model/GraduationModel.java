package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class GraduationModel implements Parcelable {

    public int GraduationId;
    public String GraduationName;

    protected GraduationModel(Parcel in) {
        GraduationId = in.readInt();
        GraduationName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(GraduationId);
        dest.writeString(GraduationName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GraduationModel> CREATOR = new Creator<GraduationModel>() {
        @Override
        public GraduationModel createFromParcel(Parcel in) {
            return new GraduationModel(in);
        }

        @Override
        public GraduationModel[] newArray(int size) {
            return new GraduationModel[size];
        }
    };
}
