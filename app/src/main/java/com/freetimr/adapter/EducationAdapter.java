package com.freetimr.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.activities.AddEducationQualificationActivity;
import com.freetimr.activities.AddWorkExperienceActivity;
import com.freetimr.activities.EducationActivity;
import com.freetimr.activities.WorkExperienceActivity;
import com.freetimr.model.CandidateEducations;
import com.freetimr.model.CandidateExperiences;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;

import java.io.EOFException;
import java.util.ArrayList;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 21-Feb-17.
 */

public class EducationAdapter extends RecyclerView.Adapter<EducationAdapter.EducationViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<CandidateEducations> candidateEducations;
    EducationActivity educationActivity;

    public EducationAdapter (Context context, ArrayList<CandidateEducations> candidateEducations) {
        this.candidateEducations = candidateEducations;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public EducationAdapter (Context context, ArrayList<CandidateEducations> candidateEducations, EducationActivity educationActivity) {
        this.candidateEducations = candidateEducations;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.educationActivity = educationActivity;
    }

    @Override
    public EducationAdapter.EducationViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.education_list_item, parent, false);
        return new EducationAdapter.EducationViewHolder(view);
    }

    @Override
    public void onBindViewHolder (EducationAdapter.EducationViewHolder holder, int position) {
        final CandidateEducations model = candidateEducations.get(position);

        holder.degreeName.setText(FreeTimrUtils.getGraduationNameById(context, model.GraduationId));
        holder.specialization.setText("Specialization: " + FreeTimrUtils.getSpecializationById(context, model.SpecializationId));
        if (model.EducationMode.equalsIgnoreCase("F")) {
            holder.educationMode.setText("Full-Time");
        }
        holder.university.setText(model.InstituteName + ", Passed out in " + model.GraduatedYear);
        holder.grade.setText(model.Marks + " " + FreeTimrUtils.getGradingSystemById(context, model.GradingSystem));

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                deleteEducation(model.CandidateEducationId);
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(context, AddEducationQualificationActivity.class);
                intent.putExtra("IS_SINGLE_ENTRY",true);
                intent.putExtra("IS_UPDATE",true);
                intent.putExtra("CANDIDATE_EDUCATION",model);
                educationActivity.startActivityForResult(intent, EducationActivity.ADD_EDUCATION);
            }
        });
    }

    @Override
    public int getItemCount () {
        return candidateEducations.size();
    }

    class EducationViewHolder extends RecyclerView.ViewHolder {

        TextView degreeName, specialization, university, educationMode, grade;
        ImageView delete, edit;

        public EducationViewHolder (View itemView) {
            super(itemView);

            degreeName = (TextView) itemView.findViewById(R.id.education_degree_name_text_view);
            specialization = (TextView) itemView.findViewById(R.id.education_specialization_text_view);
            educationMode = (TextView) itemView.findViewById(R.id.education_mode_text_view);
            university = (TextView) itemView.findViewById(R.id.education_university_text_view);
            grade = (TextView) itemView.findViewById(R.id.education_grade_text_view);
            delete = (ImageView) itemView.findViewById(R.id.education_delete);
            edit = (ImageView) itemView.findViewById(R.id.education_edit);
        }
    }

    private void deleteEducation (final int candidateEducationId) {
        Call<Integer> call = FreeTimrUtils.getRetrofit().deleteCandidateEducation(FreeTimrURLs.HOST + FreeTimrURLs.DELETE_EDUCATION + candidateEducationId);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse (Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    deleteFromLocalStorage(candidateEducationId);
                } else {
                    FreeTimrUtils.showMessage(context, "Something went wrong. Please try again later.");
                }
            }

            @Override
            public void onFailure (Call<Integer> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
                if (t instanceof EOFException) {
                    deleteFromLocalStorage(candidateEducationId);
                }
            }
        });
    }

    private void deleteFromLocalStorage (int candidateEducationId) {
        ArrayList<CandidateEducations> candidateEducationsArrayList = new ArrayList<>(FreeTimrUtils.getCandidateProfile(context).get(0).CandidateEducation);
        for (Iterator<CandidateEducations> iterator = candidateEducationsArrayList.iterator(); iterator.hasNext(); ) {
            CandidateEducations model = iterator.next();
            if (model.CandidateEducationId == candidateEducationId) {
                iterator.remove();
            }
        }

        ArrayList<ProfileModel> profileModels = FreeTimrUtils.getCandidateProfile(context);
        profileModels.get(0).CandidateEducation= candidateEducationsArrayList;

        FreeTimrUtils.saveCandidateProfile(context, FreeTimrUtils.newGson().toJson(profileModels));

        educationActivity.setEducationAdapter(FreeTimrUtils.getCandidateProfile(context).get(0).CandidateEducation);
    }
}

