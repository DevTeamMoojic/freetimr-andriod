package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class WorkLocationModel implements Parcelable {

    public int WorkLocationId;
    public String WorkLocationName;
    public boolean isSelected;

    protected WorkLocationModel(Parcel in) {
        WorkLocationId = in.readInt();
        WorkLocationName = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(WorkLocationId);
        dest.writeString(WorkLocationName);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WorkLocationModel> CREATOR = new Creator<WorkLocationModel>() {
        @Override
        public WorkLocationModel createFromParcel(Parcel in) {
            return new WorkLocationModel(in);
        }

        @Override
        public WorkLocationModel[] newArray(int size) {
            return new WorkLocationModel[size];
        }
    };
}
