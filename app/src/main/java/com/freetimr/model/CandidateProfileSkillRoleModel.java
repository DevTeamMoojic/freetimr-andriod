package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by varunbarve on 16/02/18.
 */

public class CandidateProfileSkillRoleModel implements Parcelable{

    public int CandidateSkillRoleId;
    public CandidateSkillRoleModel SkillRole;

    public CandidateProfileSkillRoleModel(int candidateSkillRoleId, CandidateSkillRoleModel skillRole) {
        CandidateSkillRoleId = candidateSkillRoleId;
        SkillRole = skillRole;
    }

    protected CandidateProfileSkillRoleModel(Parcel in) {
        CandidateSkillRoleId = in.readInt();
        SkillRole = in.readParcelable(CandidateSkillRoleModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(CandidateSkillRoleId);
        dest.writeParcelable(SkillRole, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CandidateProfileSkillRoleModel> CREATOR = new Creator<CandidateProfileSkillRoleModel>() {
        @Override
        public CandidateProfileSkillRoleModel createFromParcel(Parcel in) {
            return new CandidateProfileSkillRoleModel(in);
        }

        @Override
        public CandidateProfileSkillRoleModel[] newArray(int size) {
            return new CandidateProfileSkillRoleModel[size];
        }
    };
}
