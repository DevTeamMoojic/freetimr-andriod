package com.freetimr.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.adapter.CompanySpecsAdapter;
import com.freetimr.adapter.PerksAdapter;
import com.freetimr.model.CompanyModel;
import com.freetimr.utils.FreeTimrUtils;
import com.freetimr.utils.ObservableScrollable;
import com.freetimr.utils.OnScrollChangedCallback;
import com.freetimr.utils.SystemBarTintManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CompanyProfileActivity extends AppCompatActivity {

    private Drawable mActionBarBackgroundDrawable;
    Toolbar mToolbar;
    private View mHeader;
    private int mLastDampedScroll;
    private int mInitialStatusBarColor;
    private int mFinalStatusBarColor;
    private SystemBarTintManager mStatusBarManager;

    RecyclerView mCompanySpecsRecyclerView, mPerksRecyclerView;
    CompanySpecsAdapter companySpecsAdapter;
    PerksAdapter perksAdapter;
    CompanyModel companyModel;
    LinearLayout websiteContainer;

    TextView companyName, companyCity, companyDescription, companyWebsite;
    ImageView companyProfileImage;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_profile);
        mToolbar = (Toolbar) findViewById(R.id.company_profile_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mActionBarBackgroundDrawable = getResources().getDrawable(R.color.background_color_toolbar);

        mStatusBarManager = new SystemBarTintManager(this);
        mStatusBarManager.setStatusBarTintEnabled(true);
        mInitialStatusBarColor = Color.TRANSPARENT;
        mFinalStatusBarColor = getResources().getColor(R.color.background_color_toolbar);

        ObservableScrollable scrollView = (ObservableScrollable) findViewById(R.id.company_profile_scrollview);
        mHeader = findViewById(R.id.profile_header);

        scrollView.setOnScrollChangedCallback(new OnScrollChangedCallback() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onScroll (int l, int scrollPosition) {
                int headerHeight = mHeader.getHeight() - mToolbar.getHeight();
                float ratio = 0;
                if (scrollPosition > 0 && headerHeight > 0)
                    ratio = (float) Math.min(Math.max(scrollPosition, 0), headerHeight) / headerHeight;

                updateActionBarTransparency(ratio);
                updateStatusBarColor(ratio);
                updateParallaxEffect(scrollPosition);
            }
        });

        companyModel = getIntent().getParcelableExtra("CompanyModel");

        companyName = (TextView) findViewById(R.id.company_name);
        companyCity = (TextView) findViewById(R.id.company_city);
        companyDescription = (TextView) findViewById(R.id.company_description);
        companyWebsite = (TextView) findViewById(R.id.company_website);
        companyProfileImage = (ImageView) findViewById(R.id.company_profile_image);
        websiteContainer = (LinearLayout) findViewById(R.id.website_container);

        if (companyModel != null) {
            companyName.setText(companyModel.CompanyName);
            companyCity.setText(FreeTimrUtils.getCityNameById(this, companyModel.CityId));
            companyDescription.setText(companyModel.CompanyDescription);
            companyWebsite.setText(companyModel.Website);
            Picasso.with(this).load("http://137.59.54.53/FreeTimr/Images/Company/" + companyModel.CompanyImage).placeholder(R.drawable.recruiter_default).into(companyProfileImage);
        }

        websiteContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (!companyModel.Website.startsWith("http://") && !companyModel.Website.startsWith("https://"))
                    companyModel.Website = "http://" + companyModel.Website;

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(companyModel.Website));
                startActivity(browserIntent);
            }
        });

        mCompanySpecsRecyclerView = (RecyclerView) findViewById(R.id.company_specs_recycler_view);
        initCompanySpecs();

        mPerksRecyclerView = (RecyclerView) findViewById(R.id.perks_recycler_view);
        initPerksAndBenefits();
    }

    public void initCompanySpecs () {
        ArrayList<String> companySpecs = new ArrayList<>();
        companySpecs.add("6 Days a Week");
        companySpecs.add("Casual Dress");
        companySpecs.add("9am - 6pm");
        companySpecs.add("Cafeteria");
        companySpecs.add("Hybrid");

        companySpecsAdapter = new CompanySpecsAdapter(CompanyProfileActivity.this, companySpecs);
        mCompanySpecsRecyclerView.setAdapter(companySpecsAdapter);
        mCompanySpecsRecyclerView.setLayoutManager(new LinearLayoutManager(CompanyProfileActivity.this, LinearLayoutManager.HORIZONTAL, false));
    }

    public void initPerksAndBenefits () {
        ArrayList<String> perksList = new ArrayList<>();
        perksList.add("Flexible Timings");
        perksList.add("Stock Options");
        perksList.add("Equity");
        perksList.add("Performance Bonus");
        perksList.add("Performance Bonus");

        perksAdapter = new PerksAdapter(CompanyProfileActivity.this, perksList);
        mPerksRecyclerView.setAdapter(perksAdapter);
        mPerksRecyclerView.setLayoutManager(new LinearLayoutManager(CompanyProfileActivity.this, LinearLayoutManager.HORIZONTAL, false));
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void updateActionBarTransparency (float scrollRatio) {
        int newAlpha = (int) (scrollRatio * 255);
        mActionBarBackgroundDrawable.setAlpha(newAlpha);
        mToolbar.setBackground(mActionBarBackgroundDrawable);
    }

    private void updateStatusBarColor (float scrollRatio) {
        int r = interpolate(Color.red(mInitialStatusBarColor), Color.red(mFinalStatusBarColor), 1 - scrollRatio);
        int g = interpolate(Color.green(mInitialStatusBarColor), Color.green(mFinalStatusBarColor), 1 - scrollRatio);
        int b = interpolate(Color.blue(mInitialStatusBarColor), Color.blue(mFinalStatusBarColor), 1 - scrollRatio);
        mStatusBarManager.setTintColor(Color.rgb(r, g, b));
    }

    private void updateParallaxEffect (int scrollPosition) {
        float damping = 0.5f;
        int dampedScroll = (int) (scrollPosition * damping);
        int offset = mLastDampedScroll - dampedScroll;
        mHeader.offsetTopAndBottom(offset);

        mLastDampedScroll = dampedScroll;
    }

    private int interpolate (int from, int to, float param) {
        return (int) (from * param + to * (1 - param));
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
