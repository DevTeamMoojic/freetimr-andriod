package com.freetimr.model;

import com.google.gson.annotations.SerializedName;

public class NoticePeriod {
    @SerializedName("NoticePeriodId")
    public int id;
    @SerializedName("NoticePeriodName")
    public String name;
}
