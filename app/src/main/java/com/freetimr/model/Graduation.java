package com.freetimr.model;

import com.google.gson.annotations.SerializedName;

public class Graduation {
    @SerializedName("GraduationId")
    public int id;
    @SerializedName("GraduationName")
    public String name;
}
