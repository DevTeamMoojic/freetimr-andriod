package com.freetimr.activities;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.freetimr.R;
import com.freetimr.adapter.JobApplicationsAdapter;
import com.freetimr.adapter.JobListAdapter;
import com.freetimr.fragments.JobListFragment;
import com.freetimr.model.CandidateModel;
import com.freetimr.model.JobsRequestModel;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.N)
public class JobApplicationsActivity extends AppCompatActivity {

    RecyclerView mJobListRecyclerView;
    JobApplicationsAdapter jobApplicationsAdapter;
    Toolbar mToolbar;
    CandidateModel candidateModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_applications);
        mToolbar = (Toolbar) findViewById(R.id.job_applications_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        candidateModel = FreeTimrUtils.getCandidateInfo(this);

        mJobListRecyclerView = (RecyclerView) findViewById(R.id.job_applications_rv);

        getAppliedJobs();
    }

    public void initJobListAdapter(List<JobsRequestModel> list) {
        jobApplicationsAdapter = new JobApplicationsAdapter(JobApplicationsActivity.this, list);
        mJobListRecyclerView.setAdapter(jobApplicationsAdapter);
        mJobListRecyclerView.setLayoutManager(new LinearLayoutManager(JobApplicationsActivity.this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void getAppliedJobs() {
        Call<JobsRequestModel[]> call = FreeTimrUtils.getRetrofit().getAppliedJobs(FreeTimrURLs.HOST + FreeTimrURLs.GET_APPLIED_JOB + candidateModel.Id);
        call.enqueue(new Callback<JobsRequestModel[]>() {
            @Override
            public void onResponse(Call<JobsRequestModel[]> call, Response<JobsRequestModel[]> response) {
                FreeTimrUtils.log("" + response.code());
                initJobListAdapter(Arrays.asList(response.body()));
            }

            @Override
            public void onFailure(Call<JobsRequestModel[]> call, Throwable t) {
                FreeTimrUtils.log("" + t);
            }
        });
    }
}
