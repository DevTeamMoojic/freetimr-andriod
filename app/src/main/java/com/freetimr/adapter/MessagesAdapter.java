package com.freetimr.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.activities.ChatActivity;
import com.freetimr.views.CustomTextView;

import java.util.ArrayList;

/**
 * Created by Admin on 26-Jan-17.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessageViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<String> list;

    public MessagesAdapter(Context context, ArrayList<String> list) {
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.message_list_item, parent, false);
        return new MessagesAdapter.MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        holder.name.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView name;

        public MessageViewHolder(View itemView) {
            super(itemView);
            name = (CustomTextView) itemView.findViewById(R.id.messages_company_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent intent = new Intent(context, ChatActivity.class);
//                    context.startActivity(intent);
                }
            });
        }
    }

}
