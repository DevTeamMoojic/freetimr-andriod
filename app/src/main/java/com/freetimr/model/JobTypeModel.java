package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 23-Mar-17.
 */

public class JobTypeModel implements Parcelable {

    public int JobTypeId;
    public String JobTypeName;

    protected JobTypeModel(Parcel in) {
        JobTypeId = in.readInt();
        JobTypeName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(JobTypeId);
        dest.writeString(JobTypeName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobTypeModel> CREATOR = new Creator<JobTypeModel>() {
        @Override
        public JobTypeModel createFromParcel(Parcel in) {
            return new JobTypeModel(in);
        }

        @Override
        public JobTypeModel[] newArray(int size) {
            return new JobTypeModel[size];
        }
    };
}
