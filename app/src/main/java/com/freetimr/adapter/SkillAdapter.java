package com.freetimr.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.activities.AddWorkExperienceActivity;
import com.freetimr.activities.SkillsActivity;
import com.freetimr.model.CandidateExperiences;
import com.freetimr.model.CandidateSkills;
import com.freetimr.model.ProfileModel;
import com.freetimr.model.Skill;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;
import com.freetimr.views.CustomTextView;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.EOFException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by varunbarve on 05/09/17.
 */

public class SkillAdapter extends RecyclerView.Adapter<SkillAdapter.SkillViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<CandidateSkills> list;
    boolean isEdit;

    public SkillAdapter(Context context, ArrayList<CandidateSkills> list, boolean isEdit) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.isEdit = isEdit;
    }

    @Override
    public SkillViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_skill, parent, false);
        return new SkillAdapter.SkillViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(SkillViewHolder holder, int position) {

        final CandidateSkills model = list.get(position);

        holder.skillName.setText(FreeTimrUtils.getSkillName(context, model.Skill.SkillId));
        holder.experienceTime.setText(model.SkillExperienceInYears + "." + model.SkillExperienceInMonths + " yrs");
        holder.rating.setText("" + model.SkillRating);

        if (isEdit) {
            holder.delete.setVisibility(View.VISIBLE);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addSkill(model);
                }
            });

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteCandidateSkills(model.CandidateSkillId);
                }
            });
        } else {
            holder.delete.setVisibility(View.GONE);
        }
    }

    public class SkillViewHolder extends RecyclerView.ViewHolder {

        CustomTextView skillName, experienceTime, rating;
        ImageView delete;

        public SkillViewHolder(View itemView) {
            super(itemView);
            skillName = (CustomTextView) itemView.findViewById(R.id.skill_name);
            experienceTime = (CustomTextView) itemView.findViewById(R.id.skill_exp);
            rating = (CustomTextView) itemView.findViewById(R.id.skill_rating);
            delete = (ImageView) itemView.findViewById(R.id.delete_skill);
        }
    }

    String skillId, skillYear, skillMonth, skillRating;

    public void addSkill(final CandidateSkills model) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(context);
        // Include dialog.xml file
        dialog.setContentView(R.layout.add_skill_dialog);

        AutoCompleteTextView skillNameAutoComplete = (AutoCompleteTextView) dialog.findViewById(R.id.skill_name_autocomplete);
        final EditText year = (EditText) dialog.findViewById(R.id.skill_year);
        final EditText month = (EditText) dialog.findViewById(R.id.skill_month);
        final DiscreteSeekBar ratings = (DiscreteSeekBar) dialog.findViewById(R.id.skills_percent);
        TextView cancel = (TextView) dialog.findViewById(R.id.skill_cancel);
        TextView add = (TextView) dialog.findViewById(R.id.skill_add);
        add.setText("Update");

        ArrayList<Skill> skills = FreeTimrUtils.getCandidateMasters(context).skills;
        final SkillAutoCompleteAdapter skillAutoCompleteAdapter = new SkillAutoCompleteAdapter(context, skills) ;
        skillNameAutoComplete.setAdapter(skillAutoCompleteAdapter);

        skillNameAutoComplete.setText(FreeTimrUtils.getSkillName(context, model.SkillId));
        skillNameAutoComplete.dismissDropDown();
        year.setText("" + model.SkillExperienceInYears);
        month.setText("" + model.SkillExperienceInMonths);
        ratings.setProgress(model.SkillRating);

        skillId = "" + model.SkillId;
        skillYear = "" + model.SkillExperienceInYears;
        skillMonth = "" + model.SkillExperienceInMonths;
        skillRating = "" + model.SkillRating;

        skillNameAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                skillId = "" + skillAutoCompleteAdapter.getItem(position).id;
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skillYear = year.getText().toString();
                skillMonth = month.getText().toString();
                skillRating = "" + ratings.getProgress();

                CandidateSkills candidateSkills = new CandidateSkills();
                candidateSkills.CandidateSkillId = model.CandidateSkillId;
                candidateSkills.SkillExperienceInMonths = Integer.parseInt(skillMonth);
                candidateSkills.SkillExperienceInYears = Integer.parseInt(skillYear);
                candidateSkills.SkillId = Integer.parseInt(skillId);
                candidateSkills.SkillRating = Integer.parseInt(skillRating);

                FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(candidateSkills));

                updateCandidateSkills(candidateSkills);

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void updateCandidateSkills(CandidateSkills model) {
        FreeTimrUtils.showProgressDialog((Activity) context);

        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(model));

        Call<CandidateSkills> call = FreeTimrUtils.getRetrofit().updateCandidateSkill(model);
        call.enqueue(new Callback<CandidateSkills>() {
            @Override
            public void onResponse(Call<CandidateSkills> call, Response<CandidateSkills> response) {
                if (response.code() == 200) {
                    ArrayList<ProfileModel> profileModels = FreeTimrUtils.getCandidateProfile(context);
                    ArrayList<CandidateSkills> candidateSkillses = profileModels.get(0).CandidateSkill;

                    for (Iterator<CandidateSkills> iterator = candidateSkillses.iterator(); iterator.hasNext(); ) {
                        CandidateSkills model = iterator.next();
                        if (model.CandidateSkillId == response.body().CandidateSkillId) {
                            iterator.remove();
                        }
                    }

                    candidateSkillses.add(response.body());

                    profileModels.get(0).CandidateSkill = candidateSkillses;

                    FreeTimrUtils.saveCandidateProfile(context, FreeTimrUtils.newGson().toJson(profileModels));

                    ((SkillsActivity) context).initSkillsAdapter();
                } else {
                    FreeTimrUtils.showMessage(context, "Something went wrong. Please try again.");
                }
                FreeTimrUtils.cancelCurrentDialog((Activity) context);
            }

            @Override
            public void onFailure(Call<CandidateSkills> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
                FreeTimrUtils.cancelCurrentDialog((Activity) context);
            }
        });

    }


    private void deleteCandidateSkills(final int id) {
        FreeTimrUtils.showProgressDialog((Activity) context);

        Call<Integer> call = FreeTimrUtils.getRetrofit().deleteCandidateSkill(FreeTimrURLs.HOST + FreeTimrURLs.DELETE_SKILLS + id);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    ArrayList<ProfileModel> profileModels = FreeTimrUtils.getCandidateProfile(context);
                    ArrayList<CandidateSkills> candidateSkillses = profileModels.get(0).CandidateSkill;

                    for (Iterator<CandidateSkills> iterator = candidateSkillses.iterator(); iterator.hasNext(); ) {
                        CandidateSkills model = iterator.next();
                        if (model.CandidateSkillId == id) {
                            iterator.remove();
                        }
                    }

                    FreeTimrUtils.saveCandidateProfile(context, FreeTimrUtils.newGson().toJson(profileModels));

                    ((SkillsActivity) context).initSkillsAdapter();
                } else {
                    FreeTimrUtils.showMessage(context, "Something went wrong. Please try again.");
                }
                FreeTimrUtils.cancelCurrentDialog((Activity) context);
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                if (t instanceof EOFException) {
                    ArrayList<ProfileModel> profileModels = FreeTimrUtils.getCandidateProfile(context);
                    ArrayList<CandidateSkills> candidateSkillses = profileModels.get(0).CandidateSkill;

                    for (Iterator<CandidateSkills> iterator = candidateSkillses.iterator(); iterator.hasNext(); ) {
                        CandidateSkills model = iterator.next();
                        if (model.CandidateSkillId == id) {
                            iterator.remove();
                        }
                    }

                    FreeTimrUtils.saveCandidateProfile(context, FreeTimrUtils.newGson().toJson(profileModels));

                    ((SkillsActivity) context).initSkillsAdapter();
                }
                FreeTimrUtils.cancelCurrentDialog((Activity) context);
                FreeTimrUtils.log(t.getLocalizedMessage());
            }
        });
    }
}
