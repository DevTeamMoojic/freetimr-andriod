package com.freetimr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.utils.Constants;
import com.sendbird.android.UserMessage;

import java.util.List;

/**
 * Created by Admin on 03-Apr-17.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int CANDIDATE_MESSAGE = 1;
    private static final int COMPANY_MESSAGE = 0;

    Context context;
    LayoutInflater inflater;
    List<UserMessage> list;

    public ChatAdapter(Context context, List<UserMessage> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COMPANY_MESSAGE) {
            View view = inflater.inflate(R.layout.company_message_chat_item, parent, false);
            return new CompanyMessageChatViewHolder(view);
        } else if (viewType == CANDIDATE_MESSAGE) {
            View view = inflater.inflate(R.layout.candidate_message_chat_item, parent, false);
            return new CandidateMessageChatViewHolder(view);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UserMessage model = list.get(position);
        if (getItemViewType(position) == COMPANY_MESSAGE) {
            CandidateMessageChatViewHolder candidateHolder = (CandidateMessageChatViewHolder) holder;

            candidateHolder.message.setText(model.getMessage());
            candidateHolder.time.setText("" + model.getCreatedAt());

        } else if (getItemViewType(position) == COMPANY_MESSAGE) {
            CompanyMessageChatViewHolder companyHolder = (CompanyMessageChatViewHolder) holder;

            companyHolder.message.setText(model.getMessage());
            companyHolder.time.setText("" + model.getCreatedAt());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position).getCustomType().equals(Constants.USERTYPE_CANDIDATE)) {
            return CANDIDATE_MESSAGE;
        } else if (list.get(position).getCustomType().equals(Constants.USERTYPE_COMPANY)) {
            return COMPANY_MESSAGE;
        } else {
            return -1;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CompanyMessageChatViewHolder extends RecyclerView.ViewHolder {
        ImageView logo;
        TextView message, time;

        public CompanyMessageChatViewHolder(View itemView) {
            super(itemView);
            logo = (ImageView) itemView.findViewById(R.id.company_profile_image);
            message = (TextView) itemView.findViewById(R.id.company_message);
            time = (TextView) itemView.findViewById(R.id.company_message_time);
        }
    }

    class CandidateMessageChatViewHolder extends RecyclerView.ViewHolder {
        ImageView logo;
        TextView message, time;

        public CandidateMessageChatViewHolder(View itemView) {
            super(itemView);
            logo = (ImageView) itemView.findViewById(R.id.candidate_profile_image);
            message = (TextView) itemView.findViewById(R.id.candidate_message);
            time = (TextView) itemView.findViewById(R.id.candidate_message_time);
        }
    }

}
