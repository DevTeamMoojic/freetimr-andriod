package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 23-Mar-17.
 */

public class JobDisabilityModel implements Parcelable {

    public int JobDisabilityId;
    public String JobId;
    public int DisabilityId;

    protected JobDisabilityModel(Parcel in) {
        JobDisabilityId = in.readInt();
        JobId = in.readString();
        DisabilityId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(JobDisabilityId);
        dest.writeString(JobId);
        dest.writeInt(DisabilityId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobDisabilityModel> CREATOR = new Creator<JobDisabilityModel>() {
        @Override
        public JobDisabilityModel createFromParcel(Parcel in) {
            return new JobDisabilityModel(in);
        }

        @Override
        public JobDisabilityModel[] newArray(int size) {
            return new JobDisabilityModel[size];
        }
    };
}
