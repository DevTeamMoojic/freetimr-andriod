package com.freetimr.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.JobCategoryModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by varunbarve on 29/04/17.
 */

public class JobCategoryAdapter extends RecyclerView.Adapter<JobCategoryAdapter.JobCategoryViewHolder>{

    Context context;
    ArrayList<JobCategoryModel> list;
    LayoutInflater inflater;
    int pos = -1;
    int categoryId = -1;

    public JobCategoryAdapter(Context context, ArrayList<JobCategoryModel> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public JobCategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.job_category_list_item,parent,false);
        return new JobCategoryViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final JobCategoryViewHolder holder, final int position) {
        JobCategoryModel model = list.get(position);
        holder.name.setText(model.JobCategoryName);

        if (this.pos==position){
            holder.selected.setVisibility(View.VISIBLE);
        }else{
            holder.selected.setVisibility(View.GONE);
        }

        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = (int)holder.itemView.getTag();
                categoryId = list.get(pos).JobCategoryId;
                onBindViewHolder(holder,position);
                notifyDataSetChanged();
            }
        });

        String imageUrl = "http://137.59.54.53/FreeTimr/Images/JobCategory/"+model.JobCategoryImage;
        Picasso.with(context).load(imageUrl).placeholder(R.drawable.browse_category_retail).into(holder.image);
    }

    public int getCategoryId(){
        return categoryId;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class JobCategoryViewHolder extends RecyclerView.ViewHolder{

        TextView name;
        ImageView image;
        View selected;

        public JobCategoryViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.job_category_name);
            image= (ImageView) itemView.findViewById(R.id.job_category_image);
            selected =itemView.findViewById(R.id.selected);
        }
    }
}
