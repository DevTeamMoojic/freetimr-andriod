package com.freetimr.model;

import com.google.gson.annotations.SerializedName;

public class WorkLocation {
    @SerializedName("WorkLocationId")
    public int id;
    @SerializedName("WorkLocationName")
    public String name;

    public boolean isSelected;
}
