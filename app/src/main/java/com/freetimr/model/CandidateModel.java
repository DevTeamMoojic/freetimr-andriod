package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Admin on 09-Feb-17.
 */

public class CandidateModel implements Parcelable{

    @SerializedName("CandidateId")
    public String Id;

    @SerializedName("CandidateName")
    public String Name;

    @SerializedName("Gender")
    public String Gender;

    @SerializedName("EmailId")
    public String EmailId;

    @SerializedName("Passcode")
    public String Password;

    @SerializedName("MobileNo")
    public String MobileNumber;

    @SerializedName("IsVerified")
    public boolean IsVerified;

    @SerializedName("CandidateImage")
    public String Image;

    @SerializedName("CandidateImageUrl")
    public String ImageUrl;

    @SerializedName("LastLoginDate")
    public String LastLoginDate;

    @SerializedName("IsActive")
    public boolean IsActive;

    @SerializedName("DeactivatedDate")
    public String DeactivatedDate;

    @SerializedName("CreatedDate")
    public String CreatedDate;

    @SerializedName("UpdatedDate")
    public String UpdatedDate;

    @SerializedName("UserGuid")
    public String UserGuid;

    @SerializedName("DateOfBirth")
    public Date DateOfBirth;

//    public ProfileModel[] profileModels;
//
//    public ProfileModel[] getProfileModels() {
//        return profileModels;
//    }
//
//    public void setProfileModels(ProfileModel[] profileModels) {
//        this.profileModels = profileModels;
//    }

    public ArrayList<ProfileModel> profileModels;

    public CandidateModel () {
    }

    protected CandidateModel (Parcel in) {
        Id = in.readString();
        Name = in.readString();
        Gender = in.readString();
        EmailId = in.readString();
        Password = in.readString();
        MobileNumber = in.readString();
        IsVerified = in.readByte() != 0;
        Image = in.readString();
        ImageUrl = in.readString();
        LastLoginDate = in.readString();
        IsActive = in.readByte() != 0;
        DeactivatedDate = in.readString();
        CreatedDate = in.readString();
        UpdatedDate = in.readString();
        UserGuid = in.readString();
        profileModels = in.createTypedArrayList(ProfileModel.CREATOR);
    }

    public static final Creator<CandidateModel> CREATOR = new Creator<CandidateModel>() {
        @Override
        public CandidateModel createFromParcel (Parcel in) {
            return new CandidateModel(in);
        }

        @Override
        public CandidateModel[] newArray (int size) {
            return new CandidateModel[size];
        }
    };

    public ArrayList<ProfileModel> getProfileModels () {
        return profileModels;
    }

    public void setProfileModels (ArrayList<ProfileModel> profileModels) {
        this.profileModels = profileModels;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public boolean isVerified() {
        return IsVerified;
    }

    public void setVerified(boolean verified) {
        IsVerified = verified;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public String getDeactivatedDate() {
        return DeactivatedDate;
    }

    public void setDeactivatedDate(String deactivatedDate) {
        DeactivatedDate = deactivatedDate;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        UpdatedDate = updatedDate;
    }

    public String getUserGuid() {
        return UserGuid;
    }

    public void setUserGuid(String userGuid) {
        UserGuid = userGuid;
    }

    public String getLastLoginDate() {
        return LastLoginDate;
    }

    public void setLastLoginDate(String lastLoginDate) {
        LastLoginDate = lastLoginDate;
    }

    @Override
    public int describeContents () {
        return 0;
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeString(Id);
        dest.writeString(Name);
        dest.writeString(Gender);
        dest.writeString(EmailId);
        dest.writeString(Password);
        dest.writeString(MobileNumber);
        dest.writeByte((byte) (IsVerified ? 1 : 0));
        dest.writeString(Image);
        dest.writeString(ImageUrl);
        dest.writeString(LastLoginDate);
        dest.writeByte((byte) (IsActive ? 1 : 0));
        dest.writeString(DeactivatedDate);
        dest.writeString(CreatedDate);
        dest.writeString(UpdatedDate);
        dest.writeString(UserGuid);
        dest.writeTypedList(profileModels);
    }
}
