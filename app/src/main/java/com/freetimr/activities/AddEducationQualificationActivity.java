package com.freetimr.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.fragments.ProfileDetailsFragment;
import com.freetimr.model.CandidateEducations;
import com.freetimr.model.CandidateExperiences;
import com.freetimr.model.CandidateMastersModel;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.BottomSheetListOnItemClickListener;
import com.freetimr.utils.Constants;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;

import java.util.ArrayList;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddEducationQualificationActivity extends AppCompatActivity {

    Toolbar mToolbar;
    RadioButton mModeFullTime, mModePartTime, mModeCorrespondence;
    EditText mUniversity, mGrade;
    TextView mYear, mGradingSystem, mSpecialization, mDegreeName;
    Button mProceedBtn;

    String educationModeStr, universityNameStr, gradeStr, graduationName, specializationName, gradingName;
    int graduationIdValue, specializationIdValue, graduatedYearValue, gradingSystemValue;
    CandidateEducations candidateEducations;

    boolean isSingleEntry = false;
    boolean isUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_education_qualification);

        mToolbar = (Toolbar) findViewById(R.id.add_education_qualification_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        isSingleEntry = getIntent().getBooleanExtra("IS_SINGLE_ENTRY", false);
        isUpdate = getIntent().getBooleanExtra("IS_UPDATE", false);

        if (isUpdate) {
            candidateEducations = getIntent().getParcelableExtra("CANDIDATE_EDUCATION");
        } else {
            candidateEducations = new CandidateEducations();
        }

        final CandidateMastersModel candidateMastersModel = FreeTimrUtils.getCandidateMasters(AddEducationQualificationActivity.this);

        mModeFullTime = (RadioButton) findViewById(R.id.education_mode_full_time);
        mModePartTime = (RadioButton) findViewById(R.id.education_mode_part_time);
        mModeCorrespondence = (RadioButton) findViewById(R.id.education_mode_correspondence);
        mSpecialization = (TextView) findViewById(R.id.education_specialization);
        mUniversity = (EditText) findViewById(R.id.education_university);
        mGrade = (EditText) findViewById(R.id.education_grade);
        mYear = (TextView) findViewById(R.id.education_year);
        mGradingSystem = (TextView) findViewById(R.id.education_grading_system);
        mDegreeName = (TextView) findViewById(R.id.degree_name);
        mProceedBtn = (Button) findViewById(R.id.education_proceed_btn);

        if (isUpdate) {
            setData(candidateEducations);
        }

        mYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] yearString = new String[]{"2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000"};

                FreeTimrUtils.showBottomSheetDialog(getSupportFragmentManager(), "Year of Graduation", yearString, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected(int position) {
                        graduatedYearValue = Integer.parseInt(yearString[position]);
                        mYear.setText(yearString[position]);
                    }
                });
            }
        });

        mGradingSystem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] strings = new String[candidateMastersModel.gradingSystems.size()];
                for (int i = 0; i < candidateMastersModel.gradingSystems.size(); i++) {
                    strings[i] = candidateMastersModel.gradingSystems.get(i).name;
                }

                FreeTimrUtils.showBottomSheetDialog(getSupportFragmentManager(), "Grading System", strings, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected(int position) {
                        gradingSystemValue = candidateMastersModel.gradingSystems.get(position).id;
                        gradingName = candidateMastersModel.gradingSystems.get(position).name;
                        mGradingSystem.setText(candidateMastersModel.gradingSystems.get(position).name);
                    }
                });
            }
        });

        mSpecialization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] strings = new String[candidateMastersModel.specializations.size()];
                for (int i = 0; i < candidateMastersModel.specializations.size(); i++) {
                    strings[i] = candidateMastersModel.specializations.get(i).name;
                }

                FreeTimrUtils.showBottomSheetDialog(getSupportFragmentManager(), "Specializations", strings, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected(int position) {
                        specializationIdValue = candidateMastersModel.specializations.get(position).id;
                        specializationName = candidateMastersModel.specializations.get(position).name;
                        mSpecialization.setText(candidateMastersModel.specializations.get(position).name);
                    }
                });
            }
        });

        mDegreeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] strings = new String[candidateMastersModel.graduations.size()];
                for (int i = 0; i < candidateMastersModel.graduations.size(); i++) {
                    strings[i] = candidateMastersModel.graduations.get(i).name;
                }

                FreeTimrUtils.showBottomSheetDialog(getSupportFragmentManager(), "Basic / Graduation", strings, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected(int position) {
                        graduationIdValue = candidateMastersModel.graduations.get(position).id;
                        graduationName = candidateMastersModel.graduations.get(position).name;
                        mDegreeName.setText(candidateMastersModel.graduations.get(position).name);
                    }
                });
            }
        });

        mModeFullTime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                educationModeStr = "F";
            }
        });

        mModePartTime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                educationModeStr = "P";
            }
        });

        mModeCorrespondence.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                educationModeStr = "C";
            }
        });

        mProceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                universityNameStr = mUniversity.getText().toString();
                gradeStr = mGrade.getText().toString();

                if (graduationIdValue == 0 || educationModeStr == null || universityNameStr.isEmpty() || gradeStr.isEmpty() || specializationIdValue == 0 || graduatedYearValue == 0 || gradingSystemValue == 0) {
                    FreeTimrUtils.showMessage(AddEducationQualificationActivity.this, "Please fill all the fields");
                } else {
                    candidateEducations.GraduationId = graduationIdValue;
                    candidateEducations.EducationMode = educationModeStr;
                    candidateEducations.GradingSystem = gradingSystemValue;
                    candidateEducations.GraduatedYear = graduatedYearValue;
                    candidateEducations.InstituteName = universityNameStr;
                    candidateEducations.Marks = gradeStr;
                    candidateEducations.SpecializationId = specializationIdValue;
                    candidateEducations.GraduationName = graduationName;
                    candidateEducations.SpecializationName = specializationName;
                    candidateEducations.GradingSystemName = gradingName;

                    if (isSingleEntry) {
                        if (isUpdate) {
                            updateEducationQualification(candidateEducations);
                        } else {
                            addEducationQualification(candidateEducations);
                        }
                    } else {
                        Intent educationIntent = new Intent();
                        educationIntent.putExtra("Education", candidateEducations);
                        setResult(RESULT_OK, educationIntent);
                        finish();
                    }
                }
            }
        });
    }

    private void setData(CandidateEducations candidateEducations) {
        if (candidateEducations.EducationMode.equalsIgnoreCase("F")) {
            mModeFullTime.setChecked(true);
        } else if (candidateEducations.EducationMode.equalsIgnoreCase("P")) {
            mModePartTime.setChecked(true);
        } else if (candidateEducations.EducationMode.equalsIgnoreCase("C")) {
            mModeCorrespondence.setChecked(true);
        } else {
            mModeFullTime.setChecked(false);
            mModePartTime.setChecked(false);
            mModeCorrespondence.setChecked(false);
        }
        educationModeStr = candidateEducations.EducationMode;

        mSpecialization.setText(FreeTimrUtils.getSpecializationById(this, candidateEducations.SpecializationId));
        specializationIdValue = candidateEducations.SpecializationId;

        mUniversity.setText(candidateEducations.InstituteName);
        universityNameStr = candidateEducations.InstituteName;

        mYear.setText("" + candidateEducations.GraduatedYear);
        graduatedYearValue = candidateEducations.GraduatedYear;

        mGrade.setText(candidateEducations.Marks);
        gradeStr = candidateEducations.Marks;

        mGradingSystem.setText(FreeTimrUtils.getGradingSystemById(this, candidateEducations.GradingSystem));
        gradingSystemValue = candidateEducations.GradingSystem;

        mDegreeName.setText(FreeTimrUtils.getGraduationNameById(this, candidateEducations.GraduationId));
        graduationIdValue = candidateEducations.GraduationId;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent educationIntent = new Intent();
        setResult(RESULT_CANCELED, educationIntent);
        finish();
    }


    public void addEducationQualification(CandidateEducations candidateEducations) {
        FreeTimrUtils.showProgressDialog(this);
        String candidateProfileId = FreeTimrUtils.getCandidateProfile(AddEducationQualificationActivity.this).get(0).CandidateProfileId;

        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(candidateEducations));

        Call<CandidateEducations> call = FreeTimrUtils.getRetrofit().addCandidateEducation(FreeTimrURLs.HOST + FreeTimrURLs.ADD_EDUCATION + candidateProfileId, candidateEducations);
        call.enqueue(new Callback<CandidateEducations>() {
            @Override
            public void onResponse(Call<CandidateEducations> call, Response<CandidateEducations> response) {
                if (response.code() == 200) {
                    ArrayList<ProfileModel> profileModel = FreeTimrUtils.getCandidateProfile(AddEducationQualificationActivity.this);
                    ArrayList<CandidateEducations> candidateExperiencesArrayList = profileModel.get(0).CandidateEducation;
                    candidateExperiencesArrayList.add(response.body());

                    profileModel.get(0).CandidateEducation = candidateExperiencesArrayList;

                    FreeTimrUtils.saveCandidateProfile(AddEducationQualificationActivity.this, FreeTimrUtils.newGson().toJson(profileModel));
                    Intent workExpIntent = new Intent();
                    workExpIntent.putExtra("Education", response.body());
                    setResult(RESULT_OK, workExpIntent);
                    finish();
                } else if (response.code() == 400) {
                    FreeTimrUtils.showMessage(AddEducationQualificationActivity.this, "Graduation type already exists.");
                } else {
                    FreeTimrUtils.showMessage(AddEducationQualificationActivity.this, "Something went wrong. Please try again later.");
                }
                FreeTimrUtils.cancelCurrentDialog(AddEducationQualificationActivity.this);
            }

            @Override
            public void onFailure(Call<CandidateEducations> call, Throwable t) {
                FreeTimrUtils.cancelCurrentDialog(AddEducationQualificationActivity.this);
                FreeTimrUtils.log(t.getLocalizedMessage());
            }
        });
    }

    public void updateEducationQualification(CandidateEducations candidateEducations) {
        FreeTimrUtils.showProgressDialog(this);
        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(candidateEducations));

        Call<CandidateEducations> call = FreeTimrUtils.getRetrofit().updateCandidateEducation(candidateEducations);
        call.enqueue(new Callback<CandidateEducations>() {
            @Override
            public void onResponse(Call<CandidateEducations> call, Response<CandidateEducations> response) {
                if (response.code() == 200) {
                    ArrayList<CandidateEducations> candidateEducationsArrayList = FreeTimrUtils.getCandidateProfile(AddEducationQualificationActivity.this).get(0).CandidateEducation;
                    for (Iterator<CandidateEducations> iterator = candidateEducationsArrayList.iterator(); iterator.hasNext(); ) {
                        CandidateEducations model = iterator.next();
                        if (model.CandidateEducationId == response.body().CandidateEducationId) {
                            iterator.remove();
                        }
                    }

                    candidateEducationsArrayList.add(response.body());

                    ArrayList<ProfileModel> profileModels = FreeTimrUtils.getCandidateProfile(AddEducationQualificationActivity.this);
                    profileModels.get(0).CandidateEducation = candidateEducationsArrayList;

                    FreeTimrUtils.saveCandidateProfile(AddEducationQualificationActivity.this, FreeTimrUtils.newGson().toJson(profileModels));

                    setData(response.body());
                } else {
                    FreeTimrUtils.showMessage(AddEducationQualificationActivity.this, "Something went wrong. Please try again.");
                }
                FreeTimrUtils.cancelCurrentDialog(AddEducationQualificationActivity.this);
            }

            @Override
            public void onFailure(Call<CandidateEducations> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
                FreeTimrUtils.cancelCurrentDialog(AddEducationQualificationActivity.this);
            }
        });
    }
}
