package com.freetimr.model;

import com.google.gson.annotations.SerializedName;

public class Occupation {
    @SerializedName("OccupationId")
    public int id;
    @SerializedName("OccupationName")
    public String name;
}
