package com.freetimr.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.freetimr.R;
import com.freetimr.model.CandidateMastersModel;
import com.freetimr.model.CandidateModel;
import com.freetimr.model.JobMastersModel;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.Constants;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;
import com.freetimr.utils.SharedPreferenceStore;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getCandidateMasters();
            }
        }, 1000);
    }

    private void getCandidateMasters() {
        Call<CandidateMastersModel> candidateMastersModelCall = FreeTimrUtils.getRetrofit().getCandidateMasters(FreeTimrURLs.GET_CANDIDATE_MASTERS);
        candidateMastersModelCall.enqueue(new Callback<CandidateMastersModel>() {
            @Override
            public void onResponse(Call<CandidateMastersModel> call, Response<CandidateMastersModel> response) {
                if (response.code() == Constants.RESPONSE_SUCCESS)
                    FreeTimrUtils.saveCandidateMasters(SplashActivity.this, FreeTimrUtils.newGson().toJson(response.body()));

                getJobMaster();
            }

            @Override
            public void onFailure(Call<CandidateMastersModel> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
                switchToNextPage();
            }
        });
    }

    public void getJobMaster() {
        Call<JobMastersModel> call = FreeTimrUtils.getRetrofit().getJobMaster(FreeTimrURLs.HOST + FreeTimrURLs.GET_JOB_MASTER);
        call.enqueue(new Callback<JobMastersModel>() {
            @Override
            public void onResponse(Call<JobMastersModel> call, Response<JobMastersModel> response) {
                if (response.isSuccessful())
                    FreeTimrUtils.saveJobMasters(SplashActivity.this, FreeTimrUtils.newGson().toJson(response.body()));

//                if (FreeTimrUtils.getCandidateProfile(SplashActivity.this).size() == 0) {
                getUserProfile();
//                }else {
//                    switchToNextPage();
//                }
            }

            @Override
            public void onFailure(Call<JobMastersModel> call, Throwable t) {
                FreeTimrUtils.log("" + t);
                switchToNextPage();
            }
        });
    }

    public void getUserProfile() {
        try {
            String candidateId = FreeTimrUtils.getCandidateInfo(this).Id;
            Call<ProfileModel[]> profileCall = FreeTimrUtils.getRetrofit().getCandidateProfile(FreeTimrURLs.HOST + FreeTimrURLs.GET_PROFILE + "/" + candidateId);
            profileCall.enqueue(new Callback<ProfileModel[]>() {
                @Override
                public void onResponse(Call<ProfileModel[]> call, Response<ProfileModel[]> response) {
//                    FreeTimrUtils.log("" + FreeTimrUtils.newGson().toJson(response.body()));
                    if (response.body().length != 0) {
                        FreeTimrUtils.saveCandidateProfile(SplashActivity.this, FreeTimrUtils.newGson().toJson(response.body()));
                        SharedPreferenceStore.storeValue(SplashActivity.this, Constants.IS_RPOFILE_COMPLETE, true);
                        switchToNextPage();
                    } else {
                        Intent intent = new Intent(SplashActivity.this, RegistrationActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<ProfileModel[]> call, Throwable t) {
                    FreeTimrUtils.log("" + t.getLocalizedMessage());
                    switchToNextPage();
                }
            });
        } catch (Exception e) {
            switchToNextPage();
        }
    }

    public void switchToNextPage() {
        CandidateModel candidateModel = FreeTimrUtils.getCandidateInfo(SplashActivity.this);

        if (candidateModel == null || !candidateModel.isVerified()) {
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);//RegisterActivity
            startActivity(intent);
            finish();
        } else {
            boolean isProfileComplete = SharedPreferenceStore.getValue(SplashActivity.this, Constants.IS_RPOFILE_COMPLETE, false);
            if (isProfileComplete) {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(SplashActivity.this, RegistrationActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }
}
