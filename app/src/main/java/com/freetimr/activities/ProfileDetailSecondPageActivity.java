package com.freetimr.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.freetimr.R;

public class ProfileDetailSecondPageActivity extends AppCompatActivity {

    Toolbar mToolbar;

    Button mProceedBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_detail_second_page);

        mToolbar = (Toolbar) findViewById(R.id.profile_details_second_page_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mProceedBtn = (Button) findViewById(R.id.proceed_btn);
        mProceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileDetailSecondPageActivity.this, ProfileQualificationAndSkillsActivity.class);
                startActivity(intent);
            }
        });
    }
}
