package com.freetimr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.CandidateProfileSkillRoleModel;
import com.freetimr.model.CandidateSkillRoleModel;

import java.util.ArrayList;

/**
 * Created by Admin on 23-Dec-16.
 */

public class RoleAdapter extends RecyclerView.Adapter<RoleAdapter.RoleViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<CandidateProfileSkillRoleModel> list;

    public RoleAdapter(Context context, ArrayList<CandidateProfileSkillRoleModel> list) {
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RoleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.role_list_item, parent, false);
        return new RoleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RoleViewHolder holder, int position) {
        CandidateSkillRoleModel model = list.get(position).SkillRole;
        holder.name.setText(model.SkillRoleName);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RoleViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        public RoleViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.role_name);
        }
    }

}
