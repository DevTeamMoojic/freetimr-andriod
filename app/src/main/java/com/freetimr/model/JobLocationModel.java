package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 23-Mar-17.
 */

public class JobLocationModel implements Parcelable {

    public int JobLocationId;
    public String JobId;
    public int CityId;
    public String LocationName;

    protected JobLocationModel(Parcel in) {
        JobLocationId = in.readInt();
        JobId = in.readString();
        CityId = in.readInt();
        LocationName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(JobLocationId);
        dest.writeString(JobId);
        dest.writeInt(CityId);
        dest.writeString(LocationName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobLocationModel> CREATOR = new Creator<JobLocationModel>() {
        @Override
        public JobLocationModel createFromParcel(Parcel in) {
            return new JobLocationModel(in);
        }

        @Override
        public JobLocationModel[] newArray(int size) {
            return new JobLocationModel[size];
        }
    };
}
