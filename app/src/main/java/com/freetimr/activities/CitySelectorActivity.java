package com.freetimr.activities;

import android.graphics.Color;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;

import com.freetimr.R;
import com.freetimr.adapter.CityAdapter;
import com.freetimr.model.CandidatePreferredCityModel;
import com.freetimr.model.CityModel;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import fr.castorflex.android.smoothprogressbar.SmoothProgressDrawable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;

public class CitySelectorActivity extends AppCompatActivity {

    CityAdapter cityAdapter;
    RecyclerView cityRecyclerView;
    CityModel[] cityModels;
    ImageView cancel, update;
    SmoothProgressBar smoothProgressBar;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_selector);

        cityRecyclerView = (RecyclerView) findViewById(R.id.city_recycler_view);
        cancel = (ImageView) findViewById(R.id.update_candidate_city_cancel);
        update = (ImageView) findViewById(R.id.update_candidate_city_btn);
        smoothProgressBar = (SmoothProgressBar) findViewById(R.id.smooth_progress_bar);
        smoothProgressBar.setVisibility(View.GONE);

        int[] colors = new int[]{Color.parseColor("#F8B810"), Color.parseColor("#e74c3c"), Color.parseColor("#009688"), Color.parseColor("#9C27B0")};

        smoothProgressBar.setIndeterminateDrawable(new SmoothProgressDrawable.Builder(this).colors(colors).interpolator(new FastOutSlowInInterpolator()).sectionsCount(4).separatorLength(8).strokeWidth(8f).speed(1.5f).progressiveStartSpeed(2).progressiveStopSpeed(3.4f).reversed(false).mirrorMode(false).progressiveStart(true).build());

        setAdapter();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                onBackPressed();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                List<CityModel> selectedList = cityAdapter.getSelectedList();
                FreeTimrUtils.log("" + selectedList);
            }
        });
    }

    public void setAdapter(){
        cityModels = FreeTimrUtils.getJobMasters(CitySelectorActivity.this).City;

        List<CityModel> list = Arrays.asList(cityModels);
        ArrayList<CandidatePreferredCityModel> candidatePreferredCityList = new ArrayList<>(FreeTimrUtils.getCandidateProfile(this).get(0).CandidatePreferredCity);

        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < candidatePreferredCityList.size(); j++) {
                if (list.get(i).CityId == candidatePreferredCityList.get(j).City.CityId) {
                    list.get(i).isSelected = true;
                    list.get(i).CandidatePreferredCityId = candidatePreferredCityList.get(j).CandidatePreferredCityId;
                }
            }
        }

        cityAdapter = new CityAdapter(CitySelectorActivity.this, list, smoothProgressBar, this);
        cityRecyclerView.setAdapter(cityAdapter);
        cityRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(4, VERTICAL));
    }
}
