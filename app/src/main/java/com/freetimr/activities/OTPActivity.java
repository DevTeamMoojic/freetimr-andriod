package com.freetimr.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.CandidateModel;
import com.freetimr.utils.FreeTimrUtils;

public class OTPActivity extends AppCompatActivity {

    Toolbar mToolbar;
    Button proceedBtn;
    TextView mOTPCandidateName;
    CandidateModel candidateModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        /* INIT TOOLBAR */
        mToolbar = (Toolbar) findViewById(R.id.otp_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        candidateModel = FreeTimrUtils.getCandidateInfo(OTPActivity.this);

        proceedBtn = (Button) findViewById(R.id.proceed_btn);
        mOTPCandidateName = (TextView) findViewById(R.id.otp_candidate_name);

        mOTPCandidateName.setText(candidateModel.getName() + " !");

        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OTPActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });
    }


//    private void sendNumberForOTP() {
//        Call<String> call = FreeTimrUtils.getZivaRetrofit().sendOtp(PostParams.init().add("Mobile", candidateModel.MobileNumber).addPlatform());
//        call.enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//
//            }
//        });
//    }
//
//    private void verifyOTP() {
//        Call<String> call = FreeTimrUtils.getZivaRetrofit().verifyOtp(PostParams.init().add("RegistrationId", candidateModel.Id + "").add("OTP", 123).addPlatform());
//        call.enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//
//            }
//        });
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
