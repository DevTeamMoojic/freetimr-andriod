package com.freetimr.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.freetimr.model.AvailabilityDay;
import com.freetimr.model.AvailabilityTime;
import com.freetimr.model.AvailabilityTimeModel;
import com.freetimr.model.CandidateMastersModel;
import com.freetimr.model.CandidateModel;
import com.freetimr.model.CandidateProfileSkillRoleModel;
import com.freetimr.model.CandidateSkillRoleModel;
import com.freetimr.model.CityModel;
import com.freetimr.model.GradingSystem;
import com.freetimr.model.Graduation;
import com.freetimr.model.JobCategoryModel;
import com.freetimr.model.JobMastersModel;
import com.freetimr.model.LanguageModel;
import com.freetimr.model.NullCheckModel;
import com.freetimr.model.ProfileModel;
import com.freetimr.model.Skill;
import com.freetimr.model.Specialization;
import com.freetimr.model.WorkLocation;
import com.freetimr.model.WorkLocationModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Varun Barve on 09-Feb-17.
 * <p>
 * Utils Class for various Utility functions.
 */

public class FreeTimrUtils {


    static DialogDisplay showDialog = new FreeTimrUtils.DialogDisplay();
    static LoadingDialogBox pDialog;
    static Runnable cancelDialog = new Runnable() {
        @Override
        public void run() {
            try {
                if (pDialog != null)
                    pDialog.cancel();
                pDialog = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public static String getMonthCode(String month) {
        switch (month) {
            case "January":
                return "JAN";
            case "February":
                return "FEB";
            case "March":
                return "MAR";
            case "April":
                return "APR";
            case "May":
                return "MAY";
            case "June":
                return "JUN";
            case "July":
                return "JUL";
            case "August":
                return "AUG";
            case "September":
                return "SEP";
            case "October":
                return "OCT";
            case "November":
                return "NOV";
            case "December":
                return "DEC";
            default:
                return "";
        }
    }

    public static void showBottomSheetDialog(FragmentManager fragmentManager, String title, String[] strings, BottomSheetListOnItemClickListener bottomSheetListOnItemClickListener) {
        BottomSheetDialogFragment bottomSheetDialogFragment = MyBottomSheetDialogFragment.getInstance(title, strings, bottomSheetListOnItemClickListener);
        bottomSheetDialogFragment.show(fragmentManager, "Dialog");
    }

    public static CandidateModel getCandidateInfo(Context context) {
        String candidateInfo = SharedPreferenceStore.getValue(context, Constants.KEY_CANDIDATE_INFO, "");
        if (candidateInfo != null) {
            CandidateModel candidateModel = newGson().fromJson(candidateInfo, CandidateModel.class);
            return candidateModel;
        } else {
            return null;
        }
    }

    public static void saveCandidateInfo(Context context, String candidateInfoJSON) {
        if (candidateInfoJSON != null) {
            SharedPreferenceStore.storeValue(context, Constants.KEY_CANDIDATE_INFO, candidateInfoJSON);
        }
    }

//    public static ProfileModel[] getCandidateProfile (Context context) {
//        String profileStr = SharedPreferenceStore.getValue(context, Constants.KEY_CANDIDATE_PROFILE, "");
//        if (profileStr != null) {
//            List<ProfileModel> profileList = new ArrayList<>();
//            Type listType = new TypeToken<List<ProfileModel>>() {
//            }.getType();
//            profileList = newGson().fromJson(profileStr, listType);
//            return profileList.toArray(new ProfileModel[profileList.size()]);
//        } else {
//            return null;
//        }
//    }

    public static ArrayList<ProfileModel> getCandidateProfile(Context context) {
        String profileStr = SharedPreferenceStore.getValue(context, Constants.KEY_CANDIDATE_PROFILE, "");
        if (!profileStr.isEmpty()) {
            ArrayList<ProfileModel> profileList;
            Type listType = new TypeToken<List<ProfileModel>>() {
            }.getType();
            profileList = newGson().fromJson(profileStr, listType);
            return profileList;
        } else {
            return new ArrayList<>();
        }
    }

    public static void saveCandidateProfile(Context context, String candidateProfileJSON) {
        if (candidateProfileJSON != null) {
            SharedPreferenceStore.storeValue(context, Constants.KEY_CANDIDATE_PROFILE, candidateProfileJSON);
        }
    }

    public static void saveCandidateMasters(Context context, String candidateMasters) {
        SharedPreferenceStore.storeValue(context, Constants.KEY_CANDIDATE_MASTERS, candidateMasters);
    }

    public static CandidateMastersModel getCandidateMasters(Context context) {
        String candidateMastersStr = SharedPreferenceStore.getValue(context, Constants.KEY_CANDIDATE_MASTERS, "");
        return FreeTimrUtils.newGson().fromJson(candidateMastersStr, CandidateMastersModel.class);
    }

    public static void saveJobMasters(Context context, String jobMasters) {
        SharedPreferenceStore.storeValue(context, Constants.KEY_JOB_MASTER, jobMasters);
    }

    public static JobMastersModel getJobMasters(Context context) {
        String candidateMastersStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");
        return FreeTimrUtils.newGson().fromJson(candidateMastersStr, JobMastersModel.class);
    }

    /**
     * Check for Not Null
     *
     * @param data To check
     * @return
     */
    public static boolean notNull(String data) {
        if (data == null || data.equals("") || data.isEmpty())
            return false;
        return true;
    }

    /**
     * Check For Blank String
     *
     * @param text To check
     * @return
     */
    public static boolean notBlank(String text) {
        if (text == null || text.trim().length() == 0)
            return false;
        return true;
    }

    /**
     * Log String data in Error
     *
     * @param logData
     */
    public static void log(String logData) {
        Log.e(Constants.LOG_TAG, logData + "");
    }

    /**
     * Get NetworkConnector object with RetroFit.
     *
     * @return
     */
    public static NetworkConnector getRetrofit() {
        if (FreeTimrURLs.retrofitNetworkHandler == null) {

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                    .readTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS);

            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder().header("Content-Type", "application/json").header("APIAccessKey", "FreeTimrDev");

                    Request request = requestBuilder.build();

                    return chain.proceed(request);
                }
            });

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Date.class, new GsonDateDeSerializer());
            Retrofit retrofit = new Retrofit.Builder()
                    .client(httpClient.build())
                    .baseUrl(FreeTimrURLs.HOST)
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .build();
            FreeTimrURLs.retrofitNetworkHandler = retrofit.create(NetworkConnector.class);
        }

        return FreeTimrURLs.retrofitNetworkHandler;
    }

    public static NetworkConnector getZivaRetrofit() {
        if (FreeTimrURLs.retrofitNetworkHandler == null) {

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                    .readTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS);

            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder().header("Content-Type", "application/json").header("APIAccessKey", "FreeTimrDev");
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Date.class, new GsonDateDeSerializer());
            Retrofit retrofit = new Retrofit.Builder()
                    .client(httpClient.build())
                    .baseUrl("http://api.zivadiva.com/v2/")
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .build();
            FreeTimrURLs.retrofitNetworkHandler = retrofit.create(NetworkConnector.class);
        }


        return FreeTimrURLs.retrofitNetworkHandler;
    }

    /**
     * Check if the email address entered is Valid or Not.
     *
     * @param email
     * @return
     */
    public static boolean isValidEmailId(String email) {
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern p = Pattern.compile(emailPattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    /**
     * Show Progress Dialog
     *
     * @param activity
     */
    public static void showProgressDialog(final Activity activity) {
        Log.e(Constants.LOG_TAG, "Showing dialog... :)");
        activity.runOnUiThread(showDialog.updateActivity(activity, null));
    }

    /**
     * Show progress Dialog with message from Strings file
     *
     * @param activity
     * @param messageId Resource Id from Strings file
     */
    public static void showProgressDialog(final Activity activity, int messageId) {
        Log.e(Constants.LOG_TAG, "Showing dialog... :)");
        String message = activity.getString(messageId);
        activity.runOnUiThread(showDialog.updateActivity(activity, message));
    }

    /**
     * Show progress Dialog with message
     *
     * @param activity
     * @param message  String message
     */
    public static void showProgressDialog(final Activity activity, String message) {
        Log.e(Constants.LOG_TAG, "Showing dialog... :)");
        activity.runOnUiThread(showDialog.updateActivity(activity, message));
    }

    /**
     * Cancel current visible dialog
     *
     * @param activity
     */
    public static void cancelCurrentDialog(Activity activity) {
        Log.e(Constants.LOG_TAG, "Canceling dialog... :|");
        if (pDialog != null && activity != null) {
            new Handler().postDelayed(cancelDialog, 150);
        }
    }

    /**
     * Return new Gson object
     *
     * @return Gson Object
     */
    public static Gson newGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new GsonDateSerializer());
        return builder.create();
    }

    /**
     * Check if network is connected
     *
     * @param activity
     * @param checkInBackground
     * @return
     */
//    public static boolean isNetworkConnected(Context activity, boolean checkInBackground) {
//        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo netInfo = cm.getActiveNetworkInfo();
//        if (netInfo != null && netInfo.isConnectedOrConnecting())
//            return true;
//        else if (!checkInBackground)
//            showNoInternetPopUp((Activity) activity);
//        return false;
//    }

    /**
     * Show pop up if no Internet connected
     *
     * @param activity
     */
//    public static void showNoInternetPopUp(Activity activity) {
//        if (!HomeActivity.isNoInternetPopUpShown) {
//            HomeActivity.isNoInternetPopUpShown = true;
//            new NoInternetDialogBox(context).show();
//        }
//    }

    /**
     * Check Values for NULL
     *
     * @param activity
     * @param models
     * @return
     */
    public static boolean checkForNull(Activity activity, NullCheckModel... models) {
        for (NullCheckModel model : models) {
            if (model.data == null || model.data.equals("")) {
                FreeTimrUtils.showMessage(activity, model.message + " must not be null.");
                return false;
            }
        }
        return true;
    }

    public static String getErrorMessage(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            return jsonObject.getString("Message");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Show Message in Toast
     *
     * @param context
     * @param message
     */
    public static void showMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Encode Bitmap to Base64 String
     *
     * @param image Bitmap Image
     * @return Base64 String
     */
    public static String encodeTobase64(Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        Log.d("Image Log:", imageEncoded);
        return imageEncoded;
    }

    /**
     * Decode Base64 String to Bitmap
     *
     * @param input String Base64
     * @return Bitmap
     */
    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    /**
     *
     */
    static class DialogDisplay implements Runnable {
        Activity activity;
        String message;

        public DialogDisplay updateActivity(Activity activity, String message) {
            this.activity = activity;
            this.message = message;
            return this;
        }


        @Override
        public void run() {
            try {
                if (pDialog != null) {
                    pDialog.cancel();
                }
                pDialog = new LoadingDialogBox(activity);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.setCancelable(false);
                pDialog.setMessage(message);
                pDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static int getAge(int _year, int _month, int _day) {

        GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, a;

        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(_year, _month, _day);
        a = y - cal.get(Calendar.YEAR);
        if ((m < cal.get(Calendar.MONTH))
                || ((m == cal.get(Calendar.MONTH)) && (d < cal
                .get(Calendar.DAY_OF_MONTH)))) {
            --a;
        }
        if (a < 0)
            throw new IllegalArgumentException("Age < 0");
        return a;
    }

    public static ArrayList<AvailabilityTimeModel> getAvailableTimeList(Context context) {
        ArrayList<AvailabilityTimeModel> availabilityTimeModelList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        AvailabilityTimeModel[] availabilityTimeModels = jobMastersModel.AvailabilityTime;

        for (int i = 0; i < availabilityTimeModels.length; i++) {
            availabilityTimeModelList.add(availabilityTimeModels[i]);
        }
        return availabilityTimeModelList;
    }

    public static ArrayList<AvailabilityDay> getAvailableDayList(Context context) {
        ArrayList<AvailabilityDay> availabilityDaysList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        AvailabilityDay[] availabilityDays = jobMastersModel.AvailabilityDay;

        for (int i = 0; i < availabilityDays.length; i++) {
            availabilityDaysList.add(availabilityDays[i]);
        }
        return availabilityDaysList;
    }

    public static ArrayList<WorkLocationModel> getWorkLocationList(Context context) {
        ArrayList<WorkLocationModel> workLocationModelList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        WorkLocationModel[] workLocationModels = jobMastersModel.WorkLocation;

        for (int i = 0; i < workLocationModels.length; i++) {
            workLocationModelList.add(workLocationModels[i]);
        }
        return workLocationModelList;
    }

    // CANDIDATE
    public static ArrayList<AvailabilityTime> getCandidateAvailableTimeList(Context context) {
        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_CANDIDATE_MASTERS, "");

        CandidateMastersModel candidateMastersModel = newGson().fromJson(jobCompanyStr, CandidateMastersModel.class);

        ArrayList<AvailabilityTime> availabilityTimeModels = candidateMastersModel.availabilityTimes;

        return availabilityTimeModels;
    }

    public static ArrayList<AvailabilityDay> getCandidateAvailableDayList(Context context) {
        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_CANDIDATE_MASTERS, "");

        CandidateMastersModel candidateMastersModel = newGson().fromJson(jobCompanyStr, CandidateMastersModel.class);

        ArrayList<AvailabilityDay> availabilityDays = candidateMastersModel.availabilityDays;

        return availabilityDays;
    }

    public static ArrayList<WorkLocation> getCandidateWorkLocationList(Context context) {
        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_CANDIDATE_MASTERS, "");

        CandidateMastersModel candidateMastersModel = newGson().fromJson(jobCompanyStr, CandidateMastersModel.class);

        ArrayList<WorkLocation> workLocationModels = candidateMastersModel.workLocations;

        return workLocationModels;
    }

    public static ArrayList<Skill> getCandidateSkillsList(Context context) {
        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_CANDIDATE_MASTERS, "");
        CandidateMastersModel candidateMastersModel = newGson().fromJson(jobCompanyStr, CandidateMastersModel.class);
        ArrayList<Skill> skills = candidateMastersModel.skills;
        return skills;
    }

    public static ArrayList<CandidateSkillRoleModel> getSkillRoleList(Context context) {
        String candidateMasterStr = SharedPreferenceStore.getValue(context, Constants.KEY_CANDIDATE_MASTERS, "");
        CandidateMastersModel candidateMastersModel = newGson().fromJson(candidateMasterStr, CandidateMastersModel.class);
        ArrayList<CandidateSkillRoleModel> skillRole = candidateMastersModel.SkillRole;
        return skillRole;
    }

    public static ArrayList<JobCategoryModel> getJobCategoryList(Context context) {
        ArrayList<JobCategoryModel> jobCategoryModelsList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        JobCategoryModel[] jobCategoryModels = jobMastersModel.JobCategory;

        for (int i = 0; i < jobCategoryModels.length; i++) {
            jobCategoryModelsList.add(jobCategoryModels[i]);
        }
        return jobCategoryModelsList;
    }

    public static ArrayList<CityModel> getCityList(Context context) {
        ArrayList<CityModel> cityModelsList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        CityModel[] cityModels = jobMastersModel.City;

        for (int i = 0; i < cityModels.length; i++) {
            cityModelsList.add(cityModels[i]);
        }
        return cityModelsList;
    }

    public static String getCityNameById(Context context, int id) {
        ArrayList<CityModel> cityModelsList = getCityList(context);
        for (int i = 0; i < cityModelsList.size(); i++) {
            if (cityModelsList.get(i).CityId == id) {
                return cityModelsList.get(i).CityName;
            }
        }
        return "";
    }

    public String getWorkLocationById(Context context, int id) {
        JobMastersModel jobMastersModel = newGson().fromJson(SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, ""), JobMastersModel.class);

        WorkLocationModel[] workLocations = jobMastersModel.WorkLocation;

        for (int i = 0; i < workLocations.length; i++) {
            if (id == workLocations[i].WorkLocationId) {
                return workLocations[i].WorkLocationName;
            }
        }

        return null;
    }

    public static String getGraduationNameById(Context context, int id) {
        String gradName = "";

        ArrayList<Graduation> graduations = getCandidateMasters(context).graduations;
        for (int i = 0; i < graduations.size(); i++) {
            if (graduations.get(i).id == id) {
                gradName = graduations.get(i).name;
            }
        }

        return gradName;
    }

    public static String getSpecializationById(Context context, int id) {
        String specName = "";

        ArrayList<Specialization> specializations = getCandidateMasters(context).specializations;
        for (int i = 0; i < specializations.size(); i++) {
            if (specializations.get(i).id == id) {
                specName = specializations.get(i).name;
            }
        }

        return specName;
    }

    public static String getGradingSystemById(Context context, int id) {
        String specName = "";

        ArrayList<GradingSystem> gradingSystems = getCandidateMasters(context).gradingSystems;
        for (int i = 0; i < gradingSystems.size(); i++) {
            if (gradingSystems.get(i).id == id) {
                specName = gradingSystems.get(i).name;
            }
        }

        return specName;
    }

    public static String getSkillName(Context context, int id) {
        String name = "";

        ArrayList<Skill> skills = getCandidateMasters(context).skills;
        for (int i = 0; i < skills.size(); i++) {
            if (skills.get(i).id == id) {
                name = skills.get(i).name;
            }
        }
        return name;
    }

    public static ArrayList<LanguageModel> getLanguageList(Context context) {
        ArrayList<LanguageModel> model = getCandidateMasters(context).Language;
        return model;
    }
}
