package com.freetimr.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.freetimr.R;
import com.freetimr.adapter.EducationAdapter;
import com.freetimr.fragments.ProfileQualificationAndSkillsFragment;
import com.freetimr.model.CandidateEducations;
import com.freetimr.model.CandidateExperiences;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.FreeTimrUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EducationActivity extends AppCompatActivity {

    public static final int ADD_EDUCATION = 199;
    RecyclerView educationRecyclerView;
    LinearLayout addEducation;
    ImageView update, updateCancel;
    ArrayList<ProfileModel> profileModel;
    ArrayList<CandidateEducations> candidateEducationArrayList;
    EducationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_education);

        educationRecyclerView = (RecyclerView) findViewById(R.id.education_rv);
        addEducation = (LinearLayout) findViewById(R.id.add_education);
        update = (ImageView) findViewById(R.id.update_btn);
        updateCancel = (ImageView) findViewById(R.id.update_cancel);

        profileModel = FreeTimrUtils.getCandidateProfile(this);
        setEducationAdapter(profileModel.get(0).CandidateEducation);

        addEducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EducationActivity.this, AddEducationQualificationActivity.class);
                intent.putExtra("IS_SINGLE_ENTRY",true);
                startActivityForResult(intent, ADD_EDUCATION);
            }
        });

        updateCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
//                if (profileModel[0] != null)
//                    updateEducationExperience(profileModel[0]);
            }
        });
    }

    private void updateEducationExperience(ProfileModel profileModel) {

        ProfileModel profileModelRequest = new ProfileModel();
        profileModelRequest.CandidateId = profileModel.CandidateId;
        profileModelRequest.CandidateEducation = profileModel.CandidateEducation;

        FreeTimrUtils.log("" + FreeTimrUtils.newGson().toJson(profileModelRequest));

        Call<String> updateCall = FreeTimrUtils.getRetrofit().updateCandidateProfile(profileModel);
        updateCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                FreeTimrUtils.log("" + response.code());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                FreeTimrUtils.log("" + t);
            }
        });
    }

    private void initEducationArrayList() {
        candidateEducationArrayList = new ArrayList<>();
        for (int i = 0; i < profileModel.get(0).CandidateEducation.size(); i++)
            candidateEducationArrayList.add(profileModel.get(0).CandidateEducation.get(i));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ADD_EDUCATION) {

                initEducationArrayList();

                CandidateEducations candidateEducations = data.getParcelableExtra("Education");
                candidateEducationArrayList.add(candidateEducations);
                profileModel.get(0).CandidateEducation = candidateEducationArrayList;

                setEducationAdapter(profileModel.get(0).CandidateEducation);
            }
        } else {
            FreeTimrUtils.showMessage(EducationActivity.this, "Action Canceled");
        }
    }

    public void setEducationAdapter(ArrayList<CandidateEducations> candidateEducations) {
        adapter = new EducationAdapter(EducationActivity.this, candidateEducations, this);
        educationRecyclerView.setAdapter(adapter);
        educationRecyclerView.setLayoutManager(new LinearLayoutManager(EducationActivity.this));
    }
}
