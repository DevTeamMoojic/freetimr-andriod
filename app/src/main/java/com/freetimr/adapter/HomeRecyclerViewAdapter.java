package com.freetimr.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.activities.CompanyProfileActivity;
import com.freetimr.activities.JobsListActivity;
import com.freetimr.activities.ProfileActivity;
import com.freetimr.model.HomeModel;
import com.freetimr.model.JobCategoryModel;
import com.freetimr.utils.FreeTimrUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Admin on 12-Dec-16.
 */

public class HomeRecyclerViewAdapter extends RecyclerView.Adapter<HomeRecyclerViewAdapter.HomeViewHolder> {

    Context context;
    LayoutInflater inflater;
    JobCategoryModel[] jobCategoryModels;

    public HomeRecyclerViewAdapter (Context context, JobCategoryModel[] jobCategoryModels) {
        this.context = context;
        this.jobCategoryModels = jobCategoryModels;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public HomeViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view;
        view = inflater.inflate(R.layout.home_list_item, parent, false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder (HomeViewHolder holder, final int position) {
        holder.name.setText(jobCategoryModels[position].JobCategoryName);
//        holder.name.setVisibility(View.GONE);
//        FreeTimrUtils.log("http://137.59.54.53/FreeTimr/Images/JobCategory/"+jobCategoryModels[position].JobCategoryImage);
        Picasso.with(context).load("http://137.59.54.53/FreeTimr/Images/JobCategory/"+jobCategoryModels[position].JobCategoryImage).into(holder.background);
//        holder.logo.setVisibility(View.GONE);
//        holder.background.setImageResource(list.get(position).getBackground());
//        holder.logo.setImageResource(list.get(position).getLogo());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                Intent intent = new Intent(context, JobsListActivity.class);
                intent.putExtra("Selected_Cat",position);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount () {
        return jobCategoryModels.length;
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {

        ImageView background, logo;
        TextView name;

        public HomeViewHolder (View itemView) {
            super(itemView);

            background = (ImageView) itemView.findViewById(R.id.background);
            logo = (ImageView) itemView.findViewById(R.id.logo);
            name = (TextView) itemView.findViewById(R.id.name);
        }
    }

}
