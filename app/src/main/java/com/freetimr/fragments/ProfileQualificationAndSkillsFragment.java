package com.freetimr.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.freetimr.R;
import com.freetimr.activities.AddEducationQualificationActivity;
import com.freetimr.activities.AddWorkExperienceActivity;
import com.freetimr.activities.HomeActivity;
import com.freetimr.adapter.EducationAdapter;
import com.freetimr.adapter.ProfileSkillsAdapter;
import com.freetimr.adapter.WorkExperienceAdapter;
import com.freetimr.model.CandidateEducations;
import com.freetimr.model.CandidateExperiences;
import com.freetimr.model.CandidateModel;
import com.freetimr.model.CandidateSkills;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.Constants;
import com.freetimr.utils.FreeTimrUtils;
import com.freetimr.utils.SharedPreferenceStore;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 28-Dec-16.
 */

public class ProfileQualificationAndSkillsFragment extends Fragment {

    private static final int ADD_WORK_EXP = 159;
    private static final int ADD_EDUCATION_QUALIFICATION = 170;

    Button mRegisterBtn;
    LinearLayout mAddWorkExperience, mAddEducationBtn;
    ProfileModel profileModel;
    RecyclerView workExpRv, educationRv, skillsRecyclerView;
    LinearLayout noWorkExp, noEducation;
    RadioButton skillsWebApplications, skillsLogistics, skillsTyping;

    WorkExperienceAdapter workExperienceAdapter;
    EducationAdapter educationAdapter;

    ArrayList<CandidateExperiences> candidateExperiencesArrayList;
    ArrayList<CandidateEducations> candidateEducationArrayList;

    int skillIdTemp = -1;

    public static ProfileQualificationAndSkillsFragment getInstance(ProfileModel profileModel) {
        ProfileQualificationAndSkillsFragment fragment = new ProfileQualificationAndSkillsFragment();
        Bundle args = new Bundle();
        args.putParcelable("ProfileModel", profileModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileModel = getArguments().getParcelable("ProfileModel");
        candidateExperiencesArrayList = new ArrayList<>();
        candidateEducationArrayList = new ArrayList<>();
        profileModel.CandidateSkill = new ArrayList<>();
//                profileModel.CandidateSkill = new CandidateSkills[1];
//        profileModel.CandidateSkill[0] = new CandidateSkills();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_profile_qualification_and_skills, container, false);

        mRegisterBtn = (Button) rootView.findViewById(R.id.proceed_btn);
        mAddWorkExperience = (LinearLayout) rootView.findViewById(R.id.add_work_exp);
        mAddEducationBtn = (LinearLayout) rootView.findViewById(R.id.add_education_btn);
        workExpRv = (RecyclerView) rootView.findViewById(R.id.work_exp_rv);
        noWorkExp = (LinearLayout) rootView.findViewById(R.id.no_work_exp);
        educationRv = (RecyclerView) rootView.findViewById(R.id.education_rv);
        noEducation = (LinearLayout) rootView.findViewById(R.id.no_education);
        skillsWebApplications = (RadioButton) rootView.findViewById(R.id.skills_name_web_applications);
        skillsLogistics = (RadioButton) rootView.findViewById(R.id.skills_name_logistics);
        skillsTyping = (RadioButton) rootView.findViewById(R.id.skills_name_web_applications);
        skillsRecyclerView = (RecyclerView) rootView.findViewById(R.id.skills_recycler_view);

        ProfileSkillsAdapter adapter = new ProfileSkillsAdapter(ProfileQualificationAndSkillsFragment.this.getContext(), FreeTimrUtils.getCandidateSkillsList(ProfileQualificationAndSkillsFragment.this.getContext()), ProfileQualificationAndSkillsFragment.this);
        skillsRecyclerView.setAdapter(adapter);
        skillsRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false));

        setWorkExpAdapter(profileModel.CandidateExperience.isEmpty() ? new ArrayList<CandidateExperiences>() : profileModel.CandidateExperience);
        setEducationAdapter(profileModel.CandidateEducation.isEmpty() ? new ArrayList<CandidateEducations>() : profileModel.CandidateEducation);

        mRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // REGISTER USER
                if (profileModel.CandidateExperience == null && profileModel.CandidateEducation == null) {
                    FreeTimrUtils.showMessage(ProfileQualificationAndSkillsFragment.this.getActivity(), "Please add Work experience and Educational qualifications");
                } else if (skillIdTemp == -1) {
                    FreeTimrUtils.showMessage(ProfileQualificationAndSkillsFragment.this.getActivity(), "Please select skills");
                } else {

                    CandidateSkills candidateSkills = new CandidateSkills();
                    candidateSkills.SkillId = skillIdTemp;
                    profileModel.CandidateSkill.add(candidateSkills);

                    FreeTimrUtils.showProgressDialog(ProfileQualificationAndSkillsFragment.this.getActivity());
                    makeCandidateRegisterAPICall(profileModel);
                }
            }
        });

        mAddWorkExperience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileQualificationAndSkillsFragment.this.getActivity(), AddWorkExperienceActivity.class);
                startActivityForResult(intent, ADD_WORK_EXP);
            }
        });

        mAddEducationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileQualificationAndSkillsFragment.this.getActivity(), AddEducationQualificationActivity.class);
                startActivityForResult(intent, ADD_EDUCATION_QUALIFICATION);
            }
        });

        skillsWebApplications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                skillIdTemp = 3;
//                profileModel.CandidateSkill.get(0).SkillId = 3;
            }
        });

        skillsLogistics.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                skillIdTemp = 2;
//                profileModel.CandidateSkill.get(0).SkillId = 2;
            }
        });

        skillsTyping.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                skillIdTemp = 1;
//                profileModel.CandidateSkill.get(0).SkillId = 1;
            }
        });

        return rootView;
    }

    public void setSkillIdTemp(int id) {
        skillIdTemp = id;
    }

    private void setWorkExpAdapter(ArrayList<CandidateExperiences> candidateExperiences) {
        if (candidateExperiencesArrayList == null || candidateExperiencesArrayList.isEmpty()) {
            noWorkExp.setVisibility(View.VISIBLE);
            workExpRv.setVisibility(View.GONE);
        } else {
            noWorkExp.setVisibility(View.GONE);
            workExpRv.setVisibility(View.VISIBLE);

            workExperienceAdapter = new WorkExperienceAdapter(ProfileQualificationAndSkillsFragment.this.getActivity(), candidateExperiences);
            workExpRv.setAdapter(workExperienceAdapter);
            workExpRv.setLayoutManager(new LinearLayoutManager(ProfileQualificationAndSkillsFragment.this.getActivity()));
        }
    }

    private void setEducationAdapter(ArrayList<CandidateEducations> candidateEducations) {
        if (candidateEducationArrayList == null || candidateEducationArrayList.isEmpty()) {
            noEducation.setVisibility(View.VISIBLE);
            educationRv.setVisibility(View.GONE);
        } else {
            noEducation.setVisibility(View.GONE);
            educationRv.setVisibility(View.VISIBLE);

            educationAdapter = new EducationAdapter(ProfileQualificationAndSkillsFragment.this.getActivity(), candidateEducations);
            educationRv.setAdapter(educationAdapter);
            educationRv.setLayoutManager(new LinearLayoutManager(ProfileQualificationAndSkillsFragment.this.getActivity()));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ADD_WORK_EXP) {
                CandidateExperiences candidateExperiences = data.getParcelableExtra("Work Experience");
                candidateExperiencesArrayList.add(candidateExperiences);
                profileModel.CandidateExperience = candidateExperiencesArrayList;

                setWorkExpAdapter(profileModel.CandidateExperience);

            } else if (requestCode == ADD_EDUCATION_QUALIFICATION) {
                CandidateEducations candidateEducations = data.getParcelableExtra("Education");
                candidateEducationArrayList.add(candidateEducations);
                profileModel.CandidateEducation = candidateEducationArrayList;

                setEducationAdapter(profileModel.CandidateEducation);
            }
        } else {
            FreeTimrUtils.showMessage(ProfileQualificationAndSkillsFragment.this.getActivity(), "Action Canceled");
        }
    }

    private void makeCandidateRegisterAPICall(final ProfileModel profileModel) {

        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(profileModel));

        Call<String> registerCandidate = FreeTimrUtils.getRetrofit().registerCandidateProfile(profileModel);
        registerCandidate.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    profileModel.CandidateProfileId = response.body();

                    CandidateModel candidateModel = FreeTimrUtils.getCandidateInfo(ProfileQualificationAndSkillsFragment.this.getActivity());

                    ArrayList<ProfileModel> profileModels = new ArrayList<>();
                    profileModels.add(profileModel);

                    candidateModel.setProfileModels(profileModels);

                    FreeTimrUtils.saveCandidateInfo(ProfileQualificationAndSkillsFragment.this.getActivity(), FreeTimrUtils.newGson().toJson(candidateModel));
                    FreeTimrUtils.saveCandidateProfile(ProfileQualificationAndSkillsFragment.this.getActivity(), FreeTimrUtils.newGson().toJson(profileModels));

                    Intent intent = new Intent(ProfileQualificationAndSkillsFragment.this.getActivity(), HomeActivity.class);
                    startActivity(intent);
                    ProfileQualificationAndSkillsFragment.this.getActivity().finish();
                    SharedPreferenceStore.storeValue(ProfileQualificationAndSkillsFragment.this.getActivity(), Constants.IS_RPOFILE_COMPLETE, true);
                } else {
                    FreeTimrUtils.showMessage(ProfileQualificationAndSkillsFragment.this.getActivity(), "Something went wrong. Please try again later.");
                }

                FreeTimrUtils.cancelCurrentDialog(ProfileQualificationAndSkillsFragment.this.getActivity());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
            }
        });
    }
}
