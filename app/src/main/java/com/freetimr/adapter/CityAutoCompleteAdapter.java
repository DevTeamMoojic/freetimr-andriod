package com.freetimr.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.CityModel;

import java.util.ArrayList;

/**
 * Created by varunbarve on 29/04/17.
 */

public class CityAutoCompleteAdapter extends ArrayAdapter<CityModel> {

    Context context;
    ArrayList<CityModel> list;
    ArrayList<CityModel> listAll;
    ArrayList<CityModel> suggestions;
    LayoutInflater inflater;

    public CityAutoCompleteAdapter(@NonNull Context context, @NonNull ArrayList<CityModel> list) {
        super(context,R.layout.autocomplete_list_item,list);
        this.context  = context;
        this.list = list;
        this.listAll = (ArrayList<CityModel>) list.clone();
        this.suggestions = new ArrayList<>();
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CityHolder holder;
        if(convertView==null){
            holder = new CityHolder();

            convertView = inflater.inflate(R.layout.autocomplete_list_item,parent,false);

            holder.name = (TextView)convertView.findViewById(R.id.label);

            convertView.setTag(holder);
        }else{
            holder = (CityHolder)convertView.getTag();
        }

        holder.name.setText(list.get(position).CityName);

        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    class CityHolder{
        TextView name;
    }

    @Nullable
    @Override
    public CityModel getItem(int position) {
        return list.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new CityFilter();
    }

    class CityFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (CityModel cityModel : listAll) {
                    if (cityModel.CityName.toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(cityModel);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<CityModel> filteredList = (ArrayList<CityModel>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (CityModel c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    }

}
