package com.freetimr.adapter;

import android.content.Context;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.activities.JobDetailsActivity;
import com.freetimr.model.JobsRequestModel;

import java.util.List;

/**
 * Created by Admin on 25-Jan-17.
 */

@RequiresApi(api = Build.VERSION_CODES.N)
public class JobApplicationsAdapter extends RecyclerView.Adapter<JobApplicationsAdapter.JobApplicationListViewHolder> {

    Context context;
    LayoutInflater inflater;
    List<JobsRequestModel> list;

    public JobApplicationsAdapter(Context context, List<JobsRequestModel> list) {
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public JobApplicationListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.job_applications_list_item, parent, false);
        return new JobApplicationListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JobApplicationsAdapter.JobApplicationListViewHolder holder, int position) {
        JobsRequestModel model = list.get(position);

        holder.name.setText(model.JobTitle);
        holder.companyName.setText(model.CompanyName);

        // LOCATIONS
        if (model.JobLocations == null) {
            holder.locationContainer.setVisibility(View.GONE);
        } else {
            holder.locationContainer.setVisibility(View.VISIBLE);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < model.JobLocations.length; i++) {
                stringBuilder.append(model.JobLocations[i].LocationName + ",");
            }

            String jobLocations = "-";
            if (!stringBuilder.toString().isEmpty()) {
                jobLocations = stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1);
            }

            holder.location.setText(jobLocations);
        }

        // PAY
        if (model.PayPerDay == 0) {
            holder.payContainer.setVisibility(View.GONE);
        } else {
            holder.pay.setText(model.PayPerDay + " (per day)");
        }

        holder.itemView.setTag(model);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class JobApplicationListViewHolder extends RecyclerView.ViewHolder {

        TextView name, companyName, location, pay;
        LinearLayout locationContainer, payContainer;

        public JobApplicationListViewHolder(final View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.job_title);
            companyName = (TextView) itemView.findViewById(R.id.company_name);
            location = (TextView) itemView.findViewById(R.id.job_location);
            pay = (TextView) itemView.findViewById(R.id.job_pay);
            locationContainer = (LinearLayout) itemView.findViewById(R.id.job_location_container);
            payContainer = (LinearLayout) itemView.findViewById(R.id.job_pay_container);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, JobDetailsActivity.class);
                    intent.putExtra("JobRequestModel", (Parcelable) itemView.getTag());
                    context.startActivity(intent);
                }
            });
        }
    }

}
