package com.freetimr.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Admin on 16-Feb-17.
 */

public class CandidateMastersModel {

    @SerializedName("AvailabilityDay")
    public ArrayList<AvailabilityDay> availabilityDays;

    @SerializedName("AvailabilityTime")
    public ArrayList<AvailabilityTime> availabilityTimes;

    @SerializedName("Occupation")
    public ArrayList<Occupation> occupations;

    @SerializedName("WorkLocation")
    public ArrayList<WorkLocation> workLocations;

    @SerializedName("Skill")
    public ArrayList<Skill> skills;

    @SerializedName("Graduation")
    public ArrayList<Graduation> graduations;

    @SerializedName("GradingSystem")
    public ArrayList<GradingSystem> gradingSystems;

    @SerializedName("Specialization")
    public ArrayList<Specialization> specializations;

    @SerializedName("NoticePeriod")
    public ArrayList<NoticePeriod> noticePeriods;

    @SerializedName("Language")
    public ArrayList<LanguageModel> Language = new ArrayList<>();

    @SerializedName("SkillRole")
    public ArrayList<CandidateSkillRoleModel> SkillRole;
}