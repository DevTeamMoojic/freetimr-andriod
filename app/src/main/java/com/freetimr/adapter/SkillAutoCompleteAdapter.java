package com.freetimr.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.Skill;

import java.util.ArrayList;

/**
 * Created by varunbarve on 06/09/17.
 */

public class SkillAutoCompleteAdapter extends ArrayAdapter<Skill> {

    Context context;
    LayoutInflater inflater;
    ArrayList<Skill> list;
    ArrayList<Skill> listAll;
    ArrayList<Skill> suggestions;

    public SkillAutoCompleteAdapter (Context context, ArrayList<Skill> list) {
        super(context, R.layout.skill_name_auto_complete_item, list);
        this.context = context;
        this.list = list;
        this.listAll = (ArrayList<Skill>) list.clone();
        this.suggestions = new ArrayList<>();
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView (int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        SkillHolder holder;
        if(convertView==null){
            holder = new SkillHolder();

            convertView = inflater.inflate(R.layout.skill_name_auto_complete_item,parent,false);

            holder.name = (TextView)convertView.findViewById(R.id.skill_name_autocomplete_item);

            convertView.setTag(holder);
        }else{
            holder = (SkillHolder)convertView.getTag();
        }

        holder.name.setText(list.get(position).name);
        return convertView;
    }

    public class SkillHolder{
        TextView name;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Nullable
    @Override
    public Skill getItem(int position) {
        return list.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new SkillFilter();
    }

    class SkillFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (Skill skill : listAll) {
                    if (skill.name.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(skill);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Skill> filteredList = (ArrayList<Skill>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (Skill c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    }
}
