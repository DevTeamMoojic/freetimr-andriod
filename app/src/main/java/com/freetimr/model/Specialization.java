package com.freetimr.model;

import com.google.gson.annotations.SerializedName;

public class Specialization {
    @SerializedName("SpecializationId")
    public int id;
    @SerializedName("SpecializationName")
    public String name;
}
