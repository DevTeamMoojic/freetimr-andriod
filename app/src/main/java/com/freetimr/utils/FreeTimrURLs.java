package com.freetimr.utils;

/**
 * Created by Admin on 09-Feb-17.
 */

public class FreeTimrURLs {
    public static NetworkConnector retrofitNetworkHandler;

    public static final String HOST = "http://137.59.54.53/FreeTimr/api/";

    public static final String REGISTER_CANDIDATE = HOST + "Candidate";
    public static final String GET_CANDIDATE = HOST + "Candidate";
    public static final String GET_CANDIDATE_PROFILE_BY_ID = HOST + "Candidate/Profile/GetProfileByCandidateId";
    public static final String GET_CANDIDATE_PROFILE = HOST + "Candidate/Profile";

    public static final String GET_CANDIDATE_MASTERS = HOST + "Masters/CandidateMasters";
    public static final String GET_JOB_MASTER = "Masters/JobMasters";
    public static final String GET_COMPANY = "Company/";
    public static final String APPLY_JOB = "Candidate/ApplyJob/";
    public static final String GET_APPLIED_JOB = "Candidate/AppliedJobs/";
    public static final String LOGIN = "Candidate/AuthCandidate/";
    public static final String GET_PROFILE = "Candidate/Profile/GetProfileByCandidateId";
    public static final String ADD_CITY = "Candidate/Profile/CandidatePreferredCity/Create?candidateProfileId=";
    public static final String DELETE_CITY = "Candidate/Profile/CandidatePreferredCity/Delete?CandidatePreferredCityId=";
    public static final String ADD_WORK_EXP = "Candidate/Profile/CandidateExperience/Create?candidateProfileId=";
    public static final String DELETE_WORK_EXP = "Candidate/Profile/CandidateExperience/Delete?CandidateExperienceId=";
    public static final String ADD_EDUCATION = "Candidate/Profile/CandidateEducation/Create?candidateProfileId=";
    public static final String DELETE_EDUCATION = "Candidate/Profile/CandidateEducation/Delete?CandidateEducationId=";
    public static final String ADD_SKILLS = "Candidate/Profile/CandidateSkill/Create?candidateProfileId=";
    public static final String DELETE_SKILLS = "Candidate/Profile/CandidateSkill/Delete?CandidateSkillId=";
    public static final String CREATE_SKILLS_ROLE = "Candidate/Profile/CandidateSkillRole/Create?candidateProfileId=";

    public static final String GET_JOB_BY_COMPANY_ID = "Company/Job/GetJobsByCompany/";

}
