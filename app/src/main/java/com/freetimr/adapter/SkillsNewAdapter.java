package com.freetimr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.SkillsModel;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.ArrayList;

/**
 * Created by Admin on 11-Mar-17.
 */

public class SkillsNewAdapter extends RecyclerView.Adapter<SkillsNewAdapter.SkillsNewViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<SkillsModel> list;

    public SkillsNewAdapter(Context context, ArrayList<SkillsModel> list) {
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SkillsNewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.skill_set_list_item, parent, false);
        return new SkillsNewAdapter.SkillsNewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SkillsNewViewHolder holder, int position) {
        SkillsModel skillsModel = list.get(position);
        holder.name.setText(skillsModel.getName());
        holder.seekBar.setProgress(skillsModel.getPercent());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class SkillsNewViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        DiscreteSeekBar seekBar;

        public SkillsNewViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.skill_name);
            seekBar = (DiscreteSeekBar) itemView.findViewById(R.id.skills_percent);
        }
    }
}
