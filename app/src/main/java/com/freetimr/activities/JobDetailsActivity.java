package com.freetimr.activities;

import android.content.Intent;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.ApplyJobModel;
import com.freetimr.model.CompanyModel;
import com.freetimr.model.JobsRequestModel;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.N)
public class JobDetailsActivity extends AppCompatActivity {

    Toolbar mToolbar;
    TextView jobTitle, companyName, createdBy, salary, timings, openings, companyLocation, jobDescription, jobEligibility, jobBenefits, jobDescriptionLabel, jobEligibilityLabel, jobBenefitsLabel, knowMore;
    Button applyBtn;
    JobsRequestModel jobsRequestModel;
    CompanyModel companyModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_details);
        mToolbar = (Toolbar) findViewById(R.id.job_details_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        jobsRequestModel = getIntent().getParcelableExtra("JobRequestModel");

        jobTitle = (TextView) findViewById(R.id.job_details_job_title);
        companyName = (TextView) findViewById(R.id.job_details_company_name);
        createdBy = (TextView) findViewById(R.id.job_details_created_by);
        salary = (TextView) findViewById(R.id.job_details_job_pay);
        timings = (TextView) findViewById(R.id.job_details_timings);
        openings = (TextView) findViewById(R.id.job_details_openings);
        companyLocation = (TextView) findViewById(R.id.job_details_company_location);
        jobDescription = (TextView) findViewById(R.id.job_details_description);
        jobEligibility = (TextView) findViewById(R.id.job_details_eligibility);
        jobBenefits = (TextView) findViewById(R.id.job_details_benefits);
        jobDescriptionLabel = (TextView) findViewById(R.id.job_details_description_label);
        jobEligibilityLabel = (TextView) findViewById(R.id.job_details_eligibility_label);
        jobBenefitsLabel = (TextView) findViewById(R.id.job_details_benefits_label);
        applyBtn = (Button) findViewById(R.id.apply_btn);
        knowMore = (TextView) findViewById(R.id.know_more_company);

        getCompanyModel();

        applyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                applyForThisJob();
//                Intent intent = new Intent(JobDetailsActivity.this, ChatActivity.class);
//                intent.putExtra("JobRequestModel", jobsRequestModel);
//                intent.putExtra("CompanyModel", companyModel);
//                startActivity(intent);
            }
        });

        knowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JobDetailsActivity.this, CompanyProfileActivity.class);
//                intent.putExtra("JobRequestModel", jobsRequestModel);
                intent.putExtra("CompanyModel", companyModel);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void getCompanyModel() {
        FreeTimrUtils.showProgressDialog(this);
        Call<CompanyModel> call = FreeTimrUtils.getRetrofit().getCompanyModel(FreeTimrURLs.HOST + FreeTimrURLs.GET_COMPANY + jobsRequestModel.CompanyId);
        call.enqueue(new Callback<CompanyModel>() {
            @Override
            public void onResponse(Call<CompanyModel> call, Response<CompanyModel> response) {
                FreeTimrUtils.log("" + response.code());
                companyModel = response.body();
                updateFields(jobsRequestModel, companyModel);
            }

            @Override
            public void onFailure(Call<CompanyModel> call, Throwable t) {
                FreeTimrUtils.log("" + t);
            }
        });
    }

    private void updateFields(JobsRequestModel jobsRequestModel, CompanyModel companyModel) {
        FreeTimrUtils.cancelCurrentDialog(this);

        jobTitle.setText(jobsRequestModel.JobTitle);
        companyName.setText(companyModel.CompanyName);
        createdBy.setText(jobsRequestModel.CreatedDate == null ? "-" : "Posted on " + new SimpleDateFormat("dd-MM-yyyy").format(jobsRequestModel.CreatedDate));
        salary.setText("" + (int) jobsRequestModel.PayPerDay);
        if (jobsRequestModel.StartTime != null && jobsRequestModel.EndTime != null) {
            timings.setText(jobsRequestModel.StartTime.substring(0, 2) + " to " + jobsRequestModel.EndTime.substring(0, 2));
        } else {
            timings.setText("-");
        }

        openings.setText("" + jobsRequestModel.NoOfPositions);
        companyLocation.setText("" + FreeTimrUtils.getCityNameById(this, companyModel.CityId));

        if (jobsRequestModel.JobDescription == null) {
//            jobDescription.setVisibility(View.GONE);
//            jobDescriptionLabel.setVisibility(View.GONE);
            jobDescription.setVisibility(View.VISIBLE);
            jobDescriptionLabel.setVisibility(View.VISIBLE);
            jobDescription.setText("-");
        } else {
            jobDescription.setVisibility(View.VISIBLE);
            jobDescriptionLabel.setVisibility(View.VISIBLE);
            jobDescription.setText(jobsRequestModel.JobDescription);
        }

        if (jobsRequestModel.JobEligibility == null) {
//            jobEligibility.setVisibility(View.GONE);
//            jobEligibilityLabel.setVisibility(View.GONE);
            jobEligibility.setVisibility(View.VISIBLE);
            jobEligibilityLabel.setVisibility(View.VISIBLE);
            jobEligibility.setText("-");
        } else {
            jobEligibility.setVisibility(View.VISIBLE);
            jobEligibilityLabel.setVisibility(View.VISIBLE);
            jobEligibility.setText(jobsRequestModel.JobEligibility);
        }

        if (jobsRequestModel.JobBenefits == null) {
//            jobBenefits.setVisibility(View.GONE);
//            jobBenefitsLabel.setVisibility(View.GONE);
            jobBenefits.setVisibility(View.VISIBLE);
            jobBenefitsLabel.setVisibility(View.VISIBLE);
            jobBenefits.setText("-");
        } else {
            jobBenefits.setVisibility(View.VISIBLE);
            jobBenefitsLabel.setVisibility(View.VISIBLE);
            jobBenefits.setText(jobsRequestModel.JobBenefits);
        }

        if (jobsRequestModel.JobApplication.length != 0) {
            if (jobsRequestModel.JobApplication[0].JobApplicationStatus != null) {
                if (jobsRequestModel.JobApplication[0].JobApplicationStatus.JobApplicationStatusId == 2) {
                    applyBtn.setText(jobsRequestModel.JobApplication[0].JobApplicationStatus.JobApplicationStatusName);
                    applyBtn.setBackgroundColor(Color.parseColor("#e5e5e5"));
                    applyBtn.setEnabled(false);
                }
            }
        }
    }

    private void applyForThisJob() {
        if (FreeTimrUtils.getCandidateInfo(this) != null) {

            FreeTimrUtils.showProgressDialog(this);

            Call<ApplyJobModel> call = FreeTimrUtils.getRetrofit().applyJob(FreeTimrURLs.HOST + FreeTimrURLs.APPLY_JOB + jobsRequestModel.JobId + "?candidateId=" + FreeTimrUtils.getCandidateInfo(this).Id);

            FreeTimrUtils.log(FreeTimrURLs.HOST + FreeTimrURLs.APPLY_JOB + jobsRequestModel.JobId + "?candidateId=" + FreeTimrUtils.getCandidateInfo(this).Id);

            call.enqueue(new Callback<ApplyJobModel>() {
                @Override
                public void onResponse(Call<ApplyJobModel> call, Response<ApplyJobModel> response) {
                    if (response.code() == 200) {
                        applyBtn.setText("Applied");
                        applyBtn.setBackgroundColor(Color.parseColor("#e5e5e5"));
                        applyBtn.setEnabled(false);
                    } else {
                        FreeTimrUtils.showMessage(JobDetailsActivity.this, "Something went wrong. Please try again later.");
                    }

                    FreeTimrUtils.cancelCurrentDialog(JobDetailsActivity.this);
                }

                @Override
                public void onFailure(Call<ApplyJobModel> call, Throwable t) {
                    FreeTimrUtils.log(t.getLocalizedMessage());
                    FreeTimrUtils.cancelCurrentDialog(JobDetailsActivity.this);
                }
            });
        } else {
            Intent intent = new Intent(JobDetailsActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    }
}
