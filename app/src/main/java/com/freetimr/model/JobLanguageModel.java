package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 23-Mar-17.
 */

public class JobLanguageModel implements Parcelable {

    public int JobLanguageId;
    public String JobId;
    public int LanguageId;

    protected JobLanguageModel(Parcel in) {
        JobLanguageId = in.readInt();
        JobId = in.readString();
        LanguageId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(JobLanguageId);
        dest.writeString(JobId);
        dest.writeInt(LanguageId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobLanguageModel> CREATOR = new Creator<JobLanguageModel>() {
        @Override
        public JobLanguageModel createFromParcel(Parcel in) {
            return new JobLanguageModel(in);
        }

        @Override
        public JobLanguageModel[] newArray(int size) {
            return new JobLanguageModel[size];
        }
    };
}
