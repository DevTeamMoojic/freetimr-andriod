package com.freetimr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.freetimr.R;
import com.freetimr.fragments.ProfileDetailSecondPageFragment;
import com.freetimr.model.WorkLocation;

import java.util.ArrayList;

/**
 * Created by varunbarve on 05/02/18.
 */

public class ProfileWorkLocationAdapter extends RecyclerView.Adapter<ProfileWorkLocationAdapter.ProfileWorkLocationViewHolder> {

    Context context;
    ArrayList<WorkLocation> list;
    LayoutInflater inflater;
    RadioButton lastChecked;
    ProfileDetailSecondPageFragment fragment;

    private static int lastCheckedPos = 0;

    public ProfileWorkLocationAdapter(Context context, ArrayList<WorkLocation> list, ProfileDetailSecondPageFragment fragment) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.fragment = fragment;
    }

    @Override
    public ProfileWorkLocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.profile_work_location_list_item, parent, false);
        return new ProfileWorkLocationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProfileWorkLocationViewHolder holder, int position) {
        holder.button.setText("" + list.get(position).name);
        holder.button.setChecked(list.get(position).isSelected);
        holder.button.setTag(new Integer(position));

        if (position == 0 && list.get(position).isSelected && holder.button.isChecked()) {
            lastChecked = holder.button;
            lastCheckedPos = 0;
        }

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton cb = (RadioButton) v;
                int clickedPos = ((Integer) cb.getTag()).intValue();

                if (cb.isChecked()) {
                    if (lastChecked != null) {
                        lastChecked.setChecked(false);
                        list.get(lastCheckedPos).isSelected = false;
                    }

                    lastChecked = cb;
                    lastCheckedPos = clickedPos;

                    fragment.setWorkLocationId(list.get(clickedPos).id);
                } else
                    lastChecked = null;

                list.get(clickedPos).isSelected = cb.isSelected();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ProfileWorkLocationViewHolder extends RecyclerView.ViewHolder {

        RadioButton button;

        public ProfileWorkLocationViewHolder(View itemView) {
            super(itemView);
            button = (RadioButton) itemView.findViewById(R.id.radio_btn_employer_location);
        }
    }
}
