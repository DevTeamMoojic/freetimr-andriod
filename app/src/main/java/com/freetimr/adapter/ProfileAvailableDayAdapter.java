package com.freetimr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.freetimr.R;
import com.freetimr.fragments.ProfileDetailSecondPageFragment;
import com.freetimr.model.AvailabilityDay;
import com.freetimr.model.WorkLocationModel;

import java.util.ArrayList;

/**
 * Created by varunbarve on 05/02/18.
 */

public class ProfileAvailableDayAdapter extends RecyclerView.Adapter<ProfileAvailableDayAdapter.ProfileAvailableDayViewHolder> {

    Context context;
    ArrayList<AvailabilityDay> list;
    LayoutInflater inflater;
    RadioButton lastChecked;

    ProfileDetailSecondPageFragment fragment;

    private static int lastCheckedPos = 0;

    public ProfileAvailableDayAdapter(Context context, ArrayList<AvailabilityDay> list, ProfileDetailSecondPageFragment fragment) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.fragment = fragment;
    }

    @Override
    public ProfileAvailableDayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.profile_available_day_list_item, parent, false);
        return new ProfileAvailableDayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProfileAvailableDayViewHolder holder, int position) {
        holder.button.setText("" + list.get(position).name);
        holder.button.setChecked(list.get(position).isSelected);
        holder.button.setTag(new Integer(position));

        if (position == 0 && list.get(position).isSelected && holder.button.isChecked()) {
            lastChecked = holder.button;
            lastCheckedPos = 0;
        }

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton cb = (RadioButton) v;
                int clickedPos = ((Integer) cb.getTag()).intValue();

                if (cb.isChecked()) {
                    if (lastChecked != null) {
                        lastChecked.setChecked(false);
                        list.get(lastCheckedPos).isSelected = false;
                    }

                    lastChecked = cb;
                    lastCheckedPos = clickedPos;

                    fragment.setAvailabilityDayIdTemp(list.get(clickedPos).id);
                } else
                    lastChecked = null;

                list.get(clickedPos).isSelected = cb.isSelected();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ProfileAvailableDayViewHolder extends RecyclerView.ViewHolder {

        RadioButton button;

        public ProfileAvailableDayViewHolder(View itemView) {
            super(itemView);
            button = (RadioButton) itemView.findViewById(R.id.radio_btn_available_day);
        }
    }
}
