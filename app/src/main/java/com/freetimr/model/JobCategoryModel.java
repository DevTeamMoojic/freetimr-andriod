package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 23-Mar-17.
 */

public class JobCategoryModel implements Parcelable {

    public int JobCategoryId;
    public String JobCategoryName;
    public String JobCategoryImage;
    public String JobCategoryImageUrl;


    public JobCategoryModel(int jobCategoryId, String jobCategoryName) {
        JobCategoryId = jobCategoryId;
        JobCategoryName = jobCategoryName;
    }


    protected JobCategoryModel(Parcel in) {
        JobCategoryId = in.readInt();
        JobCategoryName = in.readString();
        JobCategoryImage = in.readString();
        JobCategoryImageUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(JobCategoryId);
        dest.writeString(JobCategoryName);
        dest.writeString(JobCategoryImage);
        dest.writeString(JobCategoryImageUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobCategoryModel> CREATOR = new Creator<JobCategoryModel>() {
        @Override
        public JobCategoryModel createFromParcel(Parcel in) {
            return new JobCategoryModel(in);
        }

        @Override
        public JobCategoryModel[] newArray(int size) {
            return new JobCategoryModel[size];
        }
    };
}
