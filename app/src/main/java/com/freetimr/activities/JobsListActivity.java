package com.freetimr.activities;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.adapter.JobsPagerAdapter;
import com.freetimr.fragments.JobListFragment;
import com.freetimr.model.JobCategoryModel;
import com.freetimr.model.JobsRequestModel;
import com.freetimr.model.SearchRequestModel;
import com.freetimr.utils.FreeTimrUtils;
import com.freetimr.views.SlidingTabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.N)
public class JobsListActivity extends AppCompatActivity {

    public static final int FILTER = 120;

    SlidingTabLayout tabLayout;
    ViewPager jobListViewPager;
    Toolbar mToolbar;
    JobsPagerAdapter pagerAdapter;
    int selectedCategory;
    SearchRequestModel searchRequestModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs_list);
        mToolbar = (Toolbar) findViewById(R.id.job_list_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        selectedCategory = getIntent().getIntExtra("Selected_Cat", 0);

        tabLayout = (SlidingTabLayout) findViewById(R.id.jobs_tab);
        jobListViewPager = (ViewPager) findViewById(R.id.job_list_view_pager);

        searchRequestModel = getIntent().getParcelableExtra("SearchRequestModel");
        if (searchRequestModel == null) {
            searchRequestModel = new SearchRequestModel();
            searchRequestModel.AvailabilityTimeId = "";
            searchRequestModel.CandidateId = FreeTimrUtils.getCandidateInfo(this) != null ? FreeTimrUtils.getCandidateInfo(this).Id : "";
            searchRequestModel.CityId = "";
            searchRequestModel.JobCategoryId = "";
            searchRequestModel.KeywordTosearch = "";
            searchRequestModel.WorkLocationId = "";

            searchJob(searchRequestModel);
        } else {
            searchJob(searchRequestModel);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.job_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.job_list_filter) {
            Intent intent = new Intent(JobsListActivity.this, BrowseJobActivity.class);
            intent.putExtra("IsFilter", true);
            startActivityForResult(intent, FILTER);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == FILTER) {
                searchRequestModel = data.getParcelableExtra("SearchRequestModel");
                searchJob(searchRequestModel);
            }
        }
    }

    private void searchJob(SearchRequestModel searchRequestModel) {

        FreeTimrUtils.showProgressDialog(this);
        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(searchRequestModel));

        Call<JobsRequestModel[]> call = FreeTimrUtils.getRetrofit().searchJob(searchRequestModel);
        call.enqueue(new Callback<JobsRequestModel[]>() {
            @Override
            public void onResponse(Call<JobsRequestModel[]> call, Response<JobsRequestModel[]> response) {
                ArrayList<JobCategoryModel> jobCategoryModelsList = FreeTimrUtils.getJobCategoryList(JobsListActivity.this);
                if (response.body().length == 0) {
                    FreeTimrUtils.showMessage(JobsListActivity.this, "No jobs found.");
                    onBackPressed();
                } else {
                    JobCategoryModel[] jobCategoryModels = new JobCategoryModel[jobCategoryModelsList.size()];
                    for (int i = 0; i < jobCategoryModelsList.size(); i++) {
                        jobCategoryModels[i] = jobCategoryModelsList.get(i);
                    }

                    mapTopologiesAndServices(response.body(), jobCategoryModels);
                }

                FreeTimrUtils.cancelCurrentDialog(JobsListActivity.this);
            }

            @Override
            public void onFailure(Call<JobsRequestModel[]> call, Throwable t) {
                FreeTimrUtils.log("" + t);
                FreeTimrUtils.cancelCurrentDialog(JobsListActivity.this);
            }
        });
    }

    public void mapTopologiesAndServices(JobsRequestModel[] jobModels, JobCategoryModel[] categoryModels) {
        if (jobModels != null && categoryModels.length > 0 && jobModels != null && categoryModels.length > 0) {
            FreeTimrUtils.cancelCurrentDialog(this);
            HashMap<Integer, ArrayList<JobsRequestModel>> servicesMap = new HashMap<>();
            for (int i = 0; i < jobModels.length; i++) {
                JobsRequestModel jobModelsTemp = jobModels[i];
                JobCategoryModel categoryModelsTemp = getCategoryNameById(jobModelsTemp.JobCategoryId, categoryModels);
                if (servicesMap.containsKey(categoryModelsTemp.JobCategoryId)) {
                    servicesMap.get(categoryModelsTemp.JobCategoryId).add(jobModelsTemp);
                } else {
                    ArrayList<JobsRequestModel> jobItemsModels = new ArrayList<>();
                    jobItemsModels.add(jobModelsTemp);
                    servicesMap.put(categoryModelsTemp.JobCategoryId, jobItemsModels);
                }
            }
            initAdapter(categoryModels, servicesMap);
        }
    }

    public void initAdapter(JobCategoryModel[] categoryModels, HashMap<Integer, ArrayList<JobsRequestModel>> jobMap) {
        pagerAdapter = new JobsPagerAdapter(getSupportFragmentManager(), categoryModels, jobMap);
        jobListViewPager.setAdapter(pagerAdapter);
        tabLayout.setViewPager(jobListViewPager);
        jobListViewPager.setCurrentItem(selectedCategory);
    }

    private JobCategoryModel getCategoryNameById(int categoryId, JobCategoryModel[] categoryModels) {
        for (int i = 0; i < categoryModels.length; i++) {
            if (categoryModels[i].JobCategoryId == categoryId)
                return categoryModels[i];
        }
        return new JobCategoryModel(99, "Un-Categories");
    }
}
