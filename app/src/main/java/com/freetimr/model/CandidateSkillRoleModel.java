package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by varunbarve on 02/08/17.
 */

public class CandidateSkillRoleModel implements Parcelable {

    public int SkillRoleId;
    public String SkillRoleName;
    public int SkillId;
    public boolean isSelected;

    protected CandidateSkillRoleModel(Parcel in) {
        SkillRoleId = in.readInt();
        SkillRoleName = in.readString();
        SkillId = in.readInt();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(SkillRoleId);
        dest.writeString(SkillRoleName);
        dest.writeInt(SkillId);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CandidateSkillRoleModel> CREATOR = new Creator<CandidateSkillRoleModel>() {
        @Override
        public CandidateSkillRoleModel createFromParcel(Parcel in) {
            return new CandidateSkillRoleModel(in);
        }

        @Override
        public CandidateSkillRoleModel[] newArray(int size) {
            return new CandidateSkillRoleModel[size];
        }
    };
}
