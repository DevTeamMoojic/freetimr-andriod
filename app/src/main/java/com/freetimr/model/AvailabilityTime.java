package com.freetimr.model;

import com.google.gson.annotations.SerializedName;

public class AvailabilityTime {
    @SerializedName("AvailabilityTimeId")
    public int id;
    @SerializedName("AvailabilityTimeName")
    public String name;

    public boolean isSelected;
}
