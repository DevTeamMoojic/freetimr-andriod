package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 17-Feb-17.
 */

public class CandidateSkills implements Parcelable {

    public int SkillId = -1;
    public int CandidateSkillId;
    public int SkillExperienceInYears;
    public int SkillExperienceInMonths;
    public int SkillRating;
    public SkillModel Skill;

    public CandidateSkills(){}

    protected CandidateSkills(Parcel in) {
        SkillId = in.readInt();
        CandidateSkillId = in.readInt();
        SkillExperienceInYears = in.readInt();
        SkillExperienceInMonths = in.readInt();
        SkillRating = in.readInt();
        Skill = in.readParcelable(SkillModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(SkillId);
        dest.writeInt(CandidateSkillId);
        dest.writeInt(SkillExperienceInYears);
        dest.writeInt(SkillExperienceInMonths);
        dest.writeInt(SkillRating);
        dest.writeParcelable(Skill, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CandidateSkills> CREATOR = new Creator<CandidateSkills>() {
        @Override
        public CandidateSkills createFromParcel(Parcel in) {
            return new CandidateSkills(in);
        }

        @Override
        public CandidateSkills[] newArray(int size) {
            return new CandidateSkills[size];
        }
    };
}
