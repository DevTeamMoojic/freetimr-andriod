package com.freetimr.fragments;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.freetimr.R;
import com.freetimr.adapter.JobListAdapter;
import com.freetimr.model.JobsRequestModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 26-Dec-16.
 */
@RequiresApi(api = Build.VERSION_CODES.N)
public class JobListFragment extends Fragment {

    RecyclerView mJobListRecyclerView;
    JobListAdapter jobListAdapter;
    List<JobsRequestModel> jobList;

    public static JobListFragment getInstance(List<JobsRequestModel> jobList) {
        JobListFragment fragment = new JobListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("JobsRequestModelList", (ArrayList<? extends Parcelable>) jobList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        jobList = getArguments().getParcelableArrayList("JobsRequestModelList");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.job_list_fragment, container, false);

        mJobListRecyclerView = (RecyclerView) rootView.findViewById(R.id.job_list_recycler_view);
        initJobListAdapter();

        return rootView;
    }

    public void initJobListAdapter() {
        jobListAdapter = new JobListAdapter(JobListFragment.this.getActivity(), jobList);
        mJobListRecyclerView.setAdapter(jobListAdapter);
        mJobListRecyclerView.addItemDecoration(new DividerItemDecoration(this.getActivity(), DividerItemDecoration.VERTICAL));
        mJobListRecyclerView.setLayoutManager(new LinearLayoutManager(JobListFragment.this.getActivity()));

    }
}
