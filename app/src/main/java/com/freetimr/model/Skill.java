package com.freetimr.model;

import com.google.gson.annotations.SerializedName;

public class Skill {
    @SerializedName("SkillId")
    public int id;
    @SerializedName("SkillName")
    public String name;

    public boolean isSelected;

    @Override
    public String toString() {
        return name;
    }
}
