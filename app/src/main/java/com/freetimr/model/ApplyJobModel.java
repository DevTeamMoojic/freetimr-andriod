package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Admin on 24-Mar-17.
 */

public class ApplyJobModel implements Parcelable {

    public int JobApplicationId;
    public String JobId;
    public String CandidateId;
    public int JobApplicationStatusId;
    public Date StatusDate;

    protected ApplyJobModel(Parcel in) {
        JobApplicationId = in.readInt();
        JobId = in.readString();
        CandidateId = in.readString();
        JobApplicationStatusId = in.readInt();
        StatusDate = (Date) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(JobApplicationId);
        dest.writeString(JobId);
        dest.writeString(CandidateId);
        dest.writeInt(JobApplicationStatusId);
        dest.writeSerializable(StatusDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ApplyJobModel> CREATOR = new Creator<ApplyJobModel>() {
        @Override
        public ApplyJobModel createFromParcel(Parcel in) {
            return new ApplyJobModel(in);
        }

        @Override
        public ApplyJobModel[] newArray(int size) {
            return new ApplyJobModel[size];
        }
    };
}
