package com.freetimr.model;

import com.google.gson.annotations.SerializedName;

public class AvailabilityDay {
    @SerializedName("AvailabilityDayId")
    public int id;
    @SerializedName("AvailabilityDayName")
    public String name;
    public boolean isSelected;
}
