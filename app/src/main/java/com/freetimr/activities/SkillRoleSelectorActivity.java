package com.freetimr.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.freetimr.R;
import com.freetimr.adapter.SkillRoleSelectorAdapter;
import com.freetimr.model.CandidateProfileSkillRoleModel;
import com.freetimr.model.CandidateSkillRoleModel;
import com.freetimr.utils.FreeTimrUtils;

import java.util.ArrayList;

public class SkillRoleSelectorActivity extends AppCompatActivity {

    RecyclerView skillRoleRecyclerView;
    SkillRoleSelectorAdapter adapter;

    ImageView updateCandidateCancel, updateCandidateBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skill_role_selector);

        skillRoleRecyclerView = (RecyclerView) findViewById(R.id.skill_role_recycler_view);
        updateCandidateCancel = (ImageView) findViewById(R.id.update_candidate_cancel);
        updateCandidateBtn = (ImageView) findViewById(R.id.update_candidate_btn);

        ArrayList<CandidateSkillRoleModel> skillRoleModelList = FreeTimrUtils.getSkillRoleList(this);
        ArrayList<CandidateProfileSkillRoleModel> profileSkillRoleModelList = FreeTimrUtils.getCandidateProfile(this).get(0).CandidateSkillRole;

        for (int i = 0; i < skillRoleModelList.size(); i++) {
            for (int j = 0; j < profileSkillRoleModelList.size(); j++) {
                if (skillRoleModelList.get(i).SkillRoleId == profileSkillRoleModelList.get(j).SkillRole.SkillRoleId) {
                    skillRoleModelList.get(i).isSelected = true;
                }
            }
        }

        adapter = new SkillRoleSelectorAdapter(this, skillRoleModelList);
        skillRoleRecyclerView.setAdapter(adapter);
        skillRoleRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        updateCandidateCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        updateCandidateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
