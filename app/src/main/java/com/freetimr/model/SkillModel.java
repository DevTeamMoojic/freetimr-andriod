package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class SkillModel implements Parcelable {

    public int SkillId;
    public String SkillName;
    public boolean isSelected;

    protected SkillModel(Parcel in) {
        SkillId = in.readInt();
        SkillName = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(SkillId);
        dest.writeString(SkillName);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SkillModel> CREATOR = new Creator<SkillModel>() {
        @Override
        public SkillModel createFromParcel(Parcel in) {
            return new SkillModel(in);
        }

        @Override
        public SkillModel[] newArray(int size) {
            return new SkillModel[size];
        }
    };
}
