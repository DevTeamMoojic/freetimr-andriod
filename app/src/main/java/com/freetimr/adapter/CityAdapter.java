package com.freetimr.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.activities.CitySelectorActivity;
import com.freetimr.model.CandidatePreferredCityModel;
import com.freetimr.model.CityModel;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;
import com.freetimr.views.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 23-Dec-16.
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {

    Context context;
    LayoutInflater inflater;
    List<CityModel> list;
    SmoothProgressBar smoothProgressBar;
    CitySelectorActivity activity;

    public CityAdapter (Context context, List<CityModel> list) {
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public CityAdapter (Context context, List<CityModel> list, SmoothProgressBar smoothProgressBar, CitySelectorActivity activity) {
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.smoothProgressBar = smoothProgressBar;
        this.activity = activity;
    }

    @Override
    public CityViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.city_list_item, parent, false);
        return new CityViewHolder(view);
    }

    @Override
    public void onBindViewHolder (CityViewHolder holder, final int position) {

        final CityModel cityModel = list.get(position);

        holder.name.setText(cityModel.CityName);
        Picasso.with(context).load("http://137.59.54.53/FreeTimr/Images/City/" + cityModel.CityImage).into(holder.cityImage);
        holder.itemView.setTag(position);
//        if(candidatePreferredCityList.isEmpty()){
//            holder.cityImage.setColorFilter(Color.parseColor("#AAffffff"));
//            holder.name.setTextColor(Color.parseColor("#97a1aa"));
//            holder.name.setCustomTypeFace(context, 4);
//        }else{
//            for (int i = 0; i < candidatePreferredCityList.size(); i++) {
        if (cityModel.isSelected) {//|| cityModel.CityId == candidatePreferredCityList.get(i).City.CityId
            holder.cityImage.setColorFilter(Color.parseColor("#00ffffff"));
            holder.name.setTextColor(Color.parseColor("#1abc9c"));
            holder.name.setCustomTypeFace(context, 2);
        } else {
            holder.cityImage.setColorFilter(Color.parseColor("#AAffffff"));
            holder.name.setTextColor(Color.parseColor("#97a1aa"));
            holder.name.setCustomTypeFace(context, 4);
        }
//            }
//        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (cityModel.isSelected) {
//                    cityModel.isSelected = false;
                    deleteCity(cityModel.CandidatePreferredCityId, cityModel, position);
                } else {
                    String profileId = FreeTimrUtils.getCandidateProfile(context).get(0).CandidateProfileId;
                    addCity(profileId, cityModel);
                }
            }
        });
    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    public List<CityModel> getSelectedList () {
        List<CityModel> selectedList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isSelected) {
                selectedList.add(list.get(i));
            }
        }
        return selectedList;
    }

    public void deleteCity (final int id, final CityModel cityModel, final int position) {
        smoothProgressBar.setVisibility(View.VISIBLE);
        smoothProgressBar.progressiveStart();
        CityModel model = new CityModel();
        model.CityId = cityModel.CityId;

        Call<Integer> call = FreeTimrUtils.getRetrofit().deleteCandidateCity(FreeTimrURLs.HOST + FreeTimrURLs.DELETE_CITY + id);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse (Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    cityModel.isSelected = false;
                    list.get(position).isSelected = false;
                    removeCityFromSharePref(id);
                    activity.setAdapter();
//                    notifyDataSetChanged();
                } else {
                    FreeTimrUtils.showMessage((Activity) context, "Something went wrong. Please try again");
                }
                smoothProgressBar.progressiveStop();
            }

            @Override
            public void onFailure (Call<Integer> call, Throwable t) {
                cityModel.isSelected = false;
                list.get(position).isSelected = false;
                removeCityFromSharePref(id);
                activity.setAdapter();
                smoothProgressBar.progressiveStop();
            }
        });
    }


    public void addCity (String id, final CityModel cityModel) {
        smoothProgressBar.setVisibility(View.VISIBLE);
        smoothProgressBar.progressiveStart();
        CityModel model = new CityModel();
        model.CityId = cityModel.CityId;

        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(model));

        Call<CandidatePreferredCityModel> cityCall = FreeTimrUtils.getRetrofit().addCandidateCity(FreeTimrURLs.HOST + FreeTimrURLs.ADD_CITY + id, model);
        cityCall.enqueue(new Callback<CandidatePreferredCityModel>() {
            @Override
            public void onResponse (Call<CandidatePreferredCityModel> call, Response<CandidatePreferredCityModel> response) {
                if (response.code() == 200) {
                    cityModel.isSelected = true;
                    addCityToSharePref(response.body().CandidatePreferredCityId, cityModel);
                    activity.setAdapter();
//                    notifyDataSetChanged();
                } else if (response.code() == 400) {
                    FreeTimrUtils.showMessage(context, "City already exists.");
                } else {
                    FreeTimrUtils.showMessage(context, "Something went wrong.");
                }
//                FreeTimrUtils.log("" + response.body().CandidatePreferredCityId);
                smoothProgressBar.progressiveStop();
            }

            @Override
            public void onFailure (Call<CandidatePreferredCityModel> call, Throwable t) {
                FreeTimrUtils.log("" + t);
                smoothProgressBar.progressiveStop();
            }
        });
    }

    public class CityViewHolder extends RecyclerView.ViewHolder {

        CustomTextView name;
        ImageView cityImage;

        public CityViewHolder (View itemView) {
            super(itemView);
            name = (CustomTextView) itemView.findViewById(R.id.city_name);
            cityImage = (ImageView) itemView.findViewById(R.id.city_image);
        }
    }

    private void removeCityFromSharePref (int id) {
        ArrayList<CandidatePreferredCityModel> candidatePreferredCityList = FreeTimrUtils.getCandidateProfile(context).get(0).CandidatePreferredCity;
        for (Iterator<CandidatePreferredCityModel> iterator = candidatePreferredCityList.iterator(); iterator.hasNext(); ) {
            CandidatePreferredCityModel model = iterator.next();
            if (model.CandidatePreferredCityId == id) {
                // Remove the current element from the iterator and the list.
                iterator.remove();
            }
        }

        ArrayList<ProfileModel> profileModels = FreeTimrUtils.getCandidateProfile(context);
        profileModels.get(0).CandidatePreferredCity = candidatePreferredCityList;

        FreeTimrUtils.saveCandidateProfile(context, FreeTimrUtils.newGson().toJson(profileModels));
    }

    private void addCityToSharePref (int id, CityModel model) {
        CandidatePreferredCityModel candidatePreferredCityModel = new CandidatePreferredCityModel();
        candidatePreferredCityModel.CandidatePreferredCityId = id;
        candidatePreferredCityModel.City = model;

        ArrayList<CandidatePreferredCityModel> candidatePreferredCityList = FreeTimrUtils.getCandidateProfile(context).get(0).CandidatePreferredCity;
        candidatePreferredCityList.add(candidatePreferredCityModel);

        ArrayList<ProfileModel> profileModels = FreeTimrUtils.getCandidateProfile(context);
        profileModels.get(0).CandidatePreferredCity = candidatePreferredCityList;

        FreeTimrUtils.saveCandidateProfile(context, FreeTimrUtils.newGson().toJson(profileModels));
    }
}