package com.freetimr.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.freetimr.R;
import com.freetimr.adapter.WorkExperienceAdapter;
import com.freetimr.fragments.ProfileQualificationAndSkillsFragment;
import com.freetimr.model.CandidateEducations;
import com.freetimr.model.CandidateExperiences;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.FreeTimrUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkExperienceActivity extends AppCompatActivity {

    public static final int ADD_WORK_EXP = 159;
    RecyclerView workExpRV;
    ArrayList<ProfileModel>profileModel;
    WorkExperienceAdapter adapter;
    LinearLayout addWorkExp;
    List<CandidateExperiences> candidateExperiencesArrayList;
    ImageView updateCancel, updateBtn;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_experience);


        addWorkExp = (LinearLayout) findViewById(R.id.add_work_exp);
        updateCancel = (ImageView) findViewById(R.id.update_cancel);
        updateBtn = (ImageView) findViewById(R.id.update_btn);



        updateCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                finish();
            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                finish();
//                if (profileModel[0] != null)
//                    updateWorkExperience(profileModel[0]);
            }
        });

        addWorkExp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                Intent intent = new Intent(WorkExperienceActivity.this, AddWorkExperienceActivity.class);
                intent.putExtra("IS_SINGLE_ENTRY",true);
                startActivityForResult(intent, ADD_WORK_EXP);
            }
        });
    }

    @Override
    protected void onResume () {
        super.onResume();
        workExpRV = (RecyclerView) findViewById(R.id.work_exp_rv);
        profileModel = FreeTimrUtils.getCandidateProfile(this);

        setWorkExpAdapter(profileModel.get(0).CandidateExperience);
    }

    public void setWorkExpAdapter (ArrayList<CandidateExperiences> candidateExperiences) {
        adapter = new WorkExperienceAdapter(this, candidateExperiences, this);
        workExpRV.setAdapter(adapter);
        workExpRV.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initExperienceArrayList () {
        candidateExperiencesArrayList = new ArrayList<>();
        for (int i = 0; i < profileModel.get(0).CandidateExperience.size(); i++)
            candidateExperiencesArrayList.add(profileModel.get(0).CandidateExperience.get(i));
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ADD_WORK_EXP) {

                initExperienceArrayList();

//                CandidateExperiences candidateExperiences = data.getParcelableExtra("Work Experience");
//                candidateExperiencesArrayList.add(candidateExperiences);
//                profileModel[0].CandidateExperience = (CandidateExperiences[]) candidateExperiencesArrayList.toArray(new CandidateExperiences[candidateExperiencesArrayList.size()]);

                setWorkExpAdapter(FreeTimrUtils.getCandidateProfile(WorkExperienceActivity.this).get(0).CandidateExperience);
            }
        } else {
//            FreeTimrUtils.showMessage(WorkExperienceActivity.this, "Action Canceled");
        }
    }
}
