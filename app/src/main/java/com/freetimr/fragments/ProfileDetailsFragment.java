package com.freetimr.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.CandidateMastersModel;
import com.freetimr.model.Occupation;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.BottomSheetListOnItemClickListener;
import com.freetimr.utils.FreeTimrUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Admin on 28-Dec-16.
 */

public class ProfileDetailsFragment extends Fragment implements View.OnClickListener {

    Button mContinueBtn;
    RelativeLayout occupationValueContainer, expValueContainer;
    TextView occupationValue, expValue, dobDay, dobMonth, dobYear;
    EditText profileHeadline, location, pincode;

    int occupationValueId, expValueId;
    String locationStr, pincodeStr, profileHeadlineStr;
    Date dateValue;

    ProfileModel profileModel;

    Calendar myCalendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            setDateTime(year, monthOfYear + 1, dayOfMonth);

            dateValue = myCalendar.getTime();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_profile_details, container, false);

        occupationValueContainer = (RelativeLayout) rootView.findViewById(R.id.occupation_value_container);
        expValueContainer = (RelativeLayout) rootView.findViewById(R.id.exp_value_container);
        mContinueBtn = (Button) rootView.findViewById(R.id.continue_btn);
        occupationValue = (TextView) rootView.findViewById(R.id.occupation_value);
        expValue = (TextView) rootView.findViewById(R.id.exp_value);
        dobDay = (TextView) rootView.findViewById(R.id.dob_day);
        dobMonth = (TextView) rootView.findViewById(R.id.dob_month);
        dobYear = (TextView) rootView.findViewById(R.id.dob_year);
        profileHeadline = (EditText) rootView.findViewById(R.id.profile_headline);
        location = (EditText) rootView.findViewById(R.id.location_edit);
        pincode = (EditText) rootView.findViewById(R.id.pincode_edit);

        dobDay.setOnClickListener(this);
        dobMonth.setOnClickListener(this);
        dobYear.setOnClickListener(this);

        final CandidateMastersModel candidateMastersModel = FreeTimrUtils.getCandidateMasters(ProfileDetailsFragment.this.getActivity());

        mContinueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                locationStr = location.getText().toString();
                pincodeStr = pincode.getText().toString();
                profileHeadlineStr = profileHeadline.getText().toString();

                if (locationStr.isEmpty() || pincodeStr.isEmpty() || profileHeadlineStr.isEmpty() || occupationValueId == 0 || expValueId == 0 || dateValue == null) {
                    FreeTimrUtils.showMessage(ProfileDetailsFragment.this.getActivity(), "Please fill all fields");
                } else {
                    profileModel = new ProfileModel();

                    profileModel.CandidateId = FreeTimrUtils.getCandidateInfo(ProfileDetailsFragment.this.getActivity()).Id;
                    profileModel.DateOfBirth = dateValue;
                    profileModel.ProfileHeadline = profileHeadlineStr;
                    profileModel.Location = locationStr;
                    profileModel.Pincode = pincodeStr;
                    profileModel.OccupationId = occupationValueId;
                    profileModel.ExperienceYears = expValueId;
                    profileModel.ExperienceMonths = 2;

                    addFragment(ProfileDetailSecondPageFragment.getInstance(profileModel));
                }
            }
        });

        occupationValueContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String[] strings = new String[candidateMastersModel.occupations.size()];
                for (int i = 0; i < candidateMastersModel.occupations.size(); i++) {
                    strings[i] = candidateMastersModel.occupations.get(i).name;
                }

                FreeTimrUtils.showBottomSheetDialog(getFragmentManager(), "Occupation", strings, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected(int position) {
                        Occupation occupation = candidateMastersModel.occupations.get(position);
                        occupationValue.setText(occupation.name);
                        occupationValueId = occupation.id;
                    }
                });
            }
        });

        expValueContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] strings = new String[]{"6 Months", "1 year", "2 years", "3 years", "4 years", "5 years", "6 years", "7 years", "8 years", "9 years", "10 years", "10+ years"};

                FreeTimrUtils.showBottomSheetDialog(getFragmentManager(), "Total Experience", strings, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected(int position) {
                        expValue.setText(strings[position]);
                        expValueId = 5;
                    }
                });
            }
        });

        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dob_day:
            case R.id.dob_month:
            case R.id.dob_year:
                // DATE PICKER
                new DatePickerDialog(ProfileDetailsFragment.this.getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
        }
    }

    private void setDateTime(int year, int monthOfYear, int dayOfMonth) {
        dobDay.setText("" + dayOfMonth);
        dobMonth.setText("" + monthOfYear);
        dobYear.setText("" + year);
    }


    public void addFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        fragmentTransaction.replace(R.id.register_frame, fragment);
        fragmentTransaction.commit();
    }


}
