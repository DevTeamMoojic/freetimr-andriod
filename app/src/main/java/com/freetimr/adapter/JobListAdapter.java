package com.freetimr.adapter;

import android.content.Context;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.activities.JobDetailsActivity;
import com.freetimr.model.CompanyModel;
import com.freetimr.model.JobsRequestModel;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;

import java.util.List;

import retrofit2.Call;

/**
 * Created by Admin on 26-Dec-16.
 */

@RequiresApi(api = Build.VERSION_CODES.N)
public class JobListAdapter extends RecyclerView.Adapter<JobListAdapter.JobListViewHolder> {

    Context context;
    LayoutInflater inflater;
    List<JobsRequestModel> list;

    public JobListAdapter(Context context, List<JobsRequestModel> list) {
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public JobListAdapter.JobListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.job_list_item, parent, false);
        return new JobListAdapter.JobListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JobListAdapter.JobListViewHolder holder, int position) {

        JobsRequestModel model = list.get(position);

        holder.name.setText(model.JobTitle);
        holder.companyName.setText(model.CompanyName);

        // LOCATIONS
        if (model.JobLocations == null) {
            holder.locationContainer.setVisibility(View.GONE);
        } else {
            holder.locationContainer.setVisibility(View.VISIBLE);

            if (model.JobLocations.length != 0) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < model.JobLocations.length; i++) {
                    stringBuilder.append(model.JobLocations[i].LocationName + ",");
                }
                String jobLocations = stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1);

                holder.location.setText(jobLocations);
            }
        }

        // PAY
        if (model.PayPerDay == 0) {
            holder.payContainer.setVisibility(View.GONE);
        } else {
            holder.pay.setText(model.PayPerDay + " (per day)");
        }

        // DATE
        if (model.StartDate == null || model.EndDate == null) {
            holder.dateContainer.setVisibility(View.GONE);
        } else {
            String startDate = new SimpleDateFormat("dd-MM-yyyy").format(model.StartDate);
            String endDate = new SimpleDateFormat("dd-MM-yyyy").format(model.EndDate);

            holder.date.setText(startDate + " to " + endDate);
        }

        // TIME
        if (model.StartTime == null || model.EndTime == null) {
            holder.timeContainer.setVisibility(View.GONE);
        } else {
            holder.time.setText(model.StartTime + " to " + model.EndTime);
        }

        holder.itemView.setTag(model);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class JobListViewHolder extends RecyclerView.ViewHolder {

        TextView name, companyName, location, time, date, pay;
        LinearLayout locationContainer, timeContainer, dateContainer, payContainer;

        public JobListViewHolder(final View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.job_title);
            companyName = (TextView) itemView.findViewById(R.id.company_name);
            location = (TextView) itemView.findViewById(R.id.job_location);
            time = (TextView) itemView.findViewById(R.id.job_timing);
            date = (TextView) itemView.findViewById(R.id.job_date);
            pay = (TextView) itemView.findViewById(R.id.job_pay);
            locationContainer = (LinearLayout) itemView.findViewById(R.id.job_location_container);
            timeContainer = (LinearLayout) itemView.findViewById(R.id.job_timing_container);
            dateContainer = (LinearLayout) itemView.findViewById(R.id.job_date_container);
            payContainer = (LinearLayout) itemView.findViewById(R.id.job_pay_container);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, JobDetailsActivity.class);
                    intent.putExtra("JobRequestModel", (Parcelable) itemView.getTag());
                    context.startActivity(intent);
                }
            });
        }
    }
}
