package com.freetimr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.CandidateProfileSkillRoleModel;
import com.freetimr.model.CandidateSkillRoleModel;
import com.freetimr.model.CreateSkillRoleModel;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.POST;

/**
 * Created by varunbarve on 15/02/18.
 */

public class SkillRoleSelectorAdapter extends RecyclerView.Adapter<SkillRoleSelectorAdapter.SkillRoleSelectorViewHolder> {

    Context context;
    ArrayList<CandidateSkillRoleModel> list;
    LayoutInflater inflater;

    public SkillRoleSelectorAdapter(Context context, ArrayList<CandidateSkillRoleModel> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SkillRoleSelectorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.skill_role_selector_list_item, parent, false);
        return new SkillRoleSelectorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SkillRoleSelectorViewHolder holder, final int position) {
        final CandidateSkillRoleModel model = list.get(position);
        holder.name.setText(model.SkillRoleName);

        if (model.isSelected) {
            holder.skillRoleCheck.setChecked(true);
        } else {
            holder.skillRoleCheck.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class SkillRoleSelectorViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        CheckBox skillRoleCheck;

        public SkillRoleSelectorViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.skill_roll_list_item_label);
            skillRoleCheck = (CheckBox) itemView.findViewById(R.id.skill_roll_list_item_checkbox);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int adapterPosition = getAdapterPosition();
                    if (!list.get(adapterPosition).isSelected) {
                        skillRoleCheck.setChecked(true);
                        list.get(adapterPosition).isSelected = true;
                        createSkillRole(list.get(adapterPosition));
                    } else {
                        skillRoleCheck.setChecked(false);
                        list.get(adapterPosition).isSelected = false;
                    }
                }
            });
        }
    }

    private void createSkillRole(final CandidateSkillRoleModel candidateSkillRoleModel) {
        CreateSkillRoleModel model = new CreateSkillRoleModel();
        model.SkillRoleId = candidateSkillRoleModel.SkillRoleId;

        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(model));

        Call<String> call = FreeTimrUtils.getRetrofit().createSkillRole(FreeTimrURLs.HOST + FreeTimrURLs.CREATE_SKILLS_ROLE + FreeTimrUtils.getCandidateProfile(context).get(0).CandidateProfileId, model);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    CandidateProfileSkillRoleModel candidateProfileSkillRoleModel = new CandidateProfileSkillRoleModel(0, candidateSkillRoleModel);

                    ArrayList<CandidateProfileSkillRoleModel> profileSkillRoleModelList = FreeTimrUtils.getCandidateProfile(context).get(0).CandidateSkillRole;

                    profileSkillRoleModelList.add(candidateProfileSkillRoleModel);

                    ArrayList<ProfileModel> profileModels = FreeTimrUtils.getCandidateProfile(context);

                    profileModels.get(0).CandidateSkillRole = profileSkillRoleModelList;

                    FreeTimrUtils.saveCandidateProfile(context, FreeTimrUtils.newGson().toJson(profileModels));
                } else {
                    FreeTimrUtils.showMessage(context, "Something went wrong. Please try again later.");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
            }
        });
    }
}
