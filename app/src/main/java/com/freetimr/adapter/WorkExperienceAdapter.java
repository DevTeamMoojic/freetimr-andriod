package com.freetimr.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.activities.AddWorkExperienceActivity;
import com.freetimr.activities.WorkExperienceActivity;
import com.freetimr.model.CandidateExperiences;
import com.freetimr.model.CandidatePreferredCityModel;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;

import java.io.EOFException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 20-Feb-17.
 */

public class WorkExperienceAdapter extends RecyclerView.Adapter<WorkExperienceAdapter.WorkExperienceViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<CandidateExperiences> candidateExperiences;
    WorkExperienceActivity workExperienceActivity;

    public WorkExperienceAdapter(Context context, ArrayList<CandidateExperiences> candidateExperiences,WorkExperienceActivity workExperienceActivity) {
        this.candidateExperiences = candidateExperiences;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.workExperienceActivity = workExperienceActivity;
    }

    public WorkExperienceAdapter(Context context, ArrayList<CandidateExperiences> candidateExperiences) {
        this.candidateExperiences = candidateExperiences;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public WorkExperienceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.work_exp_list_item, parent, false);
        return new WorkExperienceAdapter.WorkExperienceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WorkExperienceViewHolder holder, final int position) {
        final CandidateExperiences model = candidateExperiences.get(position);

        holder.role.setText(model.Designation);
        holder.companyName.setText(model.EmployerName);
        if (model.IsCurrentEmployer) {
            holder.expValue.setText("From: " + model.DurationFromMonth + " " + model.DurationFromYear + " - Present");
        } else {
            holder.expValue.setText("From: " + model.DurationFromMonth + " " + model.DurationFromYear + " - " + model.DurationToMonth + " " + model.DurationToYear);
        }
        holder.location.setText(model.Location);
        holder.description.setText(model.JobProfile);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                deleteWorkExperience(model.CandidateExperienceId);
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(context, AddWorkExperienceActivity.class);
                intent.putExtra("IS_SINGLE_ENTRY",true);
                intent.putExtra("IS_UPDATE",true);
                intent.putExtra("CANDIDATE_EXP",model);
                workExperienceActivity.startActivityForResult(intent, WorkExperienceActivity.ADD_WORK_EXP);
            }
        });
    }

    @Override
    public int getItemCount() {
        return candidateExperiences.size();
    }

    class WorkExperienceViewHolder extends RecyclerView.ViewHolder {

        TextView role, companyName, expValue, location, description;
        ImageView delete, edit;

        public WorkExperienceViewHolder(View itemView) {
            super(itemView);

            role = (TextView) itemView.findViewById(R.id.work_exp_role_text_view);
            companyName = (TextView) itemView.findViewById(R.id.work_exp_company_name_text_view);
            expValue = (TextView) itemView.findViewById(R.id.work_exp_value_text_view);
            location = (TextView) itemView.findViewById(R.id.work_exp_location_text_view);
            description = (TextView) itemView.findViewById(R.id.work_exp_description_text_view);
            delete = (ImageView) itemView.findViewById(R.id.work_exp_delete);
            edit = (ImageView) itemView.findViewById(R.id.work_exp_edit);
        }
    }

    private void deleteWorkExperience(final int candidateExperienceId){
        Call<Integer> call = FreeTimrUtils.getRetrofit().deleteCandidateWorkExperience(FreeTimrURLs.HOST+FreeTimrURLs.DELETE_WORK_EXP+candidateExperienceId);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse (Call<Integer> call, Response<Integer> response) {
                if(response.code()==200){
                    deleteExpFromLocalStorage(candidateExperienceId);
                }else{
                    FreeTimrUtils.showMessage(context, "Something went wrong. Please try again later.");
                }
            }

            @Override
            public void onFailure (Call<Integer> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
                if (t instanceof EOFException){
                    deleteExpFromLocalStorage(candidateExperienceId);
                }
            }
        });
    }

    private void deleteExpFromLocalStorage(int candidateExpId){
        ArrayList<CandidateExperiences> candidateExperiencesArrayList = new ArrayList<>(FreeTimrUtils.getCandidateProfile(context).get(0).CandidateExperience);
        for (Iterator<CandidateExperiences> iterator = candidateExperiencesArrayList.iterator(); iterator.hasNext(); ) {
            CandidateExperiences model = iterator.next();
            if (model.CandidateExperienceId == candidateExpId) {
                iterator.remove();
            }
        }

        ArrayList<ProfileModel> profileModels = FreeTimrUtils.getCandidateProfile(context);
        profileModels.get(0).CandidateExperience = candidateExperiencesArrayList;

        FreeTimrUtils.saveCandidateProfile(context, FreeTimrUtils.newGson().toJson(profileModels));

        workExperienceActivity.setWorkExpAdapter(FreeTimrUtils.getCandidateProfile(context).get(0).CandidateExperience);
    }
}
