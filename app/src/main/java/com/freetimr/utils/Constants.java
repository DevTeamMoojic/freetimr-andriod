package com.freetimr.utils;

/**
 * Created by Admin on 09-Dec-16.
 */

public class Constants {

    public static String LOG_TAG = "FREETIMR";

    public static String KEY_CANDIDATE_INFO = "FreeTimrCandidateInfo";
    public static String KEY_CANDIDATE_PROFILE= "FreeTimrCandidateProfile";

    public static String BLACK_FONT = "Montserrat-Black.otf";
    public static String BOLD_FONT = "Montserrat-Bold.ttf";
    public static String EXTRA_BOLD_FONT = "Montserrat-ExtraBold.otf";
    public static String LIGHT_FONT = "Montserrat-Light.otf";
    public static String REGULAR_FONT = "Montserrat-Regular.ttf";
    public static String SEMI_BOLD_FONT = "Montserrat-SemiBold.otf";
    public static String ULTRA_LIGHT_FONT = "Montserrat-UltraLight.otf";

    public static int RESPONSE_CREATED = 201;
    public static int RESPONSE_SUCCESS = 200;
    public static int RESPONSE_BAD_REQUEST = 400;

    public static String KEY_CANDIDATE_MASTERS = "CandidateMasters";
    public static String KEY_COMPANY_MODEL = "CompanyModel";
    public static String KEY_JOB_MASTER = "JobMaster";

    public static String IS_RPOFILE_COMPLETE = "IsProfileComplete";

    public static String USERTYPE_CANDIDATE = "UT_Candidate";
    public static String USERTYPE_COMPANY = "UT_Company";
}
