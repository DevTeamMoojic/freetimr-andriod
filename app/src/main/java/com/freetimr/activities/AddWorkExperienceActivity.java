package com.freetimr.activities;

import android.content.Intent;
import android.icu.text.DateFormatSymbols;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.model.CandidateExperiences;
import com.freetimr.model.ProfileModel;
import com.freetimr.utils.BottomSheetListOnItemClickListener;
import com.freetimr.utils.FreeTimrURLs;
import com.freetimr.utils.FreeTimrUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddWorkExperienceActivity extends AppCompatActivity {

    Toolbar mToolbar;
    CandidateExperiences candidateExperiences;
    EditText mRole, mCompanyName, mLocation, mDescription;
    TextView mStartMonth, mStartYear, mEndMonth, mEndYear;
    CheckBox mCurrentlyWorking;
    Button mWorkExpProceedBtn;

    String roleStr, companyNameStr, locationStr, descriptionStr, startMonthStr, endMonthStr;
    int startYearValue, endYearValue;
    boolean isCurrentEmployer;
    boolean isSingleEntry, isUpdate;

    String[] monthStrings = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    String[] yearString = new String[]{"2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_work_experience);

        mToolbar = (Toolbar) findViewById(R.id.add_work_exp_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        isSingleEntry = getIntent().getBooleanExtra("IS_SINGLE_ENTRY", false);
        isUpdate = getIntent().getBooleanExtra("IS_UPDATE", false);

        if (isUpdate) {
            candidateExperiences = getIntent().getParcelableExtra("CANDIDATE_EXP");
        } else {
            candidateExperiences = new CandidateExperiences();
        }

        mRole = (EditText) findViewById(R.id.work_exp_role);
        mCompanyName = (EditText) findViewById(R.id.work_exp_company_name);
        mLocation = (EditText) findViewById(R.id.work_exp_location);
        mDescription = (EditText) findViewById(R.id.work_exp_description);
        mStartMonth = (TextView) findViewById(R.id.work_exp_start_month);
        mStartYear = (TextView) findViewById(R.id.work_exp_start_year);
        mEndMonth = (TextView) findViewById(R.id.work_exp_end_month);
        mEndYear = (TextView) findViewById(R.id.work_exp_end_year);
        mCurrentlyWorking = (CheckBox) findViewById(R.id.work_exp_currently_working);
        mWorkExpProceedBtn = (Button) findViewById(R.id.work_exp_proceed_btn);

        if (isUpdate) {
            setData(candidateExperiences);
        }

        mCurrentlyWorking.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isCurrentEmployer = b;
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                endYearValue = year;
                endMonthStr = FreeTimrUtils.getMonthCode(getMonthForInt(month));
            }
        });

        mStartMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FreeTimrUtils.showBottomSheetDialog(getSupportFragmentManager(), "From Month", monthStrings, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected(int position) {
                        startMonthStr = FreeTimrUtils.getMonthCode(monthStrings[position]);
                        mStartMonth.setText(monthStrings[position]);
                    }
                });
            }
        });

        mStartYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FreeTimrUtils.showBottomSheetDialog(getSupportFragmentManager(), "From Year", yearString, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected(int position) {
                        startYearValue = Integer.parseInt(yearString[position]);
                        mStartYear.setText(yearString[position]);
                    }
                });
            }
        });

        mEndMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FreeTimrUtils.showBottomSheetDialog(getSupportFragmentManager(), "To Month", monthStrings, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected(int position) {
                        endMonthStr = FreeTimrUtils.getMonthCode(monthStrings[position]);
                        mEndMonth.setText(monthStrings[position]);
                    }
                });
            }
        });

        mEndYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FreeTimrUtils.showBottomSheetDialog(getSupportFragmentManager(), "To Year", yearString, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected(int position) {
                        endYearValue = Integer.parseInt(yearString[position]);
                        mEndYear.setText(yearString[position]);
                    }
                });
            }
        });

        mWorkExpProceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roleStr = mRole.getText().toString();
                companyNameStr = mCompanyName.getText().toString();
                locationStr = mLocation.getText().toString();
                descriptionStr = mDescription.getText().toString();

                if (companyNameStr.isEmpty() || startMonthStr == null || startYearValue == 0) {// descriptionStr.isEmpty() || roleStr.isEmpty() || locationStr.isEmpty() ||
                    FreeTimrUtils.showMessage(AddWorkExperienceActivity.this, "Please fill all the fields");
                } else {
                    if (!isCurrentEmployer) {
                        if (endMonthStr == null || endYearValue == 0) {
                            FreeTimrUtils.showMessage(AddWorkExperienceActivity.this, "Are you currently working here ?");
                            return;
                        }
                    }

                    candidateExperiences.EmployerName = companyNameStr;
                    candidateExperiences.IsCurrentEmployer = isCurrentEmployer;
                    candidateExperiences.DurationFromMonth = startMonthStr;
                    candidateExperiences.DurationFromYear = startYearValue;
                    candidateExperiences.DurationToMonth = endMonthStr;
                    candidateExperiences.DurationToYear = endYearValue;
                    candidateExperiences.Designation = roleStr;
                    candidateExperiences.JobProfile = descriptionStr;
                    candidateExperiences.Location = locationStr;

                    if (isSingleEntry) {
                        if (isUpdate) {
                            updateWorkExperience(candidateExperiences);
                        } else {
                            addWorkExperience(candidateExperiences);
                        }
                    } else {
                        Intent workExpIntent = new Intent();
                        workExpIntent.putExtra("Work Experience", candidateExperiences);
                        setResult(RESULT_OK, workExpIntent);
                        finish();
                    }
                }
            }
        });
    }

    private void setData(CandidateExperiences candidateExperiences) {
        mRole.setText(candidateExperiences.Designation);
        mCompanyName.setText(candidateExperiences.EmployerName);
        mLocation.setText(candidateExperiences.Location);
        mDescription.setText(candidateExperiences.JobProfile);

        if (candidateExperiences.IsCurrentEmployer) {
            mStartMonth.setText(candidateExperiences.DurationFromMonth);
            startMonthStr = candidateExperiences.DurationFromMonth;

            mStartYear.setText("" + candidateExperiences.DurationFromYear);
            startYearValue = candidateExperiences.DurationFromYear;


            mCurrentlyWorking.setChecked(true);
        } else {
            mStartMonth.setText(candidateExperiences.DurationFromMonth);
            startMonthStr = candidateExperiences.DurationFromMonth;

            mStartYear.setText("" + candidateExperiences.DurationFromYear);
            startYearValue = candidateExperiences.DurationFromYear;

            mEndMonth.setText(candidateExperiences.DurationToMonth);
            endMonthStr = candidateExperiences.DurationToMonth;

            mEndYear.setText("" + candidateExperiences.DurationToYear);
            endYearValue = candidateExperiences.DurationToYear;
        }

        isCurrentEmployer = candidateExperiences.IsCurrentEmployer;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent workExpIntent = new Intent();
        setResult(RESULT_CANCELED, workExpIntent);
        finish();
    }

    private void addWorkExperience(final CandidateExperiences candidateExperiences) {
        FreeTimrUtils.showProgressDialog(this);
        String candidateProfileId = FreeTimrUtils.getCandidateProfile(AddWorkExperienceActivity.this).get(0).CandidateProfileId;

        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(candidateExperiences));

        Call<CandidateExperiences> call = FreeTimrUtils.getRetrofit().addCandidateWorkExperience(FreeTimrURLs.HOST + FreeTimrURLs.ADD_WORK_EXP + candidateProfileId, candidateExperiences);
        call.enqueue(new Callback<CandidateExperiences>() {
            @Override
            public void onResponse(Call<CandidateExperiences> call, Response<CandidateExperiences> response) {
                if (response.code() == 200) {
                    ArrayList<ProfileModel> profileModel = FreeTimrUtils.getCandidateProfile(AddWorkExperienceActivity.this);
                    ArrayList<CandidateExperiences> candidateExperiencesArrayList = profileModel.get(0).CandidateExperience;
                    candidateExperiencesArrayList.add(response.body());

                    profileModel.get(0).CandidateExperience = candidateExperiencesArrayList;

                    FreeTimrUtils.saveCandidateProfile(AddWorkExperienceActivity.this, FreeTimrUtils.newGson().toJson(profileModel));

                    Intent workExpIntent = new Intent();
                    workExpIntent.putExtra("Work Experience", response.body());
                    setResult(RESULT_OK, workExpIntent);
                    finish();
                } else {
                    FreeTimrUtils.showMessage(AddWorkExperienceActivity.this, "Something went wrong. Please try again.");
                }
                FreeTimrUtils.cancelCurrentDialog(AddWorkExperienceActivity.this);
            }

            @Override
            public void onFailure(Call<CandidateExperiences> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
                FreeTimrUtils.cancelCurrentDialog(AddWorkExperienceActivity.this);
            }
        });
    }

    private void updateWorkExperience(final CandidateExperiences candidateExperiences) {
        FreeTimrUtils.showProgressDialog(this);
        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(candidateExperiences));
        Call<CandidateExperiences> call = FreeTimrUtils.getRetrofit().updateCandidateWorkExperience(candidateExperiences);
        call.enqueue(new Callback<CandidateExperiences>() {
            @Override
            public void onResponse(Call<CandidateExperiences> call, Response<CandidateExperiences> response) {
                if (response.code() == 200) {
                    ArrayList<CandidateExperiences> candidateExperiencesArrayList = FreeTimrUtils.getCandidateProfile(AddWorkExperienceActivity.this).get(0).CandidateExperience;
                    for (Iterator<CandidateExperiences> iterator = candidateExperiencesArrayList.iterator(); iterator.hasNext(); ) {
                        CandidateExperiences model = iterator.next();
                        if (model.CandidateExperienceId == response.body().CandidateExperienceId) {
                            iterator.remove();
                        }
                    }

                    candidateExperiencesArrayList.add(response.body());

                    ArrayList<ProfileModel> profileModels = FreeTimrUtils.getCandidateProfile(AddWorkExperienceActivity.this);
                    profileModels.get(0).CandidateExperience = candidateExperiencesArrayList;

                    FreeTimrUtils.saveCandidateProfile(AddWorkExperienceActivity.this, FreeTimrUtils.newGson().toJson(profileModels));

                    setData(response.body());
                } else {
                    FreeTimrUtils.showMessage(AddWorkExperienceActivity.this, "Something went wrong. Please try again.");
                }

                FreeTimrUtils.cancelCurrentDialog(AddWorkExperienceActivity.this);
            }

            @Override
            public void onFailure(Call<CandidateExperiences> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
                FreeTimrUtils.cancelCurrentDialog(AddWorkExperienceActivity.this);
            }
        });
    }
}
