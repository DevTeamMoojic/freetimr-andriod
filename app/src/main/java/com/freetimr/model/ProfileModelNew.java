package com.freetimr.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by varunbarve on 02/09/17.
 */

public class ProfileModelNew implements Parcelable {

    public String CandidateProfileId;
    public String CandidateId;
    public Date DateOfBirth;
    public String ProfileHeadline;
    public String Location;
    public String Pincode;
    public int OccupationId;
    public int ExperienceYears;
    public int ExperienceMonths;
    public int WorkLocationId;
    public ArrayList<CandidateAvailabilities> CandidateAvailability;
    public ArrayList<CandidateEducations> CandidateEducation;
    public ArrayList<CandidateExperiences> CandidateExperience;
    public ArrayList<CandidateSkills> CandidateSkill;
    public String CreatedDate;
    public String UpdatedDate;
    public ArrayList<CandidateSkillRoleModel> CandidateSkillRole;
    public ArrayList<CandidatePreferredCityModel> CandidatePreferredCity;

    public ProfileModelNew() {
    }

    protected ProfileModelNew (Parcel in) {
        CandidateProfileId = in.readString();
        CandidateId = in.readString();
        ProfileHeadline = in.readString();
        Location = in.readString();
        Pincode = in.readString();
        OccupationId = in.readInt();
        ExperienceYears = in.readInt();
        ExperienceMonths = in.readInt();
        WorkLocationId = in.readInt();
        CandidateAvailability = in.createTypedArrayList(CandidateAvailabilities.CREATOR);
        CandidateEducation = in.createTypedArrayList(CandidateEducations.CREATOR);
        CandidateExperience = in.createTypedArrayList(CandidateExperiences.CREATOR);
        CandidateSkill = in.createTypedArrayList(CandidateSkills.CREATOR);
        CreatedDate = in.readString();
        UpdatedDate = in.readString();
        CandidateSkillRole = in.createTypedArrayList(CandidateSkillRoleModel.CREATOR);
        CandidatePreferredCity = in.createTypedArrayList(CandidatePreferredCityModel.CREATOR);
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeString(CandidateProfileId);
        dest.writeString(CandidateId);
        dest.writeString(ProfileHeadline);
        dest.writeString(Location);
        dest.writeString(Pincode);
        dest.writeInt(OccupationId);
        dest.writeInt(ExperienceYears);
        dest.writeInt(ExperienceMonths);
        dest.writeInt(WorkLocationId);
        dest.writeTypedList(CandidateAvailability);
        dest.writeTypedList(CandidateEducation);
        dest.writeTypedList(CandidateExperience);
        dest.writeTypedList(CandidateSkill);
        dest.writeString(CreatedDate);
        dest.writeString(UpdatedDate);
        dest.writeTypedList(CandidateSkillRole);
        dest.writeTypedList(CandidatePreferredCity);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<ProfileModel> CREATOR = new Creator<ProfileModel>() {
        @Override
        public ProfileModel createFromParcel (Parcel in) {
            return new ProfileModel(in);
        }

        @Override
        public ProfileModel[] newArray (int size) {
            return new ProfileModel[size];
        }
    };

}
