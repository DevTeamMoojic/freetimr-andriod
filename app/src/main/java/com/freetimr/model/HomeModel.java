package com.freetimr.model;

/**
 * Created by Admin on 12-Dec-16.
 */

public class HomeModel {

    String id;
    String name;
    int background;
    int logo;

    public HomeModel(String id, String name, int logo, int background) {
        this.id = id;
        this.name = name;
        this.background = background;
        this.logo = logo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }
}
