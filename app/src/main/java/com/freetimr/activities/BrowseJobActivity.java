package com.freetimr.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.adapter.CityAutoCompleteAdapter;
import com.freetimr.adapter.JobAvailabilityAdapter;
import com.freetimr.adapter.JobCategoryAdapter;
import com.freetimr.adapter.WorkLocationAdapter;
import com.freetimr.model.AvailabilityTimeModel;
import com.freetimr.model.CityModel;
import com.freetimr.model.JobCategoryModel;
import com.freetimr.model.SearchRequestModel;
import com.freetimr.model.WorkLocationModel;
import com.freetimr.utils.FreeTimrUtils;

import java.util.ArrayList;

public class BrowseJobActivity extends AppCompatActivity {

    Toolbar mToolbar;
    AutoCompleteTextView cityAutoComplete;
    RecyclerView jobCategoryRecyclerView, workLocationRecyclerView, availableTimeRecyclerView;
    Button proceed;
    TextView title;
    EditText keywordEdit;

    JobCategoryAdapter jobCategoryAdapter;
    ArrayList<JobCategoryModel> jobCategoryModelList;

    WorkLocationAdapter workLocationAdapter;
    ArrayList<WorkLocationModel> workLocationModelList;

    JobAvailabilityAdapter jobAvailabilityAdapter;
    ArrayList<AvailabilityTimeModel> availabilityTimeModelList;

    CityAutoCompleteAdapter cityAutoCompleteAdapter;
    ArrayList<CityModel> cityModelList;

    SearchRequestModel searchRequestModel;

    int cityId, availabilityTimeId, workLocationId, jobCategoryId;
    String candidateId;
    boolean isFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_job);
        mToolbar = (Toolbar) findViewById(R.id.browse_job_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        isFilter = getIntent().getBooleanExtra("IsFilter", false);

        cityAutoComplete = (AutoCompleteTextView) findViewById(R.id.city_complete_textview);

        jobCategoryRecyclerView = (RecyclerView) findViewById(R.id.job_category_recycler_view);
        workLocationRecyclerView = (RecyclerView) findViewById(R.id.work_location_recycler_view);
        availableTimeRecyclerView = (RecyclerView) findViewById(R.id.available_time_recycler_view);
        proceed = (Button) findViewById(R.id.browse_job_proceed_btn);
        title = (TextView) findViewById(R.id.browse_filter_title);
        keywordEdit = (EditText) findViewById(R.id.keyword_edit);

        if (isFilter) {
            title.setText("Filter");
        } else {
            title.setText("Browse Job");
        }

        jobCategoryModelList = FreeTimrUtils.getJobCategoryList(BrowseJobActivity.this);
        jobCategoryAdapter = new JobCategoryAdapter(BrowseJobActivity.this, jobCategoryModelList);
        jobCategoryRecyclerView.setAdapter(jobCategoryAdapter);
        jobCategoryRecyclerView.setLayoutManager(new GridLayoutManager(BrowseJobActivity.this, 4, LinearLayoutManager.VERTICAL, false));

        workLocationModelList = FreeTimrUtils.getWorkLocationList(BrowseJobActivity.this);
        workLocationAdapter = new WorkLocationAdapter(this, workLocationModelList);
        workLocationRecyclerView.setAdapter(workLocationAdapter);
        workLocationRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        availabilityTimeModelList = FreeTimrUtils.getAvailableTimeList(BrowseJobActivity.this);
        jobAvailabilityAdapter = new JobAvailabilityAdapter(BrowseJobActivity.this, availabilityTimeModelList);
        availableTimeRecyclerView.setAdapter(jobAvailabilityAdapter);
        availableTimeRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        cityModelList = FreeTimrUtils.getCityList(BrowseJobActivity.this);
        cityAutoCompleteAdapter = new CityAutoCompleteAdapter(BrowseJobActivity.this, cityModelList);
        cityAutoComplete.setAdapter(cityAutoCompleteAdapter);

        cityAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CityModel cityModel = (CityModel) parent.getAdapter().getItem(position);
                cityId = cityModel.CityId;
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                availabilityTimeId = jobAvailabilityAdapter.getAvailabilityId();
                workLocationId = workLocationAdapter.getWorkLocationId();
                jobCategoryId = jobCategoryAdapter.getCategoryId();
                candidateId = FreeTimrUtils.getCandidateInfo(BrowseJobActivity.this) != null ? FreeTimrUtils.getCandidateInfo(BrowseJobActivity.this).Id : "";

//                if (availabilityTimeId == -1 || workLocationId == -1 || jobCategoryId == -1 || cityId == 0 ) {
//                    FreeTimrUtils.showMessage(BrowseJobActivity.this, "All the fields are mandatory.");
//                } else {
                searchRequestModel = new SearchRequestModel();
                searchRequestModel.AvailabilityTimeId = availabilityTimeId == -1 ? "" : "" + availabilityTimeId;
                searchRequestModel.WorkLocationId = workLocationId == -1 ? "" : "" + workLocationId;
                searchRequestModel.JobCategoryId = jobCategoryId == -1 ? "" : "" + jobCategoryId;
                searchRequestModel.CandidateId = candidateId;
                searchRequestModel.CityId = cityId == 0 ? "" : "" + cityId;
                searchRequestModel.KeywordTosearch = keywordEdit.getText().toString();

                FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(searchRequestModel));

                if (isFilter) {
                    searchRequestModel.isFilteredApplied = true;
                    Intent intent = new Intent();
                    intent.putExtra("SearchRequestModel", searchRequestModel);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Intent intent = new Intent(BrowseJobActivity.this, JobsListActivity.class);
                    intent.putExtra("SearchRequestModel", searchRequestModel);
                    startActivity(intent);
                }
//                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
