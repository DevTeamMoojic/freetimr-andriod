package com.freetimr.application;

import android.app.Application;

import com.sendbird.android.SendBird;

/**
 * Created by Admin on 29-Mar-17.
 */

public class FreeTimrApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SendBird.init("BB6AF194-7598-4F21-B040-3AEBC16A40B3", this);
    }
}
