package com.freetimr.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.freetimr.R;
import com.freetimr.adapter.CityAdapter;
import com.freetimr.model.CityModel;

import java.util.ArrayList;

public class PreferredCitiesActivity extends AppCompatActivity {

    RecyclerView citiesRecyclerView;
    CityAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferred_cities);

        citiesRecyclerView = (RecyclerView) findViewById(R.id.cities_rv);

//        initCityAdapter();
    }

//    public void initCityAdapter() {
//        ArrayList<CityModel> cityModels = new ArrayList<>();
//        cityModels.add(new CityModel(R.drawable.city_banglore, "Banglore"));
//        cityModels.add(new CityModel(R.drawable.city_chennai, "Chennai"));
//        cityModels.add(new CityModel(R.drawable.city_coimbatore, "Coimbatore"));
//        cityModels.add(new CityModel(R.drawable.city_delhi, "Delhi"));
//        cityModels.add(new CityModel(R.drawable.city_hyderabad, "Hyderabad"));
//        cityModels.add(new CityModel(R.drawable.city_madurai, "Madurai"));
//        cityModels.add(new CityModel(R.drawable.city_mumbai, "Mumbai"));
//        cityModels.add(new CityModel(R.drawable.city_pune, "Pune"));
//
//        adapter = new CityAdapter(PreferredCitiesActivity.this, cityModels);
//        citiesRecyclerView.setAdapter(adapter);
//        citiesRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
//    }
}
