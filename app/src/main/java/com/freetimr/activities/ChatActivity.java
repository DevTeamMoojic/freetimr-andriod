package com.freetimr.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.adapter.ChatAdapter;
import com.freetimr.model.CompanyModel;
import com.freetimr.model.JobsRequestModel;
import com.freetimr.utils.FreeTimrUtils;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.MessageListQuery;
import com.sendbird.android.PreviousMessageListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.sendbird.android.UserMessage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.freetimr.utils.Constants.USERTYPE_CANDIDATE;

public class ChatActivity extends AppCompatActivity {

    Toolbar mToolbar;
    RecyclerView chatRecyclerView;
    ImageView send;
    EditText messageEdit;
    TextView toolbarTitle;

    JobsRequestModel jobsRequestModel;
    CompanyModel companyModel;
    GroupChannel channel;

    List<UserMessage> messagesList;
    ChatAdapter chatAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        jobsRequestModel = getIntent().getParcelableExtra("JobRequestModel");
        companyModel = getIntent().getParcelableExtra("CompanyModel");

        mToolbar = (Toolbar) findViewById(R.id.messages_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        chatRecyclerView = (RecyclerView) findViewById(R.id.chat_rv);
        send = (ImageView) findViewById(R.id.send);
        messageEdit = (EditText) findViewById(R.id.message_edit);

        toolbarTitle.setText(companyModel.CompanyName);

        createSendBirdConnection();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = messageEdit.getText().toString();
                if (!message.isEmpty()) {
                    sendMessage(message);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void createSendBirdConnection() {
        FreeTimrUtils.showProgressDialog(this);

        SendBird.connect(FreeTimrUtils.getCandidateInfo(this).getId(), new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                if (e != null) {
                    // Error.
                    return;
                } else {
                    FreeTimrUtils.log("" + user.getProfileUrl());
                    createChat();
                }
            }
        });
    }

    private void createChat() {
        List<String> userId = new ArrayList<>();
        userId.add(FreeTimrUtils.getCandidateInfo(this).getId());
        userId.add(jobsRequestModel.CompanyId);

        GroupChannel.createChannelWithUserIds(userId, true, jobsRequestModel.CompanyName, "", "", new GroupChannel.GroupChannelCreateHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {
                    // Error.
                    return;
                }

                getChannelInstance(groupChannel.getUrl());
                FreeTimrUtils.log("" + groupChannel);

            }
        });
    }

    private void getChannelInstance(String channelUrl) {
        GroupChannel.getChannel(channelUrl, new GroupChannel.GroupChannelGetHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {
                    // Error!
                    return;
                }

                channel = groupChannel;
                loadPreviousMessages();
                FreeTimrUtils.log("" + groupChannel);
            }
        });
    }

    private void loadMessage() {
        MessageListQuery messageListQuery = channel.createMessageListQuery();
        messageListQuery.load(new Date().getTime(), 0, 0, true, new MessageListQuery.MessageListQueryResult() {
            @Override
            public void onResult(List<BaseMessage> list, SendBirdException e) {

            }
        });
    }

    private void loadPreviousMessages() {
        PreviousMessageListQuery prevMessageListQuery = channel.createPreviousMessageListQuery();
        prevMessageListQuery.load(30, false, new PreviousMessageListQuery.MessageListQueryResult() {
            @Override
            public void onResult(List<BaseMessage> messages, SendBirdException e) {
                if (e != null) {
                    // Error.
                    return;
                }
                FreeTimrUtils.log("" + messages);
//                setChatAdapter(messages);
                FreeTimrUtils.cancelCurrentDialog(ChatActivity.this);
            }
        });
    }

    private void sendMessage(String message) {
        channel.sendUserMessage(message, "", USERTYPE_CANDIDATE, new BaseChannel.SendUserMessageHandler() {
            @Override
            public void onSent(UserMessage userMessage, SendBirdException e) {
                if (e != null) {
                    // Error.
                    return;
                }

                FreeTimrUtils.log("" + userMessage);
            }
        });
    }

    private void setChatAdapter(List<UserMessage> list) {
        chatAdapter = new ChatAdapter(this, list);
        chatRecyclerView.setAdapter(chatAdapter);
        chatRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
