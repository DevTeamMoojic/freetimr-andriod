package com.freetimr.activities;

import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.freetimr.R;
import com.freetimr.adapter.CityAutoCompleteAdapter;
import com.freetimr.fragments.ProfileDetailsFragment;
import com.freetimr.model.CandidateModel;
import com.freetimr.model.CityModel;
import com.freetimr.utils.FreeTimrUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import me.originqiu.library.EditTag;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CandidateNameDesignationActivity extends AppCompatActivity {

    EditText name, mobileNumber, emailAddress;
    TextView dateOfBirth;
    RadioButton male, female;

    CandidateModel candidateModel;

    ImageView cancel, update;

    EditTag languageEdit;
    AutoCompleteTextView location;
    ArrayList<CityModel> locationsList;
    CityAutoCompleteAdapter cityAutoCompleteAdapter;
    int cityId;
    String genderValue, mobileNumberStr, nameStr, emailStr;

    Date dateValue;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            setDateTime(year, monthOfYear + 1, dayOfMonth);

            dateValue = myCalendar.getTime();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_name_designation);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        candidateModel = FreeTimrUtils.getCandidateInfo(this);
        locationsList = FreeTimrUtils.getCityList(this);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");

        name = (EditText) findViewById(R.id.candidate_name_update);
        mobileNumber = (EditText) findViewById(R.id.candidate_mobile_update);
        dateOfBirth = (TextView) findViewById(R.id.candidate_dob_update);
        cancel = (ImageView) findViewById(R.id.update_candidate_cancel);
        update = (ImageView) findViewById(R.id.update_candidate_btn);
        languageEdit = (EditTag) findViewById(R.id.language_edit);
        location = (AutoCompleteTextView) findViewById(R.id.candidate_location);
        male = (RadioButton) findViewById(R.id.radio_btn_male);
        female = (RadioButton) findViewById(R.id.radio_btn_female);
        emailAddress = (EditText) findViewById(R.id.candidate_email_update);

        male.setTypeface(tf);
        female.setTypeface(tf);

        if (candidateModel.Gender.equalsIgnoreCase("F")) {
            female.setChecked(true);
            male.setChecked(false);
            genderValue = "F";
        } else {
            female.setChecked(false);
            male.setChecked(true);
            genderValue = "M";
        }

        emailAddress.setText(candidateModel.EmailId);

        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderValue = "M";
            }
        });

        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderValue = "F";
            }
        });

        // SET DATA
        name.setText(candidateModel.Name);
        mobileNumber.setText(candidateModel.MobileNumber);
        dateOfBirth.setText(candidateModel.DateOfBirth == null ? "Select a date" : new SimpleDateFormat("dd/MM/yyyy").format(candidateModel.DateOfBirth));

        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(CandidateNameDesignationActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateCandidateProfile();
            }
        });

        languageEdit.addTag("English");
        languageEdit.addTag("Hindi");
        languageEdit.addTag("Marathi");
        languageEdit.getTagList();

        cityAutoCompleteAdapter = new CityAutoCompleteAdapter(this, locationsList);
        location.setAdapter(cityAutoCompleteAdapter);
        location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cityId = locationsList.get(position).CityId;
            }
        });
    }

    private void setDateTime(int year, int monthOfYear, int dayOfMonth) {
        dateOfBirth.setText(year + "-" + monthOfYear + "-" + dayOfMonth);
    }

    public class CandidateUpdateRequestModel {
        public String CandidateId;
        public String CandidateName;
        public String Gender;
        public String MobileNo;
        public String EmailId;
        public String DateOfBirth;
    }

    private void updateCandidateProfile() {
        mobileNumberStr = mobileNumber.getText().toString();
        emailStr = emailAddress.getText().toString();
        nameStr = name.getText().toString();

        if (FreeTimrUtils.isValidEmailId(emailStr)) {
            FreeTimrUtils.showProgressDialog(this);

            CandidateUpdateRequestModel model = new CandidateUpdateRequestModel();
            model.Gender = genderValue;
            model.CandidateName = nameStr;
            model.MobileNo = mobileNumberStr;
            model.EmailId = emailStr;
            model.CandidateId = FreeTimrUtils.getCandidateInfo(this).Id;
            model.DateOfBirth = "" + dateOfBirth.getText().toString();

            FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(model));

            Call<CandidateModel> call = FreeTimrUtils.getRetrofit().updateCandidate(model);
            call.enqueue(new Callback<CandidateModel>() {
                @Override
                public void onResponse(Call<CandidateModel> call, Response<CandidateModel> response) {
                    if (response.code() == 200) {
                        String candidateModel = FreeTimrUtils.newGson().toJson(response.body());
                        FreeTimrUtils.saveCandidateInfo(CandidateNameDesignationActivity.this, candidateModel);
                    } else {
                        FreeTimrUtils.showMessage(CandidateNameDesignationActivity.this, "Something went wrong. Please try again.");
                    }
                    FreeTimrUtils.cancelCurrentDialog(CandidateNameDesignationActivity.this);
                }

                @Override
                public void onFailure(Call<CandidateModel> call, Throwable t) {
                    FreeTimrUtils.log(t.getLocalizedMessage());
                    FreeTimrUtils.cancelCurrentDialog(CandidateNameDesignationActivity.this);
                }
            });
        } else {
            FreeTimrUtils.showMessage(this, "Please enter a valid email");
        }
    }

}
